﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using Shared;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Xml;
using static Shared.OpcManager;
using System.Xml.Linq;
using System.IO;
using System.Linq;

public class OPCConfig : MonoBehaviour

{
    //public GameObject OPCUAConfigInputPrefabReference;
    private int configCounter = 0;
    [SerializeField]
    private SubscriptionSetCreatorXml_MonitoredData sscxmd;
    List<string> endpoints;
    private TMP_Dropdown dropdown;

    // Start is called before the first frame update
    void Start()
    {
        List<OPCServerInfo> opcserverList = OpcManager.Instance.OpcServerList;
        dropdown = GetComponentInChildren<TMP_Dropdown>();
        string address, username, password;

        foreach (OPCServerInfo serverInfo in opcserverList)
        {
            Debug.LogWarning(transform.GetChild(1).GetComponent<TMP_InputField>().text);

            transform.GetChild(1).GetComponent<TMP_InputField>().text = serverInfo.endpoint;
            transform.GetChild(2).GetComponent<TMP_InputField>().text = serverInfo.username;
            transform.GetChild(3).GetComponent<TMP_InputField>().text = serverInfo.password;

            address = transform.GetChild(1).gameObject.GetComponentInChildren<TextMeshProUGUI>().text;
            username = transform.GetChild(2).gameObject.GetComponentInChildren<TextMeshProUGUI>().text;
            password = transform.GetChild(3).gameObject.GetComponentInChildren<TextMeshProUGUI>().text;

            transform.GetChild(6).GetComponent<TMPro.TMP_Text>().text = OpcManager.Instance.statusStr;
            transform.GetChild(6).GetComponent<TMPro.TMP_Text>().color = OpcManager.Instance.statusColor;

            configCounter++;
        }

        setDropdownItems();
        Debug.Log("Number of OPCServer: " + configCounter);

    }


    public void addEndpoint()
    {
        string address = transform.GetChild(1).GetComponent<TMP_InputField>().text;
        string username = transform.GetChild(2).GetComponent<TMP_InputField>().text; ;
        string password = transform.GetChild(3).GetComponent<TMP_InputField>().text;

        OPCServerInfo serverInfo = new OPCServerInfo();
        serverInfo.endpoint = address;
        serverInfo.username = username;
        serverInfo.password = password;

        try
        {
            OpcManager.Instance.AddOpcServerInfo(serverInfo, true);
        }
        catch (Exception exp)
        {
            Debug.LogWarning("Exception");
        }

        Debug.LogWarning("Try to add endpoint.");
        Debug.LogWarning(OpcManager.Instance.statusStr);
        transform.GetChild(6).GetComponent<TMPro.TMP_Text>().text = OpcManager.Instance.statusStr;
        transform.GetChild(6).GetComponent<TMPro.TMP_Text>().color = OpcManager.Instance.statusColor;

        // add subscription set with new endpoint
        sscxmd.address = address;
        if (OpcManager.Instance.connected) sscxmd.Init();
    }

    private void setDropdownItems()
    {
        endpoints = getEndpointsFromXml("Endpoints.xml");
        //endpoints.Add("opc.tcp://desktop-10tli67:62541/Quickstarts/ReferenceServer");
        //endpoints.Add("opc.tcp://10.66.191.1:4840");
        if (endpoints == null)  // no endpoints.xml file --> save current dropdown list
        {
            endpoints = new List<string>();
            foreach (TMP_Dropdown.OptionData option in dropdown.options)
            {
                endpoints.Add(option.text);
            }
            saveDropdownToXML();
        }
        dropdown.ClearOptions();
        dropdown.AddOptions(endpoints);
    }


    //Add new endpoint to droplist
    public void addEndpointDroplist()
    {
        string newEndpoint = transform.GetChild(1).GetComponent<TMP_InputField>().text;
        if (!endpoints.Contains(newEndpoint) && newEndpoint != "")
        {
            endpoints.Add(newEndpoint);
            dropdown.ClearOptions();
            dropdown.AddOptions(endpoints);
            Debug.Log("Adding to Dropdown..");
        }
    }


    //Import endpoints from xml
    public List<string> getEndpointsFromXml(string xmlFile)
    {
        string file = System.IO.Path.Combine(Application.persistentDataPath, "EndpointData", xmlFile);

        if (!File.Exists(file))
        {
            Debug.LogWarning(file + " not found!");
            return null;
        }

        XDocument doc = XDocument.Load(file);
        var elements = doc.Element("Endpoints").Elements("Endpoint");
        List<string> endpoints = new List<string>();

        foreach (var element in elements)
        {
            endpoints.Add(element.Value.ToString());
        }
        return endpoints;
    }

    //Export endpoints to xml
    public void saveDropdownToXML()
    {
        string dirPath = System.IO.Path.Combine(Application.persistentDataPath, "EndpointData");
        if (!Directory.Exists(dirPath))
        {
            Directory.CreateDirectory(dirPath);
            Debug.LogWarning(dirPath + " created");
        }
        Debug.Log("Saving dropdown to xml...");
        string file = System.IO.Path.Combine(dirPath, "Endpoints.xml");
        XmlWriterSettings xws = new XmlWriterSettings();
        xws.OmitXmlDeclaration = true;
        xws.Indent = true;

        using (XmlWriter xw = XmlWriter.Create(file, xws))
        {
            XElement eps = new XElement("Endpoints",
                from ep in endpoints select new XElement("Endpoint", ep));
            XDocument doc = new XDocument(eps);

            doc.Save(xw);
        }
    }

    bool dropdownXmlExists()
    {
        string dirPath = System.IO.Path.Combine(Application.persistentDataPath, "EndpointData");
        return Directory.Exists(dirPath);
    }
}

