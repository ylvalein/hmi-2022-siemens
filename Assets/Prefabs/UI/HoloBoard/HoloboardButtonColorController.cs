﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This class provides Functions to change the main and extra icon color of the holoboard
/// </summary>
public class HoloboardButtonColorController : MonoBehaviour
{
    private const string MAIN_IMAGE_NAME = "IconMain";
    private const string EXTRA_IMAGE_NAME = "IconExtra";

    private const string KPI = "KPI";
    private const string ENERGY = "Energy";
    private const string Status = "Status";

    public GameObject kpiButton;        // Reference to kpi button
    public GameObject energyButton;     // Reference to energy button
    public GameObject statusButton;     // Reference to status button

    public Color activeColor = Color.black;
    public Color inactiveColor = Color.white;

    private ButtonImage kpiImg;
    private ButtonImage energyImg;
    private ButtonImage statusImg;


    // Struct to get the main and extra image together
    struct ButtonImage
    {
        public RawImage iconMain;
        public RawImage iconExtra;
    }

    private void Start()
    {
        // Collect references to the relevant images
        if (kpiButton && energyButton && statusButton)
        {
            kpiImg.iconMain = kpiButton.transform.Find(MAIN_IMAGE_NAME).GetComponent<RawImage>();
            kpiImg.iconExtra = kpiButton.transform.Find(EXTRA_IMAGE_NAME).GetComponent<RawImage>();

            energyImg.iconMain = energyButton.transform.Find(MAIN_IMAGE_NAME).GetComponent<RawImage>();
            energyImg.iconExtra = energyButton.transform.Find(EXTRA_IMAGE_NAME).GetComponent<RawImage>();


            statusImg.iconMain = statusButton.transform.Find(MAIN_IMAGE_NAME).GetComponent<RawImage>();
            statusImg.iconExtra = statusButton.transform.Find(EXTRA_IMAGE_NAME).GetComponent<RawImage>();
        }
        else
        {
            Debug.LogWarning("Not all Buttons were referenced");
        }
    }




    /// <summary>
    /// Changes the color of the main and extra image of the button to the activeColor.
    /// Changes the color of all other buttons to the inactiveColor.
    /// </summary>
    /// <param name="buttonName"></param>
    private void ChangeColorToActive(string buttonName)
    {
        kpiImg.iconMain.color = inactiveColor;
        kpiImg.iconExtra.color = inactiveColor;

        energyImg.iconMain.color = inactiveColor;
        energyImg.iconExtra.color = inactiveColor;

        statusImg.iconMain.color = inactiveColor;
        statusImg.iconExtra.color = inactiveColor;

        switch (buttonName)
        {
            case ("KPI"):
                kpiImg.iconMain.color = activeColor;
                kpiImg.iconExtra.color = activeColor;
                break;
            case ("Energy"):
                energyImg.iconMain.color = activeColor;
                energyImg.iconExtra.color = activeColor;
                break;
            case ("Status"):
                statusImg.iconMain.color = activeColor;
                statusImg.iconExtra.color = activeColor;
                break;
            default:
                Debug.LogError("Wrong Button name.");
                break;
        }
    }

    /// <summary>
    /// Change KPI icon color to active
    /// </summary>
    /// <param name="active"></param>
    public void ChangeColorKPI(bool active)
    {
        if (active)
        {
            ChangeColorToActive("KPI");
        }
    }

    /// <summary>
    /// Change Energy icon color to active
    /// </summary>
    /// <param name="active"></param>
    public void ChangeColorEnergy(bool active)
    {
        if (active)
        {
            ChangeColorToActive("Energy");
        }
    }

    /// <summary>
    /// Change Energy icon color to active
    /// </summary>
    /// <param name="active"></param>
    public void ChangeColorStatus(bool active)
    {
        if (active)
        {
            ChangeColorToActive("Status");
        }
    }
}


