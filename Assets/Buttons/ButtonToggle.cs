﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonToggle : MonoBehaviour
{
    public Sprite buttonUnpressed;
    public Sprite buttonPressed;
    public bool pressed;
    public GameObject obj;

    // Start is called before the first frame update
    void Start()
    {
        if (obj != null)
        {
            pressed = !obj.activeInHierarchy;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Toggle()
    {
        if (!pressed)
        {
            Debug.LogError("ON");
            GetComponent<Image>().sprite = buttonPressed;
        }
        else
        {
            Debug.LogError("OFF");
            GetComponent<Image>().sprite = buttonUnpressed;
        }
        pressed = !pressed;

    }

}
