﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace Shared
{
    public class LookupAttribute : Attribute
    {
        internal void Assign(FieldInfo fieldInfo, object mono)
        {
            Debug.Log("Looking for reference to object with type: " + fieldInfo.FieldType);

            var obj = LookupReferenceManager.lookupTable[fieldInfo.FieldType];

            fieldInfo.SetValue(mono, obj);
        }
    }
}
