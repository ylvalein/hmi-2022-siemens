﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;


namespace Shared
{
    public class LookupReferenceAttribute : Attribute
    {
        internal void RegisterReference(FieldInfo fieldInfo, System.Object obj)
        {
            RegisterReference(fieldInfo.FieldType, obj);
        }

        internal void RegisterReference(Type type, System.Object obj)
        {
            Debug.Log("Registering Reference for type: " + type + ", obj: " + obj);

            if (!LookupReferenceManager.lookupTable.ContainsKey(type))
            {
                LookupReferenceManager.lookupTable.Add(type, obj);
            }
            else
            {
                Debug.LogError("Only one attribute can be assigned for lookup");
            }
        }
    }
}
