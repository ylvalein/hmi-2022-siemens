﻿using Microsoft.MixedReality.Toolkit.Input;
using UnityEngine;
using UnityEngine.Events;

namespace Shared
{
    public class ActionOnClick : MonoBehaviour, IMixedRealityPointerHandler
    {
        public UnityEvent actionOnClick;


        public void OnPointerClicked(MixedRealityPointerEventData eventData)
        {
            actionOnClick.Invoke();
        }

        public void OnPointerDown(MixedRealityPointerEventData eventData)
        {

        }

        public void OnPointerDragged(MixedRealityPointerEventData eventData)
        {

        }

        public void OnPointerUp(MixedRealityPointerEventData eventData)
        {

        }
    }
}
