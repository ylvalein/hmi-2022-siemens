﻿///-----------------------------------------------------------------
///   Namespace:      AmbergGWE
///   Class:          BeamTransform
///   Description:    Manages the Movement of clickdummys
///   Author:         Hassel, Tobias                    Date: 14.08.2018
///   Notes:          
///-----------------------------------------------------------------

using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Shared
{
    public class ObjectMover : MonoBehaviour
    {

        [Tooltip("The object that should be moved by the beam functions")]
        public GameObject beamObject;

        [Tooltip("The speed with which the movement happens")]
        public float movementSpeed = 220;

        [Tooltip("The speed with which the rotation happens")]
        public float rotationSpeed = 1;

        [Tooltip("The position the object should be moved to.")]
        public Transform targetTransform;

        [Tooltip("Offset for the beamed object")]
        public float beamMovementOffset = 0.2f;

        [Tooltip("Event that gets called after a beamObject finished it's movement and reached the target position.")]
        public UnityEvent OnMovementFinished;

        [Tooltip("Event that gets called after a beamObject finished it's rotation and reached the target rotation.")]
        public UnityEvent OnRotationFinished;

        // Threshold in degrees
        protected float rotationThreshhold = 2;

        void Start()
        {
            if (beamObject == null)
            {
                beamObject = this.gameObject;
            }
        }

        /// <summary>
        /// Moves the object animated to the given position
        /// </summary>
        /// <param name="targetPos"></param>
        /// <param name="offset"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        protected virtual IEnumerator MoveToTarget(Vector3 targetPos, float offset, UnityEvent action = null)
        {

            while ((beamObject.transform.position - targetPos).sqrMagnitude > offset * offset)
            {
                Debug.Log("targetPos: " + targetPos);
                beamObject.transform.position = Vector3.MoveTowards(beamObject.transform.position, targetPos, Time.deltaTime * movementSpeed);
                yield return new WaitForEndOfFrame();
            }
            beamObject.transform.position = targetPos;

            if (action != null)
            {
                action.Invoke();
            }
        }


        /// <summary>
        /// Rotates the object to the given targetRotation
        /// </summary>
        /// <param name="targetRotation"></param>
        /// <returns></returns>
        protected virtual IEnumerator RotateToTarget(Quaternion targetRotation, UnityEvent action = null)
        {
            // As long as the angle between the object and the target is bigger than our threshold, rotate the object (linear interpolation)
            while (Quaternion.Angle(beamObject.transform.rotation, targetRotation) > rotationThreshhold)
            {
                beamObject.transform.rotation = Quaternion.Lerp(beamObject.transform.rotation, targetRotation, Time.deltaTime * rotationSpeed);
                yield return new WaitForEndOfFrame();
            }
            // Set the objects rotation to the exact target rotation 
            beamObject.transform.rotation = targetRotation;

            if (action != null)
            {
                action.Invoke();
            }
        }

        /// <summary>
        /// Rotates the beamObject to the targetRotation
        /// </summary>
        /// <param name="action"></param>
        public void RotateToTarget(UnityEvent action = null)
        {
            StartCoroutine(RotateToTarget(targetTransform.rotation, action));
        }

        /// <summary>
        /// Rotates the beamObject to the targetRotation
        /// </summary>
        public void RotateToTarget()
        {
            RotateToTarget(OnRotationFinished);
        }

        /// <summary>
        /// Moves the beamObject to the given targetPosition
        /// </summary>
        public void MoveToTarget()
        {
            MoveToTarget(OnMovementFinished);
        }

        /// <summary>
        /// Moves the beamObject to the given targetPosition and invokes the action after the movement has finished 
        /// </summary>
        /// <param name="action"></param>
        public void MoveToTarget(UnityEvent action = null)
        {
            StartCoroutine(MoveToTarget(targetTransform.position, beamMovementOffset, action));
        }

        /// <summary>
        /// Moves and rotates object to the given targetTransform, does not invoke OnMovementFinished / OnRotationFinished events
        /// </summary>
        public virtual void MoveAndRotateTarget()
        {
            StartCoroutine(MoveToTarget(targetTransform.position, beamMovementOffset));
            StartCoroutine(RotateToTarget(targetTransform.rotation));
        }
    }
}
