﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    public class FollowTransform : MonoBehaviour
    {
        public Transform targetTransform;

        private void OnEnable()
        {
            transform.position = targetTransform.position;
            transform.rotation = targetTransform.rotation;
        }

        private void LateUpdate()
        {
            if (targetTransform.hasChanged)
            {
                targetTransform.hasChanged = false;
                transform.position = targetTransform.position;
                transform.rotation = targetTransform.rotation;
            }
        }
    }
}
