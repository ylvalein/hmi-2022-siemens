﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace Shared
{
    public class LookupReferenceManager : MonoBehaviour
    {
        public static Dictionary<System.Type, System.Object> lookupTable = new Dictionary<System.Type, System.Object>();

        public void LookupReferences()
        {
            List<UnityEngine.Object> sceneActive = new List<UnityEngine.Object>(SceneHelperFunctions.FindAllObjects());
            foreach (UnityEngine.Object unityObject in sceneActive)
            {

                var classAttribute = unityObject.GetType().GetTypeInfo().GetCustomAttribute<LookupReferenceAttribute>();// unityObject.GetType().GetCustomAttribute(typeof(LookupReferenceAttribute), true) as LookupReferenceAttribute;

                if (classAttribute != null)
                {
                    var value = unityObject;
                    if (value != null) classAttribute.RegisterReference(unityObject.GetType(), unityObject);
                }

                FieldInfo[] objectFields = unityObject.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public);
                for (int i = 0; i < objectFields.Length; i++)
                {
                    LookupReferenceAttribute attributeReference = objectFields[i].GetCustomAttribute<LookupReferenceAttribute>();// Attribute.GetCustomAttribute(, typeof(LookupReferenceAttribute)) as LookupReferenceAttribute;
                    if (attributeReference != null)
                    {
                        var value = objectFields[i].GetValue(unityObject);
                        if (value != null) attributeReference.RegisterReference(objectFields[i], value);
                    }
                }

            }

            foreach (UnityEngine.Object unityObject in sceneActive)
            {
                FieldInfo[] objectFields = unityObject.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public);
                for (int i = 0; i < objectFields.Length; i++)
                {
                    LookupAttribute attribute = objectFields[i].GetCustomAttribute<LookupAttribute>();
                    try
                    {
                        if (attribute != null) attribute.Assign(objectFields[i], unityObject);
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e);
                    }
                }
            }
        }

        public void OnDestroy()
        {
            lookupTable.Clear();
        }
    }
}
