﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    public class TestFunctionCall : MonoBehaviour
    {

        public bool callFunction;
        public UnityEngine.Events.UnityEvent function;

        // Update is called once per frame
        void Update()
        {
            if (callFunction)
            {
                callFunction = false;
                function.Invoke();
            }
        }
    }
}
#endif
