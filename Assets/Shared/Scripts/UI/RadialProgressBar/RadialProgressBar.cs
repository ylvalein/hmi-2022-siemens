﻿///-----------------------------------------------------------------
///   Namespace:      Shared
///   Class:          RadialProgressBar
///   Description:    A radial Progressbar that can be used to show a state of a progress
///   Author:         Hassel, Tobias                    Date: 08.08.2018
///   Notes:          
///-----------------------------------------------------------------

using UnityEngine;
using UnityEngine.UI;

namespace Shared
{
    public class RadialProgressBar : MonoBehaviour
    {
        [Tooltip("Text that is shown while loading")]
        public GameObject LoadingText;

        [Tooltip("E.g. \"60%\"")]
        public Text ProgressIndicator;

        [Tooltip("Actual loading bar image")]
        public Image LoadingBar;

        //public float speed;

        public float maxVal = 2;

        private bool isDone = false;
        private float currentValue = 0;

        public bool IsDone
        {
            get
            {
                return isDone;
            }
        }

        public void UpdateProgress(float value)
        {
            currentValue = value;

            LoadingBar.fillAmount = currentValue / maxVal;

            if (currentValue < maxVal)
            {
                isDone = false;
                ProgressIndicator.text = ((int)(LoadingBar.fillAmount * 100)).ToString() + "%";
                LoadingText.SetActive(true);
            }
            else
            {
                currentValue = maxVal;
                LoadingText.SetActive(false);
                ProgressIndicator.text = "Done";
                isDone = true;
            }


        }

        public void IncrementProgress(float value)
        {
            UpdateProgress(currentValue + value);
        }
    }

}
