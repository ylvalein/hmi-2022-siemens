﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Shared
{
    [Serializable]
    public class IconGraphicalChangeObject
    {
        public string IconName;
        public GraphicalChangeObject BG = new GraphicalChangeObject();
        public GraphicalChangeObject Border = new GraphicalChangeObject();
        public GraphicalChangeObject IconExtra = new GraphicalChangeObject();
        public GraphicalChangeObject IconMain = new GraphicalChangeObject();
    }
}
