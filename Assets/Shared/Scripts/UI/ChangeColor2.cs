﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Shared
{
    public class ChangeColor2 : ChangeColor1
    {

        public Color background;
        protected Color bLast;

        protected List<RawImage> bgList = new List<RawImage>();

        // Use this for initialization
        protected override void Awake()
        {
            FillOutlineList();
            FillIconList();
            FillBackgroundList();
        }

        // Update is called once per frame
        protected override void Update()
        {
            ChangeOutlineColor();
            ChangeIconColor();
            ChangeBackground();
        }

        protected void ChangeBackground()
        {
            if (background != bLast)
            {
                foreach (RawImage raw in bgList)
                    raw.color = background; bLast = background;
            }
        }

        protected void FillBackgroundList()
        {
            foreach (Component c in GetComponentsInChildren<RawImage>())
            {

                string test = c.name;

                //Background
                if (test.Contains("BG"))
                    bgList.Add(c.GetComponent<RawImage>());

            }
        }
    }
}