﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEditor;
using System;
using UnityEngine.EventSystems;
using UnityEngine.Events;

namespace Shared
{
    [Serializable]
    public class ButtonMemory : Button
    {
        //Events
        [SerializeField]
        public ButtonOnPointerEvent onPointerEnter;
        [SerializeField]
        public ButtonOnPointerEvent onPointerExit;

        [SerializeField]
        public ButtonClickedEvent OnClick;

        public bool test;

        [SerializeField]
        public bool isSelected = false;

        [SerializeField]
        public List<GraphicalChangeButtonMemory> myGraphicalObjects = new List<GraphicalChangeButtonMemory>();
        [SerializeField]
        public List<GameObject> GameObjectsAttached = new List<GameObject>();

        protected override void Start()
        {
            onClick.AddListener(click_action);
            setColor(isSelected);
            setActiveGameObjects(isSelected);
        }

        void click_action()
        {
            ActivateButton(!isSelected);
            OnClick.Invoke();
        }

        public void ActivateButton(bool activate)
        {
            if (activate)
                isSelected = true;
            else
                isSelected = false;

            setColor(isSelected);
            setActiveGameObjects(isSelected);
        }

        void setColor(bool active)
        {
            foreach (var graphObj in myGraphicalObjects)
            {
                if (graphObj == null) continue;
                graphObj.setColor(isSelected);
            }
        }

        void setActiveGameObjects(bool activate)
        {
            foreach (var gobj in GameObjectsAttached)
            {
                if (gobj == null) continue;
                gobj.SetActive(activate);
            }
        }

        public override void OnPointerEnter(PointerEventData eventData)
        {
            base.OnPointerEnter(eventData);
            onPointerEnter.Invoke();
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            base.OnPointerExit(eventData);
            onPointerExit.Invoke();
        }


        [Serializable]
        public class ButtonOnPointerEvent : UnityEvent
        {
        }

    }
}