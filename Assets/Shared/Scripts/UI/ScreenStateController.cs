﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Shared
{
    public class ScreenStateController : MonoBehaviour
    {

        [Header("Connection")]

        [SerializeField]
        [Tooltip("Connection image background")]
        private RawImage connectionBG;

        [SerializeField]
        [Tooltip("Connection image icon")]
        private RawImage connectionIcon;

        [SerializeField]
        [Tooltip("Texture for a stabil connection")]
        private Texture connectedTex;

        [SerializeField]
        [Tooltip("Texture for an instabil connection")]
        private Texture disconnectedTex;

        [SerializeField]
        [Tooltip("Connected color")]
        private Color connectedCol;

        [SerializeField]
        [Tooltip("Disconnected color")]
        private Color disconnectedCol;

        [Header("Emergency")]

        [SerializeField]
        [Tooltip("Emergency image background")]
        private RawImage emergencyBG;

        [SerializeField]
        [Tooltip("Emergency color")]
        private Color emergencyCol;

        [SerializeField]
        [Tooltip("Not an emergency color")]
        private Color noEmergencyCol;

        [Header("Background")]

        [SerializeField]
        [Tooltip("Background border info")]
        private RawImage backgroundBorderInfo;

        [SerializeField]
        [Tooltip("Error color")]
        private Color errorCol;

        [SerializeField]
        [Tooltip("No error color")]
        private Color noErrorCol;

        [Header("Operationa")]

        [SerializeField]
        [Tooltip("Operationa slider icon")]
        private RawImage operationaIcon;

        [SerializeField]
        [Tooltip("Border of the operationa slider icon")]
        private RawImage operationaIconBorder;

        [SerializeField]
        [Tooltip("On color")]
        private Color operationaOnCol;

        [SerializeField]
        [Tooltip("Off color")]
        private Color operationaOffCol;

        [SerializeField]
        private float lerpSpeed = 1f;

        private readonly Vector3 ON_POS = new Vector3(23.91f, 23.04f, 0f);
        private readonly Vector3 OFF_POS = new Vector3(26.53015f, 23.04f, 0f);

        private bool isLerping = false;

        bool isEmergencyStopped = false;
        bool isOpcConnected = false;



        /// <summary>
        /// Change the connection state in the screen
        /// </summary>
        /// <param name="isConnected"></param>
        public void ChangeConnectionState(bool isConnected)
        {
            isOpcConnected = isConnected;
            ChangeErrorState(GetCurrentErrorState());

            if ((connectionBG != null) && (connectionIcon != null) && (connectedTex != null) && (connectedTex != null))
            {
                if (isConnected)
                {
                    connectionBG.color = connectedCol;
                    connectionIcon.texture = connectedTex;
                }
                else
                {
                    connectionBG.color = disconnectedCol;
                    connectionIcon.texture = disconnectedTex;
                }
            }
            else
            {
                Debug.Log("No Color or Texture added in " + this.name);
            }
        }

        /// <summary>
        /// Change the emergency state in the screen
        /// </summary>
        /// <param name="isEmergency"></param>
        public void ChangeEmergencyState(bool isEmergency)
        {
            isEmergencyStopped = isEmergency;
            ChangeErrorState(GetCurrentErrorState());

            if ((emergencyBG != null) && (emergencyCol != null) && (noEmergencyCol != null))
            {
                if (isEmergency)
                {
                    emergencyBG.color = emergencyCol;
                }
                else
                {
                    emergencyBG.color = noEmergencyCol;
                }
            }
            else
            {
                Debug.Log("Emergency Colors not set in " + this.name);
            }
        }

        /// <summary>
        /// Change the error state shown in the screen
        /// </summary>
        /// <param name="isError"></param>
        public void ChangeErrorState(bool isError)
        {
            if ((backgroundBorderInfo != null) && (errorCol != null) && (noErrorCol != null))
            {
                if (isError)
                {
                    backgroundBorderInfo.color = errorCol;
                }
                else
                {
                    backgroundBorderInfo.color = noErrorCol;
                }
            }
        }

        /// <summary>
        /// Change the operationa state shown in the screen
        /// </summary>
        /// <param name="isOn"></param>
        public void ChangeOperationaState(bool isOn)
        {
            if (isOn)
            {
                if (!isLerping)
                {
                    StartCoroutine(SetOperationaPos(ON_POS));
                    operationaIcon.color = operationaOnCol;
                }
            }
            else
            {
                if (!isLerping)
                {
                    StartCoroutine(SetOperationaPos(OFF_POS));
                    operationaIcon.color = operationaOffCol;
                }
            }
        }

        public bool GetCurrentErrorState()
        {
            return isEmergencyStopped || !isOpcConnected;
        }

        // movement of the operationa slider
        private IEnumerator SetOperationaPos(Vector3 pos)
        {

            isLerping = true;
            float time = 0;

            while (time < .5)
            {
                time += Time.deltaTime;
                operationaIcon.transform.localPosition = Vector3.Lerp(operationaIcon.transform.localPosition, pos, Time.deltaTime * lerpSpeed);
                operationaIconBorder.transform.localPosition = Vector3.Lerp(operationaIcon.transform.localPosition, pos, Time.deltaTime * lerpSpeed);
                yield return new WaitForEndOfFrame();
            }

            isLerping = false;
        }

        #region testing
        //private bool testBool;

        //private void Update()
        //{
        //    if (Input.GetButtonDown("Fire1"))
        //    {
        //        //ChangeConnectionState(testBool);
        //        //ChangeEmergencyState(testBool);
        //        //ChangeErrorState(testBool);
        //        ChangeOperationaState(testBool);
        //        testBool = !testBool;
        //    }
        //}
        #endregion
    }
}
