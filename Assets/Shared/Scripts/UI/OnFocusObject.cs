﻿using UnityEngine;
using UnityEngine.Events;
using Microsoft.MixedReality.Toolkit.Input;

namespace Shared
{
    public class OnFocusObject : MonoBehaviour, IMixedRealityFocusHandler
    {
        public UnityEvent OnObjectFocus;
        public UnityEvent OnObjectFocusExit;


        public void OnFocusEnter(FocusEventData eventData)
        {
            OnObjectFocus.Invoke();
        }

        public void OnFocusExit(FocusEventData eventData)
        {
            OnObjectFocusExit.Invoke();
        }
    }
}
