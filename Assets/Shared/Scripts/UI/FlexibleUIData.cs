﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Shared
{
    [CreateAssetMenu(menuName = "Flexible UI Data")]
    public class FlexibleUIData : ScriptableObject
    {
        public List<IconGraphicalChangeObject> myGraphicalIcons = new List<IconGraphicalChangeObject>();

    }
}
