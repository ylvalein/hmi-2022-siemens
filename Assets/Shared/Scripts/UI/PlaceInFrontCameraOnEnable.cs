﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    public class PlaceInFrontCameraOnEnable : PlaceInFrontOnEnable
    {
        protected override void OnEnable()
        {
            targetTransform = Camera.main.transform;
            base.OnEnable();
        }
    }
}
