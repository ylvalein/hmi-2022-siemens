﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace Shared
{
    [ExecuteInEditMode()]
    public class FlexibleUIButton : MonoBehaviour
    {

        RawImage BG;
        RawImage Border;
        RawImage IconExtra;
        RawImage IconMain;

        ButtonMemory buttonMem;

        [HideInInspector]
        public int _choiceIndex = 0;

        public FlexibleUIData myData;

        [HideInInspector]
        public IconGraphicalChangeObject iconSelected;

        public void OnSelectionChanged()
        {
            Init();
            changeIcon();
        }

        private void Awake()
        {
            Init();
            changeIcon();
        }

        public void changeIcon()
        {
            if (myData == null) return;
            var nameTemp = myData.myGraphicalIcons.Select(x => x.IconName).ToArray()[_choiceIndex];
            iconSelected = myData.myGraphicalIcons.Find(x => x.IconName == nameTemp);

            if (iconSelected == null) return;

            BG.texture = iconSelected.BG.Texture;
            BG.color = iconSelected.BG.NormalColor;

            Border.texture = iconSelected.Border.Texture;
            Border.color = iconSelected.Border.NormalColor;

            IconExtra.texture = iconSelected.IconExtra.Texture;
            IconExtra.color = iconSelected.IconExtra.NormalColor;

            IconMain.texture = iconSelected.IconMain.Texture;
            IconMain.color = iconSelected.IconMain.NormalColor;

            linkToButtonMemory();
        }

        public void Init()
        {
            Debug.Log("InitFlexUIB");
            buttonMem = GetComponent<ButtonMemory>();
            BG = this.transform.Find("BG").GetComponent<RawImage>();
            Border = this.transform.Find("Border").GetComponent<RawImage>();
            IconExtra = this.transform.Find("IconExtra").GetComponent<RawImage>();
            IconMain = this.transform.Find("IconMain").GetComponent<RawImage>();

        }

        void linkToButtonMemory()
        {
            if (buttonMem == null) return;
            List<GraphicalChangeButtonMemory> myObjlist = new List<GraphicalChangeButtonMemory>();

            myObjlist.Add(convertToGBButtonMemory(BG, iconSelected.BG));
            myObjlist.Add(convertToGBButtonMemory(IconExtra, iconSelected.IconExtra));
            myObjlist.Add(convertToGBButtonMemory(IconMain, iconSelected.IconMain));
            buttonMem.myGraphicalObjects = myObjlist;

        }

        GraphicalChangeButtonMemory convertToGBButtonMemory(RawImage rI, GraphicalChangeObject GCO)
        {
            GraphicalChangeButtonMemory GCB = new GraphicalChangeButtonMemory();
            GCB.GraphicalObject = rI;
            GCB.NormalColor = GCO.NormalColor;
            GCB.HighlightedColor = GCO.HighlightedColor;
            GCB.ActiveColor = GCO.ActiveColor;
            return GCB;
        }

    }
}