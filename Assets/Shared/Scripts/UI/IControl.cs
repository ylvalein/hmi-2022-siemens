﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    public interface IControlFloatMulti
    {
        void OnValueChanged(int index, float newValue);
    }

    public interface IControlFloatSingle
    {
        void OnValueChanged(float newValue);
    }
}