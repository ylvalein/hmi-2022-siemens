﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    public class PlaceInFrontOnEnable : MonoBehaviour
    {

        public Transform targetTransform;
        public float distance;

        protected virtual void OnEnable()
        {
            PlaceInFront();
        }

        public void PlaceInFront()
        {
            this.transform.position = targetTransform.position + targetTransform.forward * distance;
        }
    }
}
