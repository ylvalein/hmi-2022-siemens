﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Shared
{
    public class Charge : MonoBehaviour
    {
        [Header("Main Parameters")]
        //public RawImage GlassBackground;
        //public RawImage Glass;
        public RawImage BatteryBackground;
        public Image BatteryLevel;
        public RawImage Lightning;


        [Header("Colors")]
        [SerializeField]
        private Color _glassBackgroundColor;
        //public Color GlassBackgroundColor
        //{
        //    get { return _glassBackgroundColor; }
        //    set
        //    {
        //        _glassBackgroundColor = value;
        //        SetColor(GlassBackground, _glassBackgroundColor);
        //    }
        //}

        [SerializeField]
        private Color _glassColor;
        //public Color GlassColor
        //{
        //    get { return _glassColor; }
        //    set
        //    {
        //        _glassColor = value;
        //        SetColor(Glass, _glassColor);
        //    }
        //}

        [SerializeField]
        private Color _batteryBackgroundColor;
        public Color BatteryBackgroundColor
        {
            get { return _batteryBackgroundColor; }
            set
            {
                _batteryBackgroundColor = value;
                SetColor(BatteryBackground, _batteryBackgroundColor);
            }
        }

        [SerializeField]
        private Color _batteryLevelColor;
        public Color BatteryLevelColor
        {
            get { return _batteryLevelColor; }
            set
            {
                _batteryLevelColor = value;
                SetColor(BatteryLevel, _batteryLevelColor);
            }
        }

        [SerializeField]
        private Color _lightningColor;
        public Color LightningColor
        {
            get { return _lightningColor; }
            set
            {
                _lightningColor = value;
                SetColor(Lightning, _lightningColor);
            }
        }


        [SerializeField]
        [Range(0, 100)]
        private float _value;
        public float Value
        {
            get { return _value; }
            set
            {
                _value = value;
                SetValue();
            }
        }

        private void OnValidate()
        {
            //SetColor(GlassBackground, _glassBackgroundColor);
            //SetColor(Glass, _glassColor);
            SetColor(BatteryBackground, _batteryBackgroundColor);
            SetColor(BatteryLevel, _batteryLevelColor);
            SetColor(Lightning, _lightningColor);
            SetValue();
        }

        protected virtual void SetColor(Graphic img, Color color)
        {
            if (color == null) return;
            img.color = color;
        }

        protected void SetValue()
        {
            BatteryLevel.fillAmount = _value / 100f;
        }
    }
}