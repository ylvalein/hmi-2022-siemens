﻿using UnityEngine;
using UnityEngine.UI;
using Microsoft.MixedReality.Toolkit.Input;

namespace Shared
{
    public class LookIndicator : MonoBehaviour, IMixedRealityFocusHandler
    {
        public enum TextPosition { Left, Top }
        public GameObject buttonText;
        public TextPosition mytextposition;
        private GameObject copy;
        private Vector3 positionText;
        public string textToShow = "Default";

        public Transform overrideParent;

        public bool useRectTransformInsteadOfBoxCollider = false;

        public float Offset = 0;
        public int textSize = 5;

        bool isCurrentlyShowing = false;
        private void Start()
        {
            if (overrideParent == null) overrideParent = transform;

            float height;
            float width;
            if (useRectTransformInsteadOfBoxCollider && transform is RectTransform)
            {
                width = ((RectTransform)transform).sizeDelta.x * transform.localScale.x;
                height = ((RectTransform)transform).sizeDelta.y * transform.localScale.y;
            }
            else
            {
                width = GetComponent<BoxCollider>().size.x;
                height = GetComponent<BoxCollider>().size.y;
            }

            switch (mytextposition)
            {
                case TextPosition.Top:
                    positionText = new Vector3(0, (height / 2) + Offset, 0);
                    break;

                case TextPosition.Left:
                    positionText = new Vector3(-(width / 2) - Offset, 0, 0);
                    break;
                default:
                    positionText = new Vector3(-GetComponent<BoxCollider>().size.x / 2, 0, 0);
                    break;
            }
        }

        public void OnFocusEnter(FocusEventData eventData)
        {
            if (isCurrentlyShowing) return;
            isCurrentlyShowing = true;
            copy = Instantiate(buttonText, overrideParent, false);
            copy.transform.localPosition = positionText;
            //copy.transform.localScale = new Vector3(0.1f, 0.1f, 0);
            copy.GetComponent<Text>().text = textToShow;
            copy.GetComponent<Text>().fontSize = textSize;
            //copy.tag = "destroy";
        }

        public void OnFocusExit(FocusEventData eventData)
        {
            if (isCurrentlyShowing)
            {
                Destroy(copy);
                isCurrentlyShowing = false;
            }
        }

        //private string setTextToShow()
        //{
        //    if (name == "IconKPI")
        //        return "KPI's";
        //    else if (name == "IconMaintenance")
        //        return "Alarms";
        //    else if (name == "IconInfo")
        //        return "Orders";
        //    else
        //        return "default";
        //}
    }
}
