﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Shared
{
    public class ChangeColorStationStatus : MonoBehaviour
    {

        public RawImage targetImage;

        public Color colorIdle = new Color32(104, 194, 218, 255);
        public Color colorWorking = new Color32(110, 190, 73, 255);
        public Color colorError = new Color32(245, 238, 54, 255);
        public Color colorEmergencyStop = new Color32(255, 0, 0, 255);

        private void Start()
        {
            //OnStationStatusChanged(MACHINE_MODULE_STATUS.Idle);
        }

        //   public void OnStationStatusChanged (MACHINE_MODULE_STATUS status) {
        //       switch (status)
        //       {
        //           case MACHINE_MODULE_STATUS.Error:
        //               targetImage.color = colorError;
        //               break;
        //           case MACHINE_MODULE_STATUS.Idle:
        //               targetImage.color = colorIdle;
        //               break;
        //           case MACHINE_MODULE_STATUS.EmergencyStop:
        //               targetImage.color = colorEmergencyStop;
        //               break;
        //           case MACHINE_MODULE_STATUS.Working:
        //               targetImage.color = colorWorking;
        //               break;
        //       }
        //}
    }
}
