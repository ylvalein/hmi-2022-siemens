﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Shared
{
    [RequireComponent(typeof(TimedUpdateCaller))]
    public class TrendManager : ControlManager, ITimedUpdate, IControlFloatSingle
    {
        public float scalingFactor = 1.0f;
        [Header("Trendgraph Reference")]
        public Material mainMatRef;
        public Material currentMatRef;
        public RawImage mainPic;
        public RectTransform rectHighHigh;
        public RectTransform rectHigh;
        public RectTransform rectLow;
        public RectTransform rectLowLow;
        public Text value;
        public Text maxText;
        public Text midText;
        public Text minText;

        public string valueUnit = "";

        private Material mainMat;
        private Material currentMat;

        //LinkedList<float> xData;
        LinkedList<float> yData;
        public LinkedList<float> YData { get { return yData; } }

        

        [Header("Set Layout")]
        public float history;
        //public float minTopValue = 9000;
        public float minZoomLevel = 100;
        public float fixedMaxValue = 500;
        public bool fixedMaximum = false;
        public float fixedMinValue = 0;
        public bool fixedMinumum = false;
        public Color32 colorBar;
        [Range(0f, 1f)]
        public float barsize;


        [Header("Set Alpha Color")]
        [Range(0f, 1f)]
        public float alpha;

        public Color32 colorHH = Color.white;

        public Color32 colorH = Color.white;

        public Color32 colorN = Color.white;

        public Color32 colorL = Color.white;

        public Color32 colorLL = Color.white;
        //public bool negativeValues = false;
        //int listMaxLength = 10000;

        public RawImage BarMat;
        public RawImage GraphMat;

        int listMaxLength = 201;

        //float xDrift = 0;
        private float yDrift = 0;
        public float YDrift { get { return yDrift; } set { yDrift = value; } }

        float lastOpcValue;
        private Texture2D t;

        private float currentOpcValue = 0;

        void Start()
        {
            mainMat = new Material(mainMatRef);
            currentMat = new Material(currentMatRef);
            BarMat.material = currentMat;
            GraphMat.material = mainMat;
            yData = new LinkedList<float>();

            yDrift += scalingFactor * currentOpcValue - 0.5f;
            yData.AddLast(yDrift);

            t = new Texture2D((int)history + 1, 1);
        }

        public void TimedUpdate(float deltaTime)
        {
            
            yDrift = lastOpcValue;
            yDrift = yDrift * scalingFactor;
            float ytemp = yDrift;
            if (ytemp < 0) ytemp = 0;
            yData.AddLast(ytemp);

            if (yData.Count > listMaxLength)
            {
                yData.RemoveFirst();
            }

            value.text = ((int)yData.Last.Value).ToString();
            value.text += " " + valueUnit;

            LinkedListNode<float> yNode = null;

            if (yData != null)
            {
                yNode = yData.Last;
            }

            float[] xTrack = new float[(int)history + 1];
            float[] yTrack = new float[(int)history + 1];

            float min = float.MaxValue;
            float max = float.MinValue;

            for (int i = (int)history; i >= 0; i--)
            {

                if (yNode != null)
                {
                    yTrack[i] = yNode.Value;

                    yNode = yNode.Previous;
                }
                else
                {
                    xTrack[i] = 0;
                    yTrack[i] = 0;
                }
                min = Mathf.Min(min, yTrack[i]);
                max = Mathf.Max(max, yTrack[i]);
            }

            float tminZoomLevel = minZoomLevel - max + min;
            if (tminZoomLevel < 0) tminZoomLevel = 0;
            max += .5f * tminZoomLevel;
            min -= .5f * tminZoomLevel;
            if (fixedMinumum) min = fixedMinValue;
            if (fixedMaximum) max = fixedMaxValue;
            maxText.text = Mathf.RoundToInt(max).ToString();
            midText.text = Mathf.RoundToInt((max + min) / 2).ToString();
            minText.text = Mathf.RoundToInt(min).ToString();

            for (int i = (int)history; i >= 0; i--)
            {

                if (max > min)
                {
                    yTrack[i] = (yTrack[i] - min) / (max - min) * .999f;
                    if (yTrack[i] < 0) yTrack[i] = 0;
                    if (yTrack[i] >= 1) yTrack[i] = .999f;

                }
                else
                {
                    yTrack[i] = 0;
                }
                xTrack[i] = 1f - (1f / history * i);
                Color32 temp = new Color32((byte)(xTrack[i] * 256), (byte)(yTrack[i] * 256), 0, 0);
                t.SetPixel(i, 0, temp);
            }
            currentMat.SetFloat("_Value", yTrack[(int)history]);  // SETFLOAT 
            t.wrapMode = TextureWrapMode.Clamp;
            t.Apply();
            mainMat.SetTexture("_Arrays", t);
            currentMat.SetColor("_Color3", colorBar);
            currentMat.SetFloat("_Bar", barsize);
            mainMat.SetFloat("_Alpha", alpha);
            mainMat.SetColor("_Color5", colorHH);
            mainMat.SetColor("_Color4", colorH);
            mainMat.SetColor("_Color3", colorN);
            mainMat.SetColor("_Color2", colorL);
            mainMat.SetColor("_Color1", colorLL);

            float topval = mainPic.rectTransform.rect.height / 2 + mainPic.rectTransform.localPosition.y;
            float botval = mainPic.rectTransform.rect.height / -2 + mainPic.rectTransform.localPosition.y;
            if (topval - botval != 0)
            {

                float root = topval - 1 / (1 - (min / max)) * (topval - botval);
                float rate = (topval - botval) / (max - min);

                float hhval = root + rate * highHigh;
                hhval = Mathf.Max(Mathf.Min(hhval, topval), botval);
                Vector3 temp = rectHighHigh.localPosition;
                temp.y = hhval;
                rectHighHigh.localPosition = temp;
                float ret = (hhval - botval) / (topval - botval);
                mainMat.SetFloat("_CutHeight3", ret);

                float hval = root + rate * high;
                hval = Mathf.Max(Mathf.Min(hval, topval), botval);
                temp = rectHigh.localPosition;
                temp.y = hval;
                rectHigh.localPosition = temp;
                ret = (hval - botval) / (topval - botval);
                mainMat.SetFloat("_CutHeight2", ret);

                float lval = root + rate * low;
                lval = Mathf.Max(Mathf.Min(lval, topval), botval);
                temp = rectLow.localPosition;
                temp.y = lval;
                rectLow.localPosition = temp;
                ret = (lval - botval) / (topval - botval);
                mainMat.SetFloat("_CutHeight1", ret);

                float llval = root + rate * lowLow;
                llval = Mathf.Max(Mathf.Min(llval, topval), botval);
                temp = rectLowLow.localPosition;
                temp.y = llval;
                rectLowLow.localPosition = temp;
                ret = (llval - botval) / (topval - botval);
                mainMat.SetFloat("_CutHeight0", ret);
            }
        }


        public void AddValue(float value)
        {

            //xData.AddLast(0);
            yData.AddLast(value);


            if (yData.Count > listMaxLength)
            {
                //xData.RemoveFirst();
                yData.RemoveFirst();
            }
        }

        public void OnValueChanged(float newValue)
        {
            lastOpcValue = newValue;
        }
    }
}
