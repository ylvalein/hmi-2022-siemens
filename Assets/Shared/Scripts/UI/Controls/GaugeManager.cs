﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Copyright 
// Slice design & production studios
namespace Shared
{
    public class GaugeManager : ControlManager, IControlFloatSingle
    {
        [Header("Marker Reference")]
        public RectTransform rthighHigh;
        public RectTransform rthigh;
        public RectTransform rtlow;
        public RectTransform rtlowLow;

        private Image g_UI;
        private Image gp_UI;
        private Text t_UI, usert_UI;

        private Image endL, endR;
        private Image middleL, middleR;
        private Image beginnL, beginnR;
        private Image l1, l2, l3, l4;

        private Component[] ChildImage;
        private Component[] ChildText;

        private float angularDegree = 268;
        private float? textValue = 100;
        private float maxQuality = 100;//maxrange
        private float old_cValue;
        private string old_userTexT;


        private float value = 100;

        [Header("Text Refference")]
        public Text valueText;
        public Text percent;

        [Header("Value Inputs")]
        public float displayValue = 100;
        public string userText = "kWh Values";

        public int decimalCount = 0;
        public float minValue = 0;
        //public new float maxValue = 100;
        public string measureUnit;

        //for fast solution internal all values would be translated to (0, 100)

        //[Header("Value Range")]
        //public float highHigh = 90;
        //public float high = 80;
        //public float low = 20;
        //public float lowLow = 10;



        public Color32 colorHH = Color.green;
        public Color32 colorH = Color.green;
        public Color32 colorN = Color.green;
        public Color32 colorL = Color.yellow;
        public Color32 colorLL = Color.red;

        private Vector3 rotaBeginn, rotaBeginnR;
        private Vector3 rotaMiddle, rotaMiddleR;

        public float cDrift = 100;

        [HideInInspector]
        private bool isStarted = false;

        private void Start()
        {
            // Access for the correct IMAGE
            ChildImage = GetComponentsInChildren<Image>();

            // Reference to left side
            foreach (Component c in ChildImage)
                if (c.tag == "QualityEnd")
                    endL = c.GetComponent<Image>();
            foreach (Component c in ChildImage)
                if (c.tag == "QualityMiddle")
                    middleL = c.GetComponent<Image>();
            foreach (Component c in ChildImage)
                if (c.tag == "QualityBeginn")
                    beginnL = c.GetComponent<Image>();

            // Reference to right side
            foreach (Component c in ChildImage)
                if (c.tag == "QualityREnd")
                    endR = c.GetComponent<Image>();
            foreach (Component c in ChildImage)
                if (c.tag == "QualityRMiddle")
                    middleR = c.GetComponent<Image>();
            foreach (Component c in ChildImage)
                if (c.tag == "QualityRBeginn")
                    beginnR = c.GetComponent<Image>();

            // Referemce lines
            foreach (Component c in ChildImage)
            {
                if (c.tag == "1")
                    l1 = c.GetComponent<Image>();
                if (c.tag == "2")
                    l2 = c.GetComponent<Image>();
                if (c.tag == "3")
                    l3 = c.GetComponent<Image>();
                if (c.tag == "4")
                    l4 = c.GetComponent<Image>();
            }



            // Reference to blue circle
            foreach (Component c in ChildImage)
                if (c.tag == "QualityPointer")
                    gp_UI = c.GetComponent<Image>();

            // Access for the correct IMAGE
            ChildText = GetComponentsInChildren<Text>();

            foreach (Component c in ChildText)
            {
                if (c.tag == "QualityPercent")
                    t_UI = c.GetComponent<Text>();
                if (c.tag == "QualityName")
                    usert_UI = c.GetComponent<Text>();
            }


            // Set the angular degree of danger (red) and warning (yellow) bar
            Vector3 rotaStart = new Vector3(0, 0, 0);
            endL.transform.localEulerAngles = rotaStart;
            endR.transform.localEulerAngles = rotaStart;
            
            // Green right
            var normalisedHight = ((Mathf.Clamp(high, minValue, maxValue) - minValue) / (maxValue - minValue)) * 100;
            var a = ((normalisedHight * angularDegree) / maxQuality);
            rotaBeginnR = new Vector3(0, 0, (angularDegree - a));
            beginnR.transform.localEulerAngles = rotaBeginnR;
            l1.transform.localEulerAngles = rotaBeginnR;
            // Yellow right
            var normalisedHightHight = ((Mathf.Clamp(highHigh, minValue, maxValue) - minValue) / (maxValue - minValue)) * 100;
            var b = (normalisedHightHight * angularDegree) / maxQuality;
            rotaMiddleR = new Vector3(0, 0, (angularDegree - b));
            middleR.transform.localEulerAngles = rotaMiddleR;
            l2.transform.localEulerAngles = rotaMiddleR;


            // Red left
            var normalisedLowLow = ((Mathf.Clamp(lowLow, minValue, maxValue) - minValue) / (maxValue - minValue)) * 100;
            int w = (int)(((49.5f + normalisedLowLow) * angularDegree) / maxQuality);
            rotaBeginn = new Vector3(0, 0, (angularDegree - w));
            beginnL.transform.localEulerAngles = rotaBeginn;
            int w1 = (int)(((normalisedLowLow - 1) * angularDegree) / maxQuality);
            Vector3 ww1 = new Vector3(0, 0, (angularDegree - w1));
            l3.transform.localEulerAngles = ww1;
            // Yellow left
            var normalisedLow = ((Mathf.Clamp(low, minValue, maxValue) - minValue) / (maxValue - minValue)) * 100;
            var v = (((49.5f + normalisedLow) * angularDegree) / maxQuality);
            rotaMiddle = new Vector3(0, 0, (angularDegree - v));
            middleL.transform.localEulerAngles = rotaMiddle;
            var v1 = (((normalisedLow - 1) * angularDegree) / maxQuality);
            Vector3 vv1 = new Vector3(0, 0, (angularDegree - v1));
            l4.transform.localEulerAngles = vv1;

            isStarted = true;
            OnValueChanged(0.0f);

        }

        public void OnValueChanged(float newValue)
        {
            if (!isStarted) return;
            value = newValue;
            displayValue = value;
            SetValue(value);

            FillLevelCalculation();
            SetBlueRota(PointerRotate());
            SetGreenRota();



            var normalisedValue = ((value - minValue) / (maxValue - minValue)) * 100;

            /*
            rthighHigh.gameObject.GetComponent<Image>().color = colorHH;
            rthigh.gameObject.GetComponent<Image>().color = colorH;
            rtlow.gameObject.GetComponent<Image>().color = colorL;
            rtlowLow.gameObject.GetComponent<Image>().color = colorLL;
            */

            // values
            if (value != old_cValue)
            {
                SetValue(value);

                FillLevelCalculation();
                if (normalisedValue >= 0f)
                    SetBlueRota(PointerRotate());
                else
                    SetBlueRota(new Vector3(0f, 0f, angularDegree));

                SetGreenRota();

                old_cValue = value;
            }

            // Text
            if (old_userTexT != userText)
            {
                usert_UI.text = userText;
                old_userTexT = userText;
            }

            // Color
            endL.color = colorN;
            endR.color = colorHH;
            middleL.color = colorL;
            middleR.color = colorH;
            beginnL.color = colorLL;
            beginnR.color = colorN;

            if (!string.IsNullOrEmpty(measureUnit))
            {
                valueText.transform.localPosition = new Vector3(-104, valueText.transform.localPosition.y, valueText.transform.localPosition.z);
                valueText.alignment = TextAnchor.MiddleRight;
                percent.gameObject.SetActive(true);
                percent.text = measureUnit;
            }
            else
            {
                valueText.transform.localPosition = new Vector3(0, valueText.transform.localPosition.y, valueText.transform.localPosition.z);
                valueText.alignment = TextAnchor.MiddleCenter;
                percent.gameObject.SetActive(false);
            }

        }

        // Get the user Inputs
        public void SetValue(float current)
        {
            if (current > maxQuality)
            {
                value = 0;
                textValue = null;
            }
            else if (current <= maxQuality && current >= minValue)
            {
                value = current;
                textValue = current;
                
            }
        }

        // Passing current value into the graph as a percentage
        // Passing the right values into the text
        private void FillLevelCalculation()
        {

            // current value / max value -> bsp. [ 100/100]
            if (textValue == null)
                t_UI.text = "--";
            else
                t_UI.text = string.Format("{0:F" + decimalCount + "}", textValue);// ((int)textValue).ToString();


        }

        // Passing current value into the graph as a percentage
        // Passing the right values into the text
        private Vector3 PointerRotate()
        {

            var normalisedValue = ((value - minValue) / (maxValue - minValue)) * 100;
            

            // Rule of three calculation is used for the blue circle
            int w = (int)((normalisedValue * angularDegree) / maxQuality);

            // Because 0% equals 280° the result has to be subtracted from 100%/280°
            // so the circle can rotate correctly. So values get mirrored for the correct percentage.
            Vector3 r_Values = new Vector3(0, 0, (angularDegree - w));

            return r_Values;

        }

        // Blue circle movement
        private void SetBlueRota(Vector3 value)
        {

            // Value can be filled by the user
            Vector3 to = value;

            //override the current position with the new value 
            gp_UI.transform.localEulerAngles = to;
        }

        // Green circle movement
        private void SetGreenRota()
        {
            // If it is more than 60%, the right bar will move
            var normalisedHightHight = ((Mathf.Clamp(highHigh, minValue, maxValue) - minValue) / (maxValue - minValue)) * 100;
            var normalisedValue = ((value - minValue) / (maxValue - minValue)) * 100;


            if (normalisedValue >= normalisedHightHight)
            {
                // Calculating the angular degree
                int w = (int)((normalisedValue * angularDegree) / maxQuality);

                // Set the correct values 
                Vector3 rotaRight = new Vector3(0, 0, (angularDegree - w));
                Vector3 rotaEnd = new Vector3(0, 0, 0);

                // right side
                endR.enabled = true;
                middleR.enabled = true;
                beginnR.enabled = true;
                endR.transform.localEulerAngles = rotaRight;
                middleR.transform.localEulerAngles = rotaMiddleR;
                beginnR.transform.localEulerAngles = rotaBeginnR;

                //left side
                middleL.enabled = true;
                endL.enabled = true;
                beginnL.enabled = true;
                endL.transform.localEulerAngles = rotaEnd;
                middleL.transform.localEulerAngles = rotaMiddle;
                beginnL.transform.localEulerAngles = rotaBeginn;
            }

            if (normalisedValue < normalisedHightHight + 1f)
            {
                // Calculating the angular degree
                int w = (int)((normalisedValue * angularDegree) / maxQuality);

                // Set the correct values 
                Vector3 rotaRight = new Vector3(0, 0, (angularDegree - w));
                Vector3 rotaEnd = new Vector3(0, 0, 0);

                // right side
                endR.enabled = false;
                middleR.enabled = true;
                beginnR.enabled = true;
                endR.transform.localEulerAngles = rotaRight;
                middleR.transform.localEulerAngles = rotaRight;
                beginnR.transform.localEulerAngles = rotaBeginnR;

                //left side
                middleL.enabled = true;
                endL.enabled = true;
                beginnL.enabled = true;
                endL.transform.localEulerAngles = rotaEnd;
                middleL.transform.localEulerAngles = rotaMiddle;
                beginnL.transform.localEulerAngles = rotaBeginn;
            }

            var normalisedHight = ((Mathf.Clamp(high, minValue, maxValue) - minValue) / (maxValue - minValue)) * 100;

            if (normalisedValue >= 50f && normalisedValue < normalisedHight + 1f)
            {
                // Calculating the angular degree
                int w = (int)((normalisedValue * angularDegree) / maxQuality);

                // Set the correct values 
                Vector3 rotaRight = new Vector3(0, 0, (angularDegree - w));
                Vector3 rotaEnd = new Vector3(0, 0, 0);

                // right side
                endR.enabled = false;
                middleR.enabled = false;
                beginnR.enabled = true;
                endR.transform.localEulerAngles = rotaRight;
                middleR.transform.localEulerAngles = rotaRight;
                beginnR.transform.localEulerAngles = rotaRight;

                //left side
                middleL.enabled = true;
                endL.enabled = true;
                beginnL.enabled = true;
                endL.transform.localEulerAngles = rotaEnd;
                middleL.transform.localEulerAngles = rotaMiddle;
                beginnL.transform.localEulerAngles = rotaBeginn;
            }

            if (normalisedValue < 50f)
            {
                // Calculating the angular degree
                int w = (int)(((49.5f + normalisedValue) * angularDegree) / maxQuality);
                // Set the correct values 
                Vector3 rotaRight = new Vector3(0, 0, 150);
                Vector3 rotaEnd = new Vector3(0, 0, (angularDegree - w));

                // right side
                endR.enabled = false;
                middleR.enabled = false;
                beginnR.enabled = false;
                endR.transform.localEulerAngles = rotaRight;
                middleR.transform.localEulerAngles = rotaRight;
                beginnR.transform.localEulerAngles = rotaRight;

                //left side
                middleL.enabled = true;
                endL.enabled = true;
                beginnL.enabled = true;
                endL.transform.localEulerAngles = rotaEnd;
                middleL.transform.localEulerAngles = rotaMiddle;
                beginnL.transform.localEulerAngles = rotaBeginn;
            }

            var normalisedLow = ((Mathf.Clamp(low, minValue, maxValue) - minValue) / (maxValue - minValue)) * 100;

            if (normalisedValue < normalisedLow + 1f)
            {
                // Calculating the angular degree
                int w = (int)(((49.5f + normalisedValue) * angularDegree) / maxQuality);
                // Set the correct values 
                Vector3 rotaRight = new Vector3(0, 0, 150);
                Vector3 rotaEnd = new Vector3(0, 0, (angularDegree - w));

                // right side
                endR.transform.localEulerAngles = rotaRight;
                middleR.transform.localEulerAngles = rotaRight;
                beginnR.transform.localEulerAngles = rotaRight;

                //left side
                endL.enabled = false;
                middleL.enabled = true;
                beginnL.enabled = true;
                endL.transform.localEulerAngles = rotaEnd;
                middleL.transform.localEulerAngles = rotaEnd;
                beginnL.transform.localEulerAngles = rotaBeginn;

            }

            //  If it is less than 50%, the left bar will move (green & yellow & red)
            if (normalisedValue < lowLow + 1f)
            {
                // Calculating the angular degree
                int w = (int)(((49.5f + normalisedValue) * angularDegree) / maxQuality);
                // Set the correct values 
                Vector3 rotaRight = new Vector3(0, 0, 150);
                Vector3 rotaEnd = new Vector3(0, 0, (angularDegree - w));

                // right side
                endR.transform.localEulerAngles = rotaRight;
                middleR.transform.localEulerAngles = rotaRight;
                beginnR.transform.localEulerAngles = rotaRight;

                //left side
                endL.enabled = false;
                middleL.enabled = false;
                beginnL.enabled = true;
                endL.transform.localEulerAngles = rotaEnd;
                middleL.transform.localEulerAngles = rotaEnd;
                beginnL.transform.localEulerAngles = rotaEnd;
            }

            if (normalisedValue < 0)
            {
                //left side
                endL.enabled = false;
                middleL.enabled = false;
                beginnL.enabled = false;
            }

        }


    }
}