﻿using Shared;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlManager : MonoBehaviour /*: PersistentObject*/
{

    [Tooltip("Save the instance of this object in an xml to load it again after application quit")]
    public bool shouldBeSavedForNextStart = true;

    [Tooltip("Check if you use the ExpertmodePrefab with Container around the Control (Chart)")]
    public bool isExpertModePrefab = false;

    //[SerializeField]
    //public ChartType chartType;

    [Header("Value Range")]
    public float highHigh = 0;
    public float high = 0;
    public float low = 0;
    public float lowLow = 0;

    public float maxValue = 0;


    //private void AddControlToSavedCharts()
    //{
    //    Chart chart;

    //    //If it is the expertmodeprefab we need to save the transform of the root object instead of the Chart itself
    //    if (isExpertModePrefab)
    //    {
    //        chart = new Chart(chartType, Id, transform.parent.parent.localPosition, transform.parent.parent.localRotation, transform.parent.parent.localScale, highHigh, high, low, lowLow, maxValue, isExpertModePrefab);
    //    }
    //    else
    //    {
    //        chart = new Chart(chartType, Id, transform.localPosition, transform.localRotation, transform.localScale, highHigh, high, low, lowLow, maxValue, isExpertModePrefab);
    //    }


    //    if (ChartSaver.Instance.DictionaryToSave.ContainsKey(chart.Id))
    //    {
    //        ChartSaver.Instance.DictionaryToSave.Remove(chart.Id);
    //    }

    //    if (!ChartSaver.Instance.DictionaryToSave.ContainsKey(chart.Id))
    //        ChartSaver.Instance.DictionaryToSave.Add(chart.Id, chart);
    //}

    //#if UNITY_EDITOR 

    //    private void OnApplicationQuit()
    //    {
    //        if (shouldBeSavedForNextStart)
    //            AddControlToSavedCharts();
    //    }

    //#else

    //    private void OnApplicationFocus(bool focus)
    //    {
    //        if (shouldBeSavedForNextStart)
    //            AddControlToSavedCharts();
    //    }

    //#endif
}
