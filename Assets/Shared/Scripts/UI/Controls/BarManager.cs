﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Shared
{
    public class BarManager : ControlManager
    {
        private const int MIN_BAR_COUNT = 1;
        private const int MAX_BAR_COUNT = 10;

        [Header("Bargraph Reference")]
        public RectTransform rectHighHigh;
        public RectTransform rectHigh;
        public RectTransform rectLow;
        public RectTransform rectLowLow;
        public RawImage mainPic;
        public RawImage r;
        public RectTransform textContainer;
        public Text t1;
        public Material m1ref, m2ref;
        private Material m1, m2;
        public Text maxtext, middletext, mintext;
        public Text label;

        public RawImage BarMain;
        public RawImage BarBack;

        [Header("Set Layout")]
        [HideInInspector]
        public float[] values;
        public int sizeofvalues;
        public int maxHeight;
        public Canvas canvas;
        Text[] mText;
        //bool b = false;


        [Range(0, 1)]
        public float barWidth = 0.5f;
        [Range(0, 1)]
        public float barBackWidth = 0.6f;

        [Range(0, 1)]
        public float allBarWidth = 1;
        [Range(0, 1)]
        public float allBarHeight = 1;

        public int decimalPlacesText = 2;

        public float textScale = 0.1f;
        public float textYOffset = -1;

        public Text textValueLow;
        public Text textValueMid;
        public Text textValueHigh;

        public Color32 colorHH = Color.red;
        public Color32 colorH = Color.yellow;
        public Color32 colorN = Color.green;
        public Color32 colorL = Color.yellow;
        public Color32 colorLL = Color.red;


        private Texture2D ret1;
        private Texture2D ret2;
        private List<Text> _textPool;
        private float oldTextSize;

        [HideInInspector]
        private bool isInitialized = false;
        [HideInInspector]
        private bool isAwakeInitialized = false;
        public BarManager()
        {
            _textPool = new List<Text>();
        }

        // Use this for initialization
        void Awake()
        {

            m1 = new Material(m1ref);
            m2 = new Material(m2ref);
            BarMain.material = m1;
            BarBack.material = m2;

            Init();
            isAwakeInitialized = true;
        }

        public void Init()
        {
            values = new float[sizeofvalues];

            ret1 = new Texture2D(values.Length, 1);
            ret1.filterMode = FilterMode.Point;
            ret2 = new Texture2D(values.Length, 1);
            ret2.filterMode = FilterMode.Point;

            //maxtext.text = ((int)(maxHeight * 2 / 3)).ToString();
            maxtext.text = ((int)(maxHeight)).ToString();
            middletext.text = ((int)(maxHeight * 1 / 2)).ToString();

            InitText();
            isInitialized = true;
            UpdateTexture();
        }


        public void UpdateTexture()
        {
            if (!isInitialized) return;
            for (int i = 0; i < values.Length; i++)
            {
                mText[i].text = values[i].ToString() + "/" + maxHeight.ToString();
            }

            // Set Bars
            for (int i = 0; i < values.Length; i++)
            {
                ret1.SetPixel(i, 1, new Color32(255, (byte)(Mathf.Min(Mathf.Max(values[i] / maxHeight * 255, 0), 255)), 0, 0));
                ret2.SetPixel(i, 1, new Color32(255, 255, 0, 0));
            }
            if (!isAwakeInitialized) return;
            ret1.Apply();
            ret2.Apply();
            m1.SetTexture("_Arrays", ret1);  //SETFLOAT
            m2.SetTexture("_Arrays", ret2);
            m1.SetColor("_Color5", colorHH);
            m1.SetColor("_Color4", colorH);
            m1.SetColor("_Color3", colorN);
            m1.SetColor("_Color2", colorL);
            m1.SetColor("_Color1", colorLL);

            //var maxColumnWidth = r.rectTransform.rect.width / values.Length;
            m1.SetFloat("_BarWidth", Mathf.Clamp01(barWidth));
            m2.SetFloat("_BarWidth", Mathf.Clamp01(barBackWidth));

            m1.SetFloat("_BarContainerWidth", Mathf.Clamp01(allBarWidth));
            m2.SetFloat("_BarContainerWidth", Mathf.Clamp01(allBarWidth));

            m1.SetFloat("_BarContainerHeight", Mathf.Clamp01(allBarHeight));
            m2.SetFloat("_BarContainerHeight", Mathf.Clamp01(allBarHeight));

            // Set Text
            if (!TextInited)
            {
                InitText();
            }

            var maxWidth = mainPic.rectTransform.rect.width * allBarWidth;

            var step = maxWidth / (mText.Length);
            for (var i = 0; i < mText.Length; i++)
            {
                var t = mText[i];
                t.rectTransform.localScale = Vector3.one * (textScale);
                t.rectTransform.localRotation = Quaternion.Euler(Vector3.zero);

                t.rectTransform.localPosition = new Vector3(-(maxWidth / 2) + i * step + step / 2, -(mainPic.rectTransform.rect.height / 2) + textYOffset, 0 /*-35 - bn, 0 - (r.rectTransform.rect.height / 2) - 3, 0*/);

                UpdateText(t, values[i]);
                //t.text = values[i] + "/" + maxHeight;
            }


            float min = 0;
            float max = maxHeight;

            float factor = 1;//25f / 27f;
            float topval = mainPic.rectTransform.rect.height * factor / 2 + mainPic.rectTransform.localPosition.y;
            float botval = mainPic.rectTransform.rect.height * factor / -2 + mainPic.rectTransform.localPosition.y;
            if (topval - botval != 0)
            {

                float root = topval - 1 / (1 - (min / max)) * (topval - botval);
                float rate = (topval - botval) / (max - min);


                var hhval = root + rate * highHigh;
                hhval = Mathf.Max(Mathf.Min(hhval, topval), botval);
                //temp.y = hhval;
                //highHigh.localPosition = temp;
                var ret = (hhval - botval) / (topval - botval);

                rectHighHigh.anchorMin = new Vector2(0, ret);
                rectHighHigh.anchorMax = new Vector2(1, ret);
                rectHighHigh.anchoredPosition = Vector2.zero;
                m1.SetFloat("_CutHeight3", ret);

                var hval = root + rate * high;
                hval = Mathf.Max(Mathf.Min(hval, topval), botval);
                ret = (hval - botval) / (topval - botval);

                rectHigh.anchorMin = new Vector2(0, ret);
                rectHigh.anchorMax = new Vector2(1, ret);
                rectHigh.anchoredPosition = Vector2.zero;
                m1.SetFloat("_CutHeight2", ret);

                var lval = root + rate * low;
                lval = Mathf.Max(Mathf.Min(lval, topval), botval);
                ret = (lval - botval) / (topval - botval);

                rectLow.anchorMin = new Vector2(0, ret);
                rectLow.anchorMax = new Vector2(1, ret);
                rectLow.anchoredPosition = Vector2.zero;
                m1.SetFloat("_CutHeight1", ret);

                var llval = root + rate * lowLow;
                llval = Mathf.Max(Mathf.Min(llval, topval), botval);
                ret = (llval - botval) / (topval - botval);
                rectLowLow.anchorMin = new Vector2(0, ret);
                rectLowLow.anchorMax = new Vector2(1, ret);
                //lowLow.anchorMin = new Vector2(0, ret);
                //lowLow.anchorMax = new Vector2(1, ret);
                rectLowLow.anchoredPosition = Vector2.zero;
                m1.SetFloat("_CutHeight0", ret);
            }
        }

        private void UpdateText(Text text, float value)
        {
            float roundedValue = Convert.ToSingle(System.Math.Round(value, decimalPlacesText));

            text.text = roundedValue + "/" + maxHeight;
        }

        private bool TextInited
        {
            get { return mText != null && mText.Length == values.Length; }
        }

        private void InitText()
        {/*
        float maxHeightPart = maxHeight / 3;
        textValueLow.text = Mathf.RoundToInt(maxHeightPart).ToString();
        textValueMid.text = Mathf.RoundToInt(maxHeightPart*2).ToString();
        textValueHigh.text = Mathf.RoundToInt(maxHeightPart*3).ToString();
        */
            if (mText != null)
            {
                foreach (var text in mText)
                {
                    text.gameObject.SetActive(false);
                    _textPool.Add(text);
                }
            }

            float x = values.Length;
            //float a = 1 / (2 * 3 + (x - 1) * 4);
            mText = new Text[values.Length];

            for (int i = 0; i < mText.Length; i++)
            {
                Text t = CreateTextObj();
                t.transform.SetParent(textContainer, false);
                mText[i] = t;

                UpdateText(t, values[i]);
            }
        }

        private Text CreateTextObj()
        {
            if (_textPool.Count > 0)
            {
                var text = _textPool[_textPool.Count - 1];
                _textPool.RemoveAt(_textPool.Count - 1);
                text.gameObject.SetActive(true);
                return text;
            }

            var obj = Instantiate(t1, new Vector3(0, 0, 0), Quaternion.identity);
            return obj;
        }


        public void SetValue(int i, float value)
        {
            if (i < 0 || i >= values.Length)
            {
                throw new Exception("Invalid bar index");
            }
        }

        public void SetBarComplete(float[] bars)
        {
            if (bars.Length < MIN_BAR_COUNT || bars.Length > MAX_BAR_COUNT)
            {
                throw new Exception(string.Format("Invalid Bars Count : {0}. Available Range [{1}, {2}]", bars.Length, MIN_BAR_COUNT, MAX_BAR_COUNT));
            }
            values = bars;
        }

        public void OnValueChanged(int index, float newValue, bool updateTexture = true)
        {
            values[index] = newValue;
            if (updateTexture) UpdateTexture();
        }

        public void SetNumberOfBars(int numBars)
        {
            sizeofvalues = numBars;
            Init();
        }

        public void SetMaximumValue(float maxBarValue)
        {
            maxHeight = Mathf.CeilToInt(maxBarValue);
            Init();
        }
    }
}