﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Shared
{
    public class StatusText : MonoBehaviour
    {

        Text textStatus;

        private void Start()
        {
            textStatus = GetComponent<Text>();
        }

        public void SetText(string text, bool shouldLog = false)
        {
            InitText();
            textStatus.text = text;
            if (shouldLog) Debug.Log(text);
        }

        public void SetColor(Color color)
        {
            InitText();
            textStatus.color = color;
        }

        public void InitText()
        {
            if (textStatus == null) textStatus = GetComponent<Text>();
        }
    }
}
