﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

[Serializable]
public class GraphicalChangeObject {

    public Texture Texture;
    public Color NormalColor;
    public Color HighlightedColor;
    public Color ActiveColor;

}
