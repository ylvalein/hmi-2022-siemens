﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Shared
{
    public class ChangeColor : MonoBehaviour
    {

        public Color outline;
        protected Color lLast;

        protected List<RawImage> outlineList = new List<RawImage>();

        // Use this for initialization
        protected virtual void Awake()
        {
            FillOutlineList();
        }

        // Update is called once per frame
        protected virtual void Update()
        {

            ChangeOutlineColor();
        }

        protected void FillOutlineList()
        {
            foreach (Component c in GetComponentsInChildren<RawImage>())
            {
                string test = c.name;

                //Lines
                if (test.Contains("Border")) //outline
                    outlineList.Add(c.GetComponent<RawImage>());


            }
        }

        protected void ChangeOutlineColor()
        {
            if (outline != lLast)
            {
                foreach (RawImage raw in outlineList)
                    raw.color = outline; lLast = outline;
            }
        }
    }
}
