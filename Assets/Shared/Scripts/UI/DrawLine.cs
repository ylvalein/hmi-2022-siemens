﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    public class DrawLine : MonoBehaviour
    {

        // Use this for initialization
        private LineRenderer line;
        public Material Material_Line;

        public Transform startPosition;
        public GameObject targetObject = null;

        public Vector3 positionOffset;

        void Start()
        {
            if (startPosition == null) startPosition = transform;
            line = this.gameObject.AddComponent<LineRenderer>();
            //line.transform.parent = this.transform;
            // Set the width of the Line Renderers
            line.startWidth = 0.005F;
            line.endWidth = 0.005F;
            // Set the number of vertex fo the Line Renderer
            line.positionCount = 2;
            line.material = Material_Line;
            //line.useWorldSpace = false;
            line.startColor = Color.blue;
            line.endColor = Color.blue;

            if (targetObject == null) targetObject = transform.parent.gameObject;
        }

        // Update is called once per frame
        void Update()
        {
            if (targetObject == null) return;
            //line.SetPosition(1, gameObject.transform.parent.TransformPoint(gameObject.transform.parent.GetComponent<BoxCollider>().center));
            line.SetPosition(0, startPosition.position + positionOffset);
            line.SetPosition(1, targetObject.transform.position);
        }
    }
}