﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.Input;

namespace Shared
{
    public class DisableWhenLookingAtObject : MonoBehaviour, IMixedRealityFocusHandler
    {

        public Behaviour[] toDisableWhenFocused;

        public void OnFocusEnter(FocusEventData eventData)
        {
            if (toDisableWhenFocused != null)
            {
                foreach (Behaviour behav in toDisableWhenFocused)
                {
                    behav.enabled = false;

                }
            }
        }


        public void OnFocusExit(FocusEventData eventData)
        {
            if (toDisableWhenFocused != null)
            {
                foreach (Behaviour behav in toDisableWhenFocused)
                {
                    behav.enabled = true;
                }
            }
        }
    }
}
