﻿using Microsoft.MixedReality.Toolkit.Input;
using UnityEngine;

namespace Shared
{
    public class ButtonAnimation : MonoBehaviour, IMixedRealityFocusHandler
    {

        private const string ANIMATION_NAME = "LookAt";

        public void cancelAnimation()
        {
            if (GetComponent<Animator>() != null)
            {
                GetComponent<Animator>().SetBool(ANIMATION_NAME, false);
            }
        }

        public void OnFocusEnter(FocusEventData eventData)
        {
            if (GetComponent<Animator>() != null)
            {
                GetComponent<Animator>().SetBool(ANIMATION_NAME, true);
            }
        }

        public void OnFocusExit(FocusEventData eventData)
        {
            cancelAnimation();
        }
    }
}
