﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if WINDOWS_UWP 
using Windows.Devices.Power;
#endif
namespace Shared
{
    public class BatteryStats : MonoBehaviour
    {
        [Tooltip("Show Battery stats at the screen")]
        public bool showBatteryStats = false;

        public uint refreshRateSeconds;

        public Text textStatus;
        public Text textCapacity;
        public Text textChargeRate;

        public Image batteryImage;
        public RawImage lightning;
        public Color batteryColor;
        //public double fillAmount = 1; //for testing

        public void updateBatteryStats()
        {

#if WINDOWS_UWP
        var aggBattery = Battery.AggregateBattery;
        var report = aggBattery.GetReport();

        string status = report.Status.ToString();
        string capacityCurrent = report.RemainingCapacityInMilliwattHours.ToString();
        string capacityFull = report.FullChargeCapacityInMilliwattHours.ToString();
        string chargeRate = report.ChargeRateInMilliwatts.ToString();
        float batteryCapacityCur = (float)report.RemainingCapacityInMilliwattHours;
        float batteryCapacityMax = (float)report.FullChargeCapacityInMilliwattHours;

#else
            string status = "Not Available";
            string capacityCurrent = "";
            string capacityFull = "";
            string chargeRate = "";
            float batteryCapacityCur = 1;
            float batteryCapacityMax = 1;
#endif

            textStatus.text = status;


            //textCapacity.text = capacityCurrent + " / " + capacityFull;

            double fillAmount = System.Math.Round((batteryCapacityCur / batteryCapacityMax), 3);
            batteryImage.fillAmount = (float)fillAmount;
            textCapacity.text = System.Math.Round(fillAmount * 100).ToString() + "%";

            if (status.Equals("Charging"))
            {
                lightning.enabled = true;
                batteryImage.color = Color.green;
            }
            else
            {
                lightning.enabled = false;
                if (fillAmount <= 0.1)
                    batteryImage.color = Color.red;
                batteryImage.color = batteryColor;
            }

            textChargeRate.text = chargeRate;
        }

        float currentWaitTime = 0;

        public void Update()
        {
            if (!showBatteryStats)
                return;

            currentWaitTime += Time.deltaTime;
            if (currentWaitTime > refreshRateSeconds)
            {
                currentWaitTime = 0;
                updateBatteryStats();
            }
        }
    }
}
