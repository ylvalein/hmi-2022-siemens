﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace Shared
{
    [Serializable]
    public class GraphicalChangeButtonMemory
    {

        public RawImage GraphicalObject;
        public Color NormalColor;
        public Color HighlightedColor;
        public Color ActiveColor;

        public void setColor(bool active)
        {
            if (active) GraphicalObject.color = ActiveColor;
            else GraphicalObject.color = NormalColor;
        }
    }
}
