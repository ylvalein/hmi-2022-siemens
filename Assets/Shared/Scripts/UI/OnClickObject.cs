﻿using UnityEngine;
using UnityEngine.Events;
using Microsoft.MixedReality.Toolkit.Input;

[AddComponentMenu("AREditor/UI/OnClickObject")]
public class OnClickObject : MonoBehaviour, IMixedRealityPointerHandler, IMixedRealityFocusHandler
{
    public UnityEvent OnObjectCliked;
    public UnityEvent OnObjectFocus;
    public UnityEvent OnObjectFocusExit;

    public void OnFocusEnter(FocusEventData eventData)
    {
        OnObjectFocus.Invoke();
    }

    public void OnFocusExit(FocusEventData eventData)
    {
        OnObjectFocusExit.Invoke();
    }

    public void OnPointerClicked(MixedRealityPointerEventData eventData)
    {
        OnObjectCliked.Invoke();
    }

    public void OnPointerDown(MixedRealityPointerEventData eventData)
    {

    }

    public void OnPointerDragged(MixedRealityPointerEventData eventData)
    {

    }

    public void OnPointerUp(MixedRealityPointerEventData eventData)
    {

    }
}
