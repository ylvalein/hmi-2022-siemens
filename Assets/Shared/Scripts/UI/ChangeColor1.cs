﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Shared
{
    public class ChangeColor1 : ChangeColor
    {

        public Color iconM;
        protected Color iLastM;

        public Color iconE;
        protected Color iLastE;

        [SerializeField]
        public List<RawImage> iconListMainElement = new List<RawImage>();
        public List<RawImage> iconListExtraElement = new List<RawImage>();

        // Use this for initialization
        protected override void Awake()
        {
            FillIconList();
        }

        // Update is called once per frame
        protected override void Update()
        {
            ChangeIconColor();
        }

        protected void FillIconList()
        {
            foreach (Component c in GetComponentsInChildren<RawImage>())
            {

                string test = c.name;

                //Icon
                if (test.Contains("IconMain"))
                    iconListMainElement.Add(c.GetComponent<RawImage>());

                if (test.Contains("IconExtra"))
                    iconListExtraElement.Add(c.GetComponent<RawImage>());
            }
        }

        protected void ChangeIconColor()
        {
            if (iconM != iLastM)
            {
                foreach (RawImage raw in iconListMainElement)
                    raw.color = iconM; iLastM = iconM;

            }

            if (iconE != iLastE)
            {
                foreach (RawImage raw in iconListExtraElement)
                    raw.color = iconE; iLastE = iconM;

            }
        }
    }
}
