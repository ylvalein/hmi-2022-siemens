﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    public class MoveHoloboardCollection : MonoBehaviour
    {
        public void Disable()
        {
            //transform.rotation = Quaternion.Euler (0, 0, 0);
            this.enabled = false;
        }

        // Update is called once per frame
        void Update()
        {
            transform.LookAt(new Vector3(Camera.main.gameObject.transform.position.x, transform.position.y, Camera.main.transform.position.z));
        }

    }
}
