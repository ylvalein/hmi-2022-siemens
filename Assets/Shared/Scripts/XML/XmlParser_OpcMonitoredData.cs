﻿using System.Collections.Generic;
using System.Xml;
using OpcUa_Siemens_Client;

namespace Shared {
    public class XmlParser_OpcMonitoredData : AbstractXmlParser {

        public OpcSettings settings;
        public List<MonitoredDataBase> opcValues;

        protected override void DoParse(XmlDocument xmlDoc) {
            settings = XmlParserHelper.ParseConnectionSettings(xmlDoc);
            opcValues = XmlParserHelper.ParseMonitoredData(xmlDoc.GetElementsByTagName("OPCValues")[0]);
        }
    }
}

