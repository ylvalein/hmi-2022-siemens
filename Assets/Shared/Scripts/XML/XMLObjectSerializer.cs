﻿using System.IO;
using System.Xml.Serialization;

namespace Shared
{
    public class XMLObjectSerializer
    {

        public static string SerializeObjectToXMLString<T>(T obj) where T : class
        {
            var stringWriter = new StringWriter();
            var serializer = new XmlSerializer(typeof(T));

            serializer.Serialize(stringWriter, obj);
            return stringWriter.ToString();
        }

        public static T Deserialize<T>(string input) where T : class
        {
            XmlSerializer ser = new XmlSerializer(typeof(T));

            using (StringReader sr = new StringReader(input))
            {
                return (T)ser.Deserialize(sr);
            }
        }
    }
}
