﻿using OpcUa_Siemens_Client;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

namespace Shared
{
    public class XmlParser_OpcMonitoredDataWithTypeAndId : AbstractXmlParser
    {

        public OpcSettings settings;
        public List<MonitoredDataBase> opcValues;

        protected override void DoParse(XmlDocument xmlDoc)
        {
            settings = XmlParserHelper.ParseConnectionSettings(xmlDoc);
            opcValues = XmlParserHelper.ParseMonitoredDataWithTypeAndId(xmlDoc.GetElementsByTagName("OPCValues")[0]);
        }
    }
}
