﻿using System.Collections.Generic;
using System.Xml;
using OpcUa_Siemens_Client;

namespace Shared
{
    public class XmlParser_OpcGenericData : AbstractXmlParser
    {

        public OpcSettings settings;
        public List<GenericDataBase> opcValues;

        protected override void DoParse(XmlDocument xmlDoc)
        {
            settings = XmlParserHelper.ParseConnectionSettings(xmlDoc);
            opcValues = XmlParserHelper.ParseGenericData(xmlDoc.GetElementsByTagName("OPCValues")[0]);
            OpcManager.AssignSession(settings, opcValues);
        }
    }
}

