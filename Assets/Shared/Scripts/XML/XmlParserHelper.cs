﻿using OpcUa_Siemens_Client;
using System;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

namespace Shared
{
    public class XmlParserHelper
    {

        private static string GetValue(XmlNode parent, string identifier)
        {
            return AbstractXmlParser.GetValue(parent, identifier);
        }

        public static OpcSettings ParseConnectionSettings(XmlDocument xmlDoc)
        {
            XmlNodeList nodesParameters = xmlDoc.GetElementsByTagName("OPCParameters");
            if (nodesParameters.Count != 1) Debug.LogError("Malformated Xml, more than one parameters tag");
            var node = nodesParameters[0];

            return new OpcSettings(GetValue(node, "ServerEndpointUrl"), int.Parse(GetValue(node, "SubscriptionTime")));
        }

        public static List<GenericDataBase> ParseGenericData(XmlNode parentNode)
        {
            List<GenericDataBase> opcValues = new List<GenericDataBase>();

            XmlNodeList opcValueNodes = parentNode.ChildNodes;

            foreach (XmlNode opcValueNode in opcValueNodes)
            {
                System.Type datatype = ConvertDatatypeFromString(GetValue(opcValueNode, "Datatype"));

                var opcValue = CreateGenericData(
                    datatype,
                    GetValue(opcValueNode, "Name"),
                    GetValue(opcValueNode, "OpcNodeId")
                );
                /*
                var opcValue = new GenericData(
                    GetValue(opcValueNode, "Name"),
                    GetValue(opcValueNode, "OpcNodeId"));
                    */
                opcValues.Add(opcValue);
            }

            return opcValues;
        }

        public static List<MonitoredDataBase> ParseMonitoredData(XmlNode parentNode)
        {
            List<MonitoredDataBase> opcValues = new List<MonitoredDataBase>();

            XmlNodeList opcValueNodes = parentNode.ChildNodes;

            foreach (XmlNode opcValueNode in opcValueNodes)
            {
                System.Type datatype = ConvertDatatypeFromString(GetValue(opcValueNode, "Datatype"));

                var opcValue = CreateMonitoredData(
                    datatype,
                    GetValue(opcValueNode, "Name"),
                    GetValue(opcValueNode, "OpcNodeId")
                );
                /*
                var opcValue = new GenericData(
                    GetValue(opcValueNode, "Name"),
                    GetValue(opcValueNode, "OpcNodeId"));
                    */
                opcValues.Add(opcValue);
            }

            return opcValues;
        }

        public static List<MonitoredDataBase> ParseMonitoredDataWithTypeAndId(XmlNode parentNode)
        {
            List<MonitoredDataBase> listSubscriptions = new List<MonitoredDataBase>();

            foreach (XmlNode subscriptionNode in parentNode.ChildNodes)
            {
                System.Type datatype = ConvertDatatypeFromString(GetValue(subscriptionNode, "Datatype"));

                var data = CreateMonitoredDataWithTypeAndId(datatype, GetValue(subscriptionNode, "Name"),
                    GetValue(subscriptionNode, "OpcNodeId"),
                    GetValue(subscriptionNode, "Type"),
                    int.Parse(GetValue(subscriptionNode, "ID")));


                listSubscriptions.Add(data);
            }

            return listSubscriptions;
        }

        public static GenericDataBase CreateGenericData(System.Type datatype, string identifier, string nodeId)
        {
            Type genericClass = typeof(GenericData<>);
            Type constructedClass = genericClass.MakeGenericType(datatype);

            var parameters = new object[2];
            parameters[0] = identifier;
            parameters[1] = nodeId;
            object created = Activator.CreateInstance(constructedClass, parameters);
            return (GenericDataBase)created;
        }

        public static MonitoredDataBase CreateMonitoredData(System.Type datatype, string identifier, string nodeId)
        {
            Type genericClass = typeof(MonitoredData<>);
            //Debug.Log("datatype:" + datatype);
            Type constructedClass = genericClass.MakeGenericType(datatype);

            var parameters = new object[2];
            parameters[0] = identifier;
            parameters[1] = nodeId;
            object created = Activator.CreateInstance(constructedClass, parameters);
            return (MonitoredDataBase)created;
        }

        public static MonitoredDataBase CreateMonitoredDataWithTypeAndId(System.Type datatype, string identifier, string nodeId, string type, int id)
        {
            Type genericClass = typeof(MonitoredDataWithTypeAndId<>);
            Type constructedClass = genericClass.MakeGenericType(datatype);

            var parameters = new object[4];
            parameters[0] = identifier;
            parameters[1] = nodeId;
            parameters[2] = type;
            parameters[3] = id;
            object created = Activator.CreateInstance(constructedClass, parameters);
            return (MonitoredDataBase)created;
        }

        public static GenericDataBase CreateGenericDataWithTypeAndId(System.Type datatype, string identifier, string nodeId, string type, int id)
        {
            Type genericClass = typeof(GenericDataWithTypeAndId<>);
            Type constructedClass = genericClass.MakeGenericType(datatype);

            var parameters = new object[4];
            parameters[0] = identifier;
            parameters[1] = nodeId;
            parameters[2] = type;
            parameters[3] = id;
            object created = Activator.CreateInstance(constructedClass, parameters);
            return (GenericDataBase)created;
        }

        public static List<GenericDataBase> ParseGenericDataWithType(XmlNode parentNode)
        {
            var list = new List<GenericDataBase>();
            foreach (XmlNode subscriptionNode in parentNode.ChildNodes)
            {
                System.Type datatype = ConvertDatatypeFromString(GetValue(subscriptionNode, "Datatype"));

                var data = CreateGenericDataWithTypeAndId(datatype, GetValue(subscriptionNode, "Name"),
                    GetValue(subscriptionNode, "OpcNodeId"),
                    GetValue(subscriptionNode, "Type"),
                    int.Parse(GetValue(subscriptionNode, "ID")));
                list.Add(data);
            }

            return list;
        }

        public static GenericDataBase CreateGenericDataWithTypeAndTwoIds(System.Type datatype, string identifier, string nodeIdTrue, string nodeIdFalse, string type, int id)
        {
            Type genericClass = typeof(GenericDataWithTypeAndTwoIds<>);
            Type constructedClass = genericClass.MakeGenericType(datatype);

            var parameters = new object[5];
            parameters[0] = identifier;
            parameters[1] = nodeIdTrue;
            parameters[2] = nodeIdFalse;
            parameters[3] = type;
            parameters[4] = id;
            //parameters[5] = twoIdState;
            object created = Activator.CreateInstance(constructedClass, parameters);
            return (GenericDataBase)created;
        }


        public static List<GenericDataBase> ParseGenericDataWithTypeAndId(XmlNode parentNode)
        {
            var list = new List<GenericDataBase>();

            foreach (XmlNode node in parentNode.ChildNodes)
            {
                bool isTwoIdStateValue = false;
                bool twoIdState = false;
                try
                {
                    GetValue(node, "OpcNodeIdTrue");
                    isTwoIdStateValue = true;
                }
                catch { }

                System.Type datatype = ConvertDatatypeFromString(GetValue(node, "Datatype"));

                GenericDataBase data;

                if (isTwoIdStateValue)
                {
                    var name = GetValue(node, "Name");
                    var opcNodeIdTrue = GetValue(node, "OpcNodeIdTrue");
                    var opcNodeIdFalse = GetValue(node, "OpcNodeIdFalse");
                    var type = GetValue(node, "Type");
                    var id = int.Parse(GetValue(node, "ID"));

                    data = CreateGenericDataWithTypeAndTwoIds(datatype,
                        name,
                        opcNodeIdTrue,
                        opcNodeIdFalse,
                        type,
                        id);
                }
                else
                {
                    data = CreateGenericDataWithTypeAndId(datatype, GetValue(node, "Name"),
                        GetValue(node, "OpcNodeId"),
                        GetValue(node, "Type"),
                        int.Parse(GetValue(node, "ID")));
                }


                list.Add(data);
            }

            return list;
        }

        public static System.Type ConvertDatatypeFromString(string datatypeString)
        {
            switch (datatypeString)
            {
                case "bool": return typeof(bool);
                case "int": return typeof(int);
                case "float": return typeof(float);
                case "string": return typeof(string);
                case "int16": return typeof(System.Int16);
                case "int32": return typeof(System.Int32);
                case "int64": return typeof(System.Int64);
                case "stringArray": return typeof(string[]);
                case "sByte": return typeof(SByte);
                case "double": return typeof(System.Double);

                default: return null;
            }
        }
    }
}