﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

namespace Shared
{
    public abstract class AbstractXmlParser
    {

        public bool Parse(TextAsset xmlFile)
        {
            XmlDocument xmlDoc;
            xmlDoc = XMLPersistance.LoadPersistanceXml(xmlFile);

            
            
                DoParse(xmlDoc);
                Debug.Log("Success Parsing XML file: " + xmlFile.name);
                return true;
            
/*
            catch (Exception e)
            {
                Debug.LogError("Error parsing xml file: " + xmlFile.name);
                Debug.LogError(e.Message);
                //throw e;
                return false;
            }
            */
        }

        public bool Parse(XmlDocument xmlFile)
        {
            DoParse(xmlFile);
            Debug.Log("Success Parsing XML file: " + xmlFile.Name);
            return true;

            /*
                        catch (Exception e)
                        {
                            Debug.LogError("Error parsing xml file: " + xmlFile.name);
                            Debug.LogError(e.Message);
                            //throw e;
                            return false;
                        }
                        */
        }

        protected abstract void DoParse(XmlDocument xmlDoc);

        public static string GetValue(XmlNode parent, string identifier)
        {
            XmlNode value = parent[identifier];
            if (value == null) throw new Exception("Could not get value from xml node: " + identifier);
            string Return= value.InnerText;
            return value.InnerText;
        }
    }
}
