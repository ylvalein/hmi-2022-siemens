﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using UnityEngine;

namespace Shared
{
    public class XMLPersistance
    {
        private const string XML_FILE_EXTENSION = ".xml";

        public const string settingsFileName = "XML_OPCSettings";  //added line

        static string filepathAppendix = Application.persistentDataPath + "/";


        public static void LoadPersistanceXmlToObject<T>(string name, out T loadedObject, bool loadFromPersistanceInEditor = true) where T : class
        {
            loadedObject = null;

            if (!loadFromPersistanceInEditor && Application.isEditor) return;

            string filepath = filepathAppendix + name + XML_FILE_EXTENSION;
            if (!File.Exists(filepath))
            {
                Debug.Log("Target xml file does not exist: " + filepath);
                return;
            }

            try
            {
                loadedObject = XMLObjectSerializer.Deserialize<T>(File.ReadAllText(filepath));
            }
            catch (System.Exception e)
            {
                Debug.LogError("Could not load xml file to object, file: " + filepath + ", exception:");
                Debug.LogException(e);
            }

        }

        public static XmlDocument LoadPersistanceXml(TextAsset xmlFile)
        {
            string filepath = filepathAppendix + xmlFile.name + XML_FILE_EXTENSION;

            XmlDocument xmlDoc;

            if (!File.Exists(filepath) || Application.isEditor)
            {
                if (!Application.isEditor)
                    Debug.LogError("HOLO: NO FILE FOUND");

                SaveXml(xmlFile.name, xmlFile.ToString());

                xmlDoc = LoadXml(xmlFile);
            }
            else
            {
                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(File.ReadAllText(filepath));
            }
            return xmlDoc;
        }

        public static XmlDocument LoadXml(TextAsset xmlFile)
        {
            MemoryStream assetStream = new MemoryStream(xmlFile.bytes);
            XmlReader reader = XmlReader.Create(assetStream);
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.Load(reader);
            }
            catch (System.Exception ex)
            {
                Debug.Log("Error loading " + xmlFile.name + ":\n" + ex);
            }
            finally
            {
                Debug.Log(xmlFile.name + " loaded");
            }

            return xmlDoc;
        }

        public static bool SaveXml(XmlDocument xmlFile)
        {
            StringWriter stringWriter = new StringWriter();
            XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter);
            xmlTextWriter.Formatting = Formatting.Indented;
            xmlFile.WriteTo(xmlTextWriter);
            xmlTextWriter.Flush();

            return SaveXml(xmlFile.Name, stringWriter.ToString());
        }

        public static bool SaveXml(string filename, string content)
        {
            try
            {
                File.WriteAllText(filepathAppendix + filename + XML_FILE_EXTENSION, content);
            }
            catch (System.Exception e)
            {
                Debug.LogError("Couldn't save xml, exception: ");
                Debug.LogException(e);
                return false;
            }

            //Debug.Log("Path: " + filepathAppendix + filename);
            Debug.Log("Saved Settings successfully");
            return true;
        }

        /// <summary>
        /// Delete the given Xml-File if it exists
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static bool DeleteXml(string filename)
        {
            if (File.Exists(filepathAppendix + filename + XML_FILE_EXTENSION))
            {
                File.Delete(filepathAppendix + filename + XML_FILE_EXTENSION);
                Debug.Log("File: " + filename + XML_FILE_EXTENSION + " has been deleted.");
                return true;
            }
            else
            {
                Debug.LogWarning("File: " + filename + XML_FILE_EXTENSION + " can not be deleted because it does not exist.");
                return false;
            }
        }

        /// <summary>
        /// Check if the given xml-file exists
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static bool Exists(string filename)
        {
            if (File.Exists(filepathAppendix + filename + XML_FILE_EXTENSION))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
