﻿using OpcUa_Siemens_Client;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

namespace Shared
{
    public class XmlParser_SensorActuatorData : AbstractXmlParser
    {
        public OpcSettings settings;

        public List<MonitoredDataBase> listSubscriptions = new List<MonitoredDataBase>();
        public List<GenericDataBase> listWrite = new List<GenericDataBase>();

        protected override void DoParse(XmlDocument xmlDoc)
        {
            settings = XmlParserHelper.ParseConnectionSettings(xmlDoc);
            ParseSubscriptions(xmlDoc);
            ParseWriteList(xmlDoc);

            OpcManager.AssignSession(settings, listWrite);
        }

        private void ParseSubscriptions(XmlDocument xmlDoc)
        {
            var subscriptionNodes = xmlDoc.GetElementsByTagName("OPCSubscriptions");
            listSubscriptions = XmlParserHelper.ParseMonitoredDataWithTypeAndId(subscriptionNodes[0]);
        }

        private void ParseWriteList(XmlDocument xmlDoc)
        {
            var subscriptionNodes = xmlDoc.GetElementsByTagName("OPCWrite");
            listWrite = XmlParserHelper.ParseGenericDataWithTypeAndId(subscriptionNodes[0]);
        }
    }
}