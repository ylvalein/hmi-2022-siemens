﻿using UnityEngine;
using UnityEngine.UI;

namespace Shared
{
    public class SettingsReflectionsPropertyAccessTextfieldInt : AbstractSettingsRefelectionsPropertyAccess
    {
        //TODO: Change to normal InputField
        //public KeyboardInputField targetKeyboardInputField;
        public InputField targetKeyboardInputField;

        protected override void InitialiseProperty()
        {
            var field = GetFieldInfo();

            targetKeyboardInputField.text = field.GetValue(settingsManager.GetSettings()).ToString();

            targetKeyboardInputField.onValueChanged.AddListener((string value) =>
            {
                var settingsObject = settingsManager.GetSettings();
                var intVal = 0;
                try
                {
                    intVal = int.Parse(value);
                }
                catch
                {
                    Debug.LogWarning("Parsing int failed, field: " + fieldName);
                }
                finally
                {
                    field.SetValue(settingsObject, intVal);
                    OnSettingsChanged(intVal);
                }
            });
        }
    }
}
