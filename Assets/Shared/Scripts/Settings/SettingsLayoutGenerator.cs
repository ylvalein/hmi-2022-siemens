﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

namespace Shared
{
    [ExecuteInEditMode]
    public class SettingsLayoutGenerator : MonoBehaviour
    {
        
        public bool generate = false;

        public AbstractSettingsManagerBase settingsManager;

        public GameObject prefabToggle;
        public GameObject prefabTextfieldString;
        public GameObject prefabTextfieldInt;
        public GameObject prefabTextfieldFloat;

        public Transform transformParentToggle;
        public Transform transformParentTextfield;


        // Update is called once per frame
        void Update()
        {
            if (generate)
            {
                generate = false;
                Generate(settingsManager.GetSettingsType());
            }
        }

        void Generate(Type settingsType)
        {
            ClearLayout();

            var members = settingsType.GetMembers();
            foreach (MemberInfo member in members)
            {
                FieldInfo field;
                try
                {
                    field = (FieldInfo)member;
                }
                catch
                {
                    continue;
                }

                GameObject instance = null;

                var type = field.FieldType;
                if (type == typeof(System.Boolean))
                {
                    instance = InstantiateToggle(field);
                }
                else if (type == typeof(System.String))
                {
                    instance = InstantiateTextfieldString(field);
                }
                else if (type == typeof(float))
                {
                    instance = InstantiateTextfieldFloat(field);
                }
                else if (type == typeof(int))
                {
                    instance = InstantiateTextfieldInt(field);
                }
                InitialisePropertyAccess(instance, field);
            }
        }

        public void ClearLayout()
        {
            Action<Transform> clear = (Transform parent) =>
            {
                while (parent.childCount > 0)
                {
                    DestroyImmediate(parent.GetChild(0).gameObject);
                }
            };

            clear(transformParentToggle);
            clear(transformParentTextfield);
        }

        public void InstantiateToggles(List<FieldInfo> toggleFields)
        {
            foreach (var field in toggleFields)
            {
                var instance = InstantiateToggle(field);
                InitialisePropertyAccess(instance, field);
            }
        }

        public GameObject InstantiateToggle(FieldInfo field)
        {
            var instance = Instantiate(prefabToggle, transformParentToggle);
            instance.name = "Toggle_" + field.Name;
            instance.GetComponentInChildren<Text>().text = StringHelper.SplitCamelCase(field.Name);

            return instance;
        }

        public GameObject InstantiateTextfieldString(FieldInfo field)
        {
            var instance = Instantiate(prefabTextfieldString, transformParentTextfield);
            instance.name = "Textfield_" + field.Name;
            //TODO:Change to normal  inputfield
            //((Text)instance.GetComponent<KeyboardInputField>().placeholder).text = "Enter Text";
            instance.transform.GetChild(1).GetComponent<Text>().text = StringHelper.SplitCamelCase(field.Name);

            return instance;
        }

        public GameObject InstantiateTextfieldFloat(FieldInfo field)
        {
            var instance = Instantiate(prefabTextfieldFloat, transformParentTextfield);
            instance.name = "Textfield_Float_" + field.Name;
            //TODO:Change to normal  inputfield
            //var keyboardInputField = instance.GetComponent<KeyboardInputField>();

            //((Text)keyboardInputField.placeholder).text = "Enter Float";
            instance.transform.GetChild(1).GetComponent<Text>().text = StringHelper.SplitCamelCase(field.Name);

            return instance;
        }

        public GameObject InstantiateTextfieldInt(FieldInfo field)
        {
            var instance = Instantiate(prefabTextfieldInt, transformParentTextfield);
            instance.name = "Textfield_Int_" + field.Name;
            //TODO:Change to normal  inputfield
            //var keyboardInputField = instance.GetComponent<KeyboardInputField>();

            //((Text)keyboardInputField.placeholder).text = "Enter Integer";
            instance.transform.GetChild(1).GetComponent<Text>().text = StringHelper.SplitCamelCase(field.Name);

            return instance;
        }

        public void InitialisePropertyAccess(GameObject instance, FieldInfo field)
        {
            if (instance == null) return;
            var propertyHelper = instance.GetComponent<AbstractSettingsRefelectionsPropertyAccess>();
            propertyHelper.fieldName = field.Name;
            propertyHelper.settingsManager = settingsManager;
        }

        
    }



    public static class StringHelper
    {
        public static string SplitCamelCase(this string str)
        {
            return Regex.Replace(
                Regex.Replace(
                    str,
                    @"(\P{Ll})(\P{Ll}\p{Ll})",
                    "$1 $2"
                ),
                @"(\p{Ll})(\P{Ll})",
                "$1 $2"
            );
        }
    }
}
