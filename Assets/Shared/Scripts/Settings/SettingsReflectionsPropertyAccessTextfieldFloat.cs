﻿using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

namespace Shared
{
    public class SettingsReflectionsPropertyAccessTextfieldFloat : AbstractSettingsRefelectionsPropertyAccess
    {
        //TODO: Change to normal InputField
        //public KeyboardInputField targetKeyboardInputField
        public InputField targetKeyboardInputField;

        protected override void InitialiseProperty()
        {
            var field = GetFieldInfo();

            var valFloat = (float)field.GetValue(settingsManager.GetSettings());
            var valString = valFloat.ToString(new CultureInfo("en-GB"));

            targetKeyboardInputField.text = (valString);

            targetKeyboardInputField.onValueChanged.AddListener((string value) =>
            {
                var settingsObject = settingsManager.GetSettings();
                float floatVal = 0;
                try
                {
                    floatVal = float.Parse(value, new CultureInfo("en-GB"));

                }
                catch
                {
                    Debug.LogWarning("Parsing float failed, field: " + fieldName);
                }
                finally
                {
                    field.SetValue(settingsObject, floatVal);
                    OnSettingsChanged(floatVal);
                }
            });
        }
    }
}
