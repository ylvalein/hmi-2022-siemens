﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    public abstract class AbstractSettingsManagerBase : MonoBehaviour
    {

        public void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                _LoadSettings();
            }
            else
            {
                Destroy(this);
            }
        }
        public void OnDestroy()
        {
            _SaveSettings();
        }


        public static ProjectSettingsBase settings;

        public abstract Type GetSettingsType();

        public abstract object GetSettings();

        public static AbstractSettingsManagerBase Instance;

        protected abstract void _LoadSettings();
        protected abstract void _SaveSettings();

        public static void LoadSettings()
        {
            if (settings != null) return;

            if (Instance == null)
            {
                Debug.LogError("No SettingsManager Instance found. Please make sure to have an active SettingsManager in your Scene and set the execution order of your settings manager implementation to the highest possible.");
                return;
            }

            Instance._LoadSettings();
        }
    }
}
