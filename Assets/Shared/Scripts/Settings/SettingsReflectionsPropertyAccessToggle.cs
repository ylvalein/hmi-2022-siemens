﻿using UnityEngine.UI;

namespace Shared
{
    public class SettingsReflectionsPropertyAccessToggle : AbstractSettingsRefelectionsPropertyAccess
    {
        public Toggle targetToggle;

        protected override void InitialiseProperty()
        {
            var field = GetFieldInfo();

            targetToggle.isOn = (bool)field.GetValue(settingsManager.GetSettings());

            targetToggle.onValueChanged.AddListener((bool value) =>
            {
                var settingsObject = settingsManager.GetSettings();
                field.SetValue(settingsObject, value);
                OnSettingsChanged(value);
            });
        }
    }
}

