﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;


namespace Shared
{
    public abstract class AbstractSettingsManager<T> : AbstractSettingsManagerBase where T : ProjectSettingsBase
    {
        public T Settings
        {
            get
            {
                return (T)settings;
            }
        }

        protected override void _LoadSettings()
        {
            T _settings;
            XMLPersistance.LoadPersistanceXmlToObject("Settings", out _settings);
            settings = _settings;

            if (settings == null)
            {
                Debug.Log("Settings could not be loaded, creating new settings object");
                settings = (T)Activator.CreateInstance(typeof(T));
            }
            else
            {
                Debug.Log("Loaded settings successfully");

                var members = typeof(T).GetMembers();
                foreach (var member in members)
                {

                    FieldInfo field;
                    try
                    {
                        field = (FieldInfo)member;
                    }
                    catch
                    {
                        continue;
                    }

                    Debug.LogFormat("{0}: {1}", field.Name, field.GetValue(settings));
                }
            }
        }

        protected override void _SaveSettings()
        {
            XMLPersistance.SaveXml("Settings", XMLObjectSerializer.SerializeObjectToXMLString((T)settings));
        }

        public override object GetSettings()
        {
            return settings;
        }

        public override Type GetSettingsType()
        {
            return typeof(T);
        }
    }
}