﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace Shared
{
    public abstract class AbstractSettingsRefelectionsPropertyAccess : MonoBehaviour
    {
        [HideInInspector]
        public string fieldName;
        [HideInInspector]
        public AbstractSettingsManagerBase settingsManager;


        public void Start()
        {
            InitialiseProperty();
        }

        protected FieldInfo GetFieldInfo()
        {
            var members = settingsManager.GetSettingsType().GetMembers();
            foreach (var member in members)
            {
                if (member.Name == fieldName)
                {
                    return (FieldInfo)member;
                }
            }
            return null;
        }

        protected void OnSettingsChanged(object value)
        {
            Debug.Log("Successfully set value of property: " + fieldName + " to: " + value);
        }

        protected abstract void InitialiseProperty();
    }
}
