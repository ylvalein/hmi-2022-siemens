﻿using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Diagnostics;
using UnityEngine;

namespace Shared
{
    public abstract class AbstractSceneInitializer : MonoBehaviour
    {
        //deprecated
        //public GameObject fpsDisplay;

        public SceneHelperFunctions sceneHelperFunctions;
        public OpcManager opcUaManager;
        public LookupReferenceManager referenceManager;

        [Lookup]
        public SetMachineVisibility setMachineVisibility;
        public abstract void InitSceneObjects();

        private void Awake()
        {
            SceneHelperFunctions.currentScene = gameObject.scene;

            if (AbstractSettingsManagerBase.settings == null) AbstractSettingsManagerBase.LoadSettings();

            if (referenceManager) referenceManager.LookupReferences();

            var settings = (ProjectSettingsBase)AbstractSettingsManagerBase.settings;

            if (opcUaManager)
            {
                opcUaManager.UseOpcUa = settings.UseOpcUa;
                opcUaManager.UseOpcReferenceServer = settings.UseOpcReferenceServer;
            }

            setMachineVisibility?.SetVisible(settings.MachineVisible);

            //deprecated
            //if (fpsDisplay && !settings.ShowFps) Destroy(fpsDisplay);

            // Show diagnostics panel in scene
            //CoreServices.DiagnosticsSystem.ShowDiagnostics = settings.ShowFps;


            var objToInit = SceneHelperFunctions.FindAllInterfaces<IInitRequired>();
            foreach (var obj in objToInit)
            {
                try
                {
                    obj.Init();
                }
                catch (System.Exception e)
                {
                    Debug.LogException(e);
                    Debug.LogError("Error initializing object: " + obj.ToString());
                }
            }

            if (settings.RandomMode)
            {
                var objRandom = SceneHelperFunctions.FindAllInterfaces<IRandomModeListener>();
                foreach (var obj in objRandom)
                {
                    obj.EnableRandomMode();
                }
            }

            InitSceneObjects();
        }
    }
}
