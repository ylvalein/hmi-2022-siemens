﻿using UnityEngine.UI;

namespace Shared
{
    public class SettingsReflectionsPropertyAccessTextfield : AbstractSettingsRefelectionsPropertyAccess
    {
        //TODO: Change to normal Inputfield
        //public KeyboardInputField targetKeyboardInputField;
        public InputField targetKeyboardInputField;

        protected override void InitialiseProperty()
        {
            var field = GetFieldInfo();

            targetKeyboardInputField.text = (string)field.GetValue(settingsManager.GetSettings());

            targetKeyboardInputField.onValueChanged.AddListener((string value) =>
            {
                var settingsObject = settingsManager.GetSettings();
                field.SetValue(settingsObject, value);

                OnSettingsChanged(value);
            });
        }
    }
}
