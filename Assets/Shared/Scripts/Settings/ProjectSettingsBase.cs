﻿namespace Shared
{
    //Do not modify this class!
    //If you want to extend it, create a subclass with the additional properties you need to save / load.
    public class ProjectSettingsBase
    {
        public bool UseOpcUa = true;
        public bool UseOpcReferenceServer = true;
        public bool AvatarSharingEnabled = false;
        public string SharingAdress = "localhost";
        public bool MachineVisible = true;
        public bool ShowFps = true;
        public bool RandomMode = true;
    }
}
