﻿using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using UnityEngine;


[RequireComponent(typeof(AudioSource))]
public class ClickSoundPlayer : MonoBehaviour, IMixedRealityPointerHandler
{
    [SerializeField]
    [Tooltip("Contains the sound that should be played at every air tap event")]
    private AudioSource audioSource;


    void Start()
    {
        if (audioSource == null)
        {
            audioSource = GetComponent<AudioSource>();
        }
    }

    private void OnEnable()
    {
        CoreServices.InputSystem?.PushFallbackInputHandler(gameObject);
    }

    private void OnDisable()
    {
        CoreServices.InputSystem?.PopFallbackInputHandler();
    }

    public void OnPointerDown(MixedRealityPointerEventData eventData)
    {

    }

    public void OnPointerDragged(MixedRealityPointerEventData eventData)
    {

    }

    public void OnPointerUp(MixedRealityPointerEventData eventData)
    {

    }

    /// <summary>
    /// Play audio on click
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerClicked(MixedRealityPointerEventData eventData)
    {
        audioSource.Play();
    }
}
