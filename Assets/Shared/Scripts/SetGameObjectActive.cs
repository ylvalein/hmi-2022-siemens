﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    public class SetGameObjectActive : MonoBehaviour
    {
        public bool inverseLogic = false;
        public void SetVisible(bool visibility)
        {
            gameObject.SetActive(inverseLogic ? !visibility : visibility);
        }
    }
}
