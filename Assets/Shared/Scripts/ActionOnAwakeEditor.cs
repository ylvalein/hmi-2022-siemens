﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Shared
{
    public class ActionOnAwakeEditor : MonoBehaviour
    {

        public UnityEvent action;

#if UNITY_EDITOR
        void Awake()
        {
            action.Invoke();
        }
#endif
    }
}
