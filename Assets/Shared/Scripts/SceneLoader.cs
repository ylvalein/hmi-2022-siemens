﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Shared
{
    public class SceneLoader : MonoBehaviour
    {
        [Tooltip("Dropdown menu to choose which scene should be loaded")]
        [SerializeField]
        private Dropdown dropDown;

        /// <summary>
        /// Load next Scene by build index
        /// </summary>
        public void LoadNextScene()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }

        /// <summary>
        /// Load next Scene asynchronously by build index
        /// </summary>
        public void LoadNextSceneAsync()
        {
            StartCoroutine(LoadAsynchronously(SceneManager.GetActiveScene().buildIndex + 1));
        }

        /// <summary>
        /// Load scene by dropDown index asynchronously
        /// </summary>
        public void LoadSceneByDropdownIndexAsync()
        {
            //Gice backgroundloading less time per frame
            Application.backgroundLoadingPriority = ThreadPriority.Low;

            // +1 beacause the settingsscene should be idx 0
            int sceneBuildIndex = dropDown.value + 1;

            StartCoroutine(LoadAsynchronously(sceneBuildIndex));
        }
        /// <summary>
        /// Load scene by dropDown index 
        /// </summary>
        public void LoadSceneByDropdownIndex()
        {
            // +1 beacause the settingsscene should be idx 0
            int sceneBuildIndex = dropDown.value + 1;

            SceneManager.LoadScene(sceneBuildIndex);
        }


        /// <summary>
        /// Coroutine to load asynchronously
        /// </summary>
        /// <param name="sceneIndex"></param>
        /// <returns></returns>
        private IEnumerator LoadAsynchronously(int sceneIndex)
        {
            AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

            while (!operation.isDone)
            {
                yield return null;
            }

        }
    }
}
