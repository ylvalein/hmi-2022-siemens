﻿using UnityEngine;

namespace Shared {
    [LookupReference]
    public class SetMachineVisibility : MonoBehaviour {
        public void SetVisible(bool visible) {
            var objectsToChange = GetComponentsInChildren<SetGameObjectActive>(true);

            foreach (var obj in objectsToChange) {
                obj.SetVisible(visible);
            }

        }
    }
}
