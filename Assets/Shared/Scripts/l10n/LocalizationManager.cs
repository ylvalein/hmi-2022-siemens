﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;



public class LocalizationManager : MonoBehaviour {

    public static LocalizationManager instance;

    [System.Serializable]
    public class LanguageInformation {
        public string languageId;
        public TextAsset languageTextFile;
    }

    public List<LanguageInformation> languageInformation = new List<LanguageInformation>();

    public string languageIdToUse;

    private Dictionary<string, string> localizedText;
    private bool isReady = false;
    private string missingTextString = "Localized text not found";


    void Awake() {
        if (instance == null) {
            instance = this;
            LoadLocalizedText();
        } else if (instance != this) {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
    }

    public void LoadLocalizedText() {
        if (languageInformation.Count == 0) return;

        localizedText = new Dictionary<string, string>();

        var languageInfo = languageInformation.Find(x => x.languageId == languageIdToUse);
        
        if (languageInfo == null) languageInfo = languageInformation[0];

        var fileToUse = languageInfo.languageTextFile;

        LocalizationData loadedData = JsonUtility.FromJson<LocalizationData>(fileToUse.text);
        
        for (int i = 0; i < loadedData.items.Length; i++) {
            if (loadedData.items[i].key == "") continue;
            Debug.Log("L10n: Adding value for key: " + loadedData.items[i].key);
            localizedText.Add(loadedData.items[i].key, loadedData.items[i].value);
            //localizedText.Add(loadedData.items[i].key, "{TXT}");
        }

        Debug.Log("Data loaded, dictionary contains: " + localizedText.Count + " entries");
        
        isReady = true;
    }

    public static string GetLocalizedValue(string key) {
        if (instance == null) {
            Debug.LogError("LocalizationManager not initialised");
            return string.Empty;
        } else return instance.GetLocalizedValue_(key);
    }

    private string GetLocalizedValue_(string key) {
        string result = missingTextString;
        if (localizedText.ContainsKey(key)) {
            result = localizedText[key];
        }

        return result;
    }

    public bool GetIsReady() {
        return isReady;
    }

    public void UnloadLocalizedText() {
        if(localizedText!=null) localizedText.Clear();
    }

}