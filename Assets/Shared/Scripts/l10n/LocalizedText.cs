﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum TextAppearance {
    Normal,
    Uppercase
}

public class LocalizedText : MonoBehaviour {

    public string key;

    public TextAppearance appearance;

    // Use this for initialization
    void Start() {
        Text text = GetComponent<Text>();

        var targetText = LocalizationManager.GetLocalizedValue(key);

        if(appearance == TextAppearance.Uppercase) {
            targetText = targetText.ToUpper();
        }

        text.text = targetText;
    }

}