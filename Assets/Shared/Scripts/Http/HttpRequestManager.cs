﻿using UnityEngine;
using System.Collections;
using System;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;


namespace Shared
{

    public class HttpRequestManager : Singleton<HttpRequestManager>
    {

#if WINDOWS_UWP
        private static Windows.Web.Http.HttpClient client;

          private void Start()
        {
            var filter = new Windows.Web.Http.Filters.HttpBaseProtocolFilter();
            filter.IgnorableServerCertificateErrors.Add(Windows.Security.Cryptography.Certificates.ChainValidationResult.Expired);
            filter.IgnorableServerCertificateErrors.Add(Windows.Security.Cryptography.Certificates.ChainValidationResult.Untrusted);
            filter.IgnorableServerCertificateErrors.Add(Windows.Security.Cryptography.Certificates.ChainValidationResult.InvalidName);

            client = new Windows.Web.Http.HttpClient(filter);
        }

         public static void GetDataFromWebserverAsyncTask(string url, Action<string> callback)
        {
            Task task = Task.Run( async () =>
           {
               try
               {                                                    
                   var response = await client.GetAsync(new System.Uri(url));
                   response.EnsureSuccessStatusCode();
                   var message = response.Content.ReadAsStringAsync().GetResults();


                   if (string.IsNullOrEmpty(message))
                   {
                       Debug.LogError("Received string is empty. Url: " + url);
                   }
                   else
                   {
                       //Debug.Log("HTTP request success, received value: " + message);
                       callback(message);
                   }
               }
               catch (Exception e)
               {
                   Debug.Log("Exception in HttpRequest:\n" + e.ToString());
               }
           });
        }

#else
        private static HttpClient client;

        private void Start()
        {
            client = new HttpClient();
        }

        public static void GetDataFromWebserverAsyncTask(string url, Action<string> callback)
        {
            Task task = Task.Run(() =>
           {
               try
               {
                   System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                   // ignore ssl errors; should be replaced
                   ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

                   //HttpClient client = new HttpClient();
                   client.BaseAddress = new Uri(url);
                   client.DefaultRequestHeaders.Accept.Clear();
                   client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                   var response = client.GetAsync(client.BaseAddress).Result;
                   response.EnsureSuccessStatusCode();
                   string message = response.Content.ReadAsStringAsync().Result;


                   if (string.IsNullOrEmpty(message))
                   {
                       Debug.LogError("Received string is empty. Url: " + url);
                   }
                   else
                   {
                       //Debug.Log("HTTP request success, received value: " + message);
                       callback(message);
                   }

                   //Debug.Log("Message: " + message);
               }
               catch (Exception e)
               {
                   Debug.Log("Exception in HttpRequest:\n" + e.ToString());
               }

           });
        }

        protected override void OnDestroy()
        {
            client.Dispose();
            base.OnDestroy();
        }





#endif

        //        public static void GetDataFromWebserverAsyncTask(string url, Action<string> callback)
        //        {
        //            //async keyword for uwp build
        //            Task task = Task.Run(async () =>
        //          {
        //              Debug.Log("Task:\n url: " + url);

        //              //TODO: No https connection possible
        //              try
        //              {
        //                  //WebRequestHandler handler = new WebRequestHandler();
        //                  //X509Certificate2 certificate = new X509Certificate2();
        //                  //handler.ClientCertificates.Add(certificate);

        //                  //HttpClient client = new HttpClient(/*handler*/);


        //#if WINDOWS_UWP
        //                   var filter = new Windows.Web.Http.Filters.HttpBaseProtocolFilter();
        //                   filter.IgnorableServerCertificateErrors.Add(Windows.Security.Cryptography.Certificates.ChainValidationResult.Expired);
        //                   filter.IgnorableServerCertificateErrors.Add(Windows.Security.Cryptography.Certificates.ChainValidationResult.Untrusted);
        //                   filter.IgnorableServerCertificateErrors.Add(Windows.Security.Cryptography.Certificates.ChainValidationResult.InvalidName);
        //                   Windows.Web.Http.HttpClient client = new Windows.Web.Http.HttpClient(filter);
        //                   var response = await client.GetAsync(new System.Uri(url));
        //                   response.EnsureSuccessStatusCode();
        //                   var message = response.Content.ReadAsStringAsync().GetResults();


        //#else
        //                  System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

        //                  // ignore ssl errors; should be replaced
        //                  ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

        //                  HttpClient client = new HttpClient();
        //                  client.BaseAddress = new Uri(url);
        //                  client.DefaultRequestHeaders.Accept.Clear();
        //                  client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //                  var response = client.GetAsync(client.BaseAddress).Result;
        //                  response.EnsureSuccessStatusCode();
        //                  string message = response.Content.ReadAsStringAsync().Result;
        //#endif


        //                  client.Dispose();

        //                  if (string.IsNullOrEmpty(message))
        //                  {
        //                      Debug.LogError("Received string is empty. Url: " + url);
        //                  }
        //                  else
        //                  {
        //                      Debug.Log("HTTP request success, received value: " + message);
        //                      callback(message);
        //                  }

        //                  //Debug.Log("Message: " + message);
        //              }
        //              catch (Exception e)
        //              {
        //                  Debug.Log("Exception in HttpRequest:\n" + e.ToString());
        //              }
        //          });
        //        }


        public static void GetDataFromWebserverAsync(string url, Action<string> callback)
        {
            Debug.LogFormat("HttpRequestManager.GetDataFromWebserverAsync({0})", url);            
            Instance.StartCoroutine(DoHttp(url, callback));
        }

        private static IEnumerator DoHttp(string url, Action<string> callback)
        {
            var www = new WWW(url);

            yield return www;

            if (!string.IsNullOrEmpty(www.error))
            {
                Debug.LogError("HTTP request error: " + www.error);
            }
            else
            {
                Debug.Log("HTTP request success, received value: " + www.text);

                callback(www.text);
            }
        }
    }

}
