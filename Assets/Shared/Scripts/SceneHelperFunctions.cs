﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Shared
{
    [LookupReference]
    public class SceneHelperFunctions : MonoBehaviour
    {
        // TODO: Change the name of the scene to "SettingsScene"
        private const string SETTINGSSCENE_NAME = "SettingsScene_Dropdown";

        GameObject gameObjectMachineModelParent;
        public static Scene currentScene;

        public void RestartScene()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        public void LoadSceneSettings()
        {
            SceneManager.LoadScene(SETTINGSSCENE_NAME);
        }

        public static IEnumerable<T> FindAllInterfaces<T>()
        {
            var resultAllScripts = FindObjectsOfTypeAll(typeof(MonoBehaviour));
            var filteredMonobehaviour = new List<MonoBehaviour>();
            for (int i = 0; i < resultAllScripts.Count(); i++)
            {
                if (((MonoBehaviour)resultAllScripts.ElementAt(i)).gameObject.scene == currentScene)
                {
                    //Added if statemant to reduce the amount of found objects from >1000 to <10 
                    if ((MonoBehaviour)resultAllScripts.ElementAt(i) is T)
                    {
                        filteredMonobehaviour.Add((MonoBehaviour)resultAllScripts.ElementAt(i));
                    }
                }
            }
            var returnVal = filteredMonobehaviour.OfType<T>();
            return returnVal;
        }

        public static IEnumerable<UnityEngine.Object> FindAllObjects()
        {
            var resultAllScripts = Resources.FindObjectsOfTypeAll(typeof(UnityEngine.Object));
            var filteredObjects = new List<UnityEngine.Object>();
            for (int i = 0; i < resultAllScripts.Count(); i++)
            {
                try
                {
                    if (((MonoBehaviour)resultAllScripts.ElementAt(i)).gameObject.scene == currentScene)
                    {
                        filteredObjects.Add((MonoBehaviour)resultAllScripts.ElementAt(i));
                    }
                }
                catch
                {
                    continue;
                }

            }

            return filteredObjects;
        }

        /*
        public static T FindSingleObject<T>() {
            var obj = FindAllObjects<T>();
            var numObj = obj.Count();
            if (numObj > 1) {
                Debug.LogError("FindSingleObject: More than one object found, returning default");
                return default(T);
            } else if (numObj == 0) {
                Debug.LogError("FindSingleObject: No Object found");
                return default(T);
            }
            return obj.First();
        }
        */

        public void WriteDebugTestMessage()
        {
            Debug.LogWarning("Debug Test Message");
        }
    }
}
