﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    public interface IRandomModeListener
    {
        void EnableRandomMode();
    }
}
