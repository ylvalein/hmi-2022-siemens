﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    public interface IInitRequired
    {
        void Init();
    }
}
