﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    public class TimedUpdateCaller : MonoBehaviour
    {

        public Component timedUpdateScript;

        public float updateInterval;

        private float lastTimeStamp;

        private void Start()
        {
            lastTimeStamp = Time.realtimeSinceStartup;
            InvokeRepeating("CallFunction", 0, updateInterval);
        }

        private void CallFunction()
        {
            if (enabled == false) return;

            try
            {
                ((ITimedUpdate)timedUpdateScript).TimedUpdate(Time.realtimeSinceStartup - lastTimeStamp);
            }
            catch (System.InvalidCastException)
            {
                Debug.LogError("You selected the wrong script, GameObject: " + gameObject.name + ", selected TimedUpdateScript does not implement ITimedUpdate interface");
            }
            finally
            {
                lastTimeStamp = Time.realtimeSinceStartup;
            }
        }
    }
}
