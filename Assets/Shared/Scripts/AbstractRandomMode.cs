﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared {
    public abstract class AbstractRandomMode : MonoBehaviour, IRandomModeListener {
        bool randomModeEnabled = false;

        public bool overrideRandomModeIsEnabled = false;
        public int randomUpdateTimeMs = 1000;

        public virtual void Awake() {
            if (!GetRandomModeEnabled()) {
                Destroy(this);
            } 
        }

        private void OnEnable() {
            float randomUpdateFrequencySeconds = randomUpdateTimeMs / 1000f;
            InvokeRepeating("RandomUpdate", randomUpdateFrequencySeconds, randomUpdateFrequencySeconds);
        }

        private void OnDisable() {
            CancelInvoke("RandomUpdate");
        }

        public void EnableRandomUpdate() {
            if (!overrideRandomModeIsEnabled) return;
            randomModeEnabled = true;
        }

        protected bool GetRandomModeEnabled() {
            return randomModeEnabled || overrideRandomModeIsEnabled;
        }

        public abstract void RandomUpdate();

        public void EnableRandomMode() {
            randomModeEnabled = true;
        }
    }
}
