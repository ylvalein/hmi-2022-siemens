﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    public class SetPositionOnStart : MonoBehaviour
    {
        public Transform target;

        void Start()
        {
            if (!target)
                target = this.transform;

            transform.position = target.position;
        }
    }
}
