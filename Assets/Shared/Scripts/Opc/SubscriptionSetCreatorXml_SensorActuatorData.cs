﻿using OpcUa_Siemens_Client;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    public class SubscriptionSetCreatorXml_SensorActuatorData : SubscriptionSetCreatorXmlBase<XmlParser_SensorActuatorData>
    {
        protected override string GetServerEndpoint()
        {
            return GetParser().settings.serverEndpoint;
        }
        
        //TODO: check call back functionality
        protected override SubscriptionSet InitSubset()
        {
            var parser = GetParser();
            return opcManager.CreateSubscriptionSet(parser.settings, parser.listSubscriptions, null, true);
        }
    }
}