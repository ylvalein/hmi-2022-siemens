﻿using OpcUa_Siemens_Client;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    public class SubscriptionSetCreatorXml_MachineModule : SubscriptionSetCreatorXmlBase<XmlParser_OpcMonitoredDataWithTypeAndId>
    {
        public Action<MonitoredDataBase> OnMessageArrivedAction;

        public int id;

        protected override string GetServerEndpoint()
        {
            return GetParser().settings.serverEndpoint;
        }

        protected override SubscriptionSet InitSubset()
        {
            var parser = GetParser();

            foreach (var value in parser.opcValues)
            {
                value.m_name = value.m_name.Replace("{ID}", id.ToString());
                value.m_nodeId = value.m_nodeId.Replace("{ID}", id.ToString());
            }

            foreach (var value in parser.opcValues)
            {
                Debug.Log("Value, name: " + value.m_name + ", NodeId: " + value.m_nodeId);
            }

            return opcManager.CreateSubscriptionSet(parser.settings, parser.opcValues, (MonitoredDataBase data) => { OnMessageArrivedAction?.Invoke((MonitoredDataBase)data); }, true);
        }
    }
}
