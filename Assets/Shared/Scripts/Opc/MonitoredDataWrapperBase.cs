﻿using OpcUa_Siemens_Client;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared {
    public abstract class MonitoredDataWrapperBase<T> {
        Action<T> listeners;
        bool callbackInitialised = false;

        protected MonitoredData<T> monitoredDataCached;
        public MonitoredData<T> MonitoredData {
            get {
                return monitoredDataCached;
            }

            set {
                monitoredDataCached = value;
            }
        }


        public void ExecuteOnUiThread(Action action) {
            UnityMainThreadDispatcher.Instance().Enqueue(action);
        }

        public T GetCurrentValue() {
            var target = MonitoredData;
            /*
            if (target == null)
            {
                Debug.LogError("MonitoredDataWrapper: Monitored Data is null, reading failed");
                return default(T);
            }
            return (T)Convert.ChangeType(target.m_value, typeof(T));
            */
            return MonitoredData.Value;
        }

        public void AddOnValueChangedListener(Action<T> listener) {
            listeners += listener;
            if (MonitoredData != null && !callbackInitialised) {
                callbackInitialised = true;
                MonitoredData.OnValueChangedCallback += OnValueChanged;

                try
                {
                    //Automatically invoke the callback with the current value
                    listener.Invoke(GetCurrentValue());
                } catch (InvalidCastException e)
                {
                    Debug.LogError("Couldn't cast the MonitoredData value to target datatype: " + typeof(T) + ", item: " + MonitoredData.m_name);
                    Debug.LogException(e);
                }
                
            }
        }

        public void RemoveOnValueChangedListener(Action<T> listener) {
            listeners -= listener;
        }

        public void Write(T newValue) {
            if (MonitoredData == null) {
                Debug.LogError("MonitoredDataWrapper: Monitored Data is null, writing failed");
                return;
            }
            MonitoredData.Value = newValue;
            MonitoredData.m_session.write(MonitoredData);
        }

        //Will be executed from seperate thread of OpcUa library
        private void OnValueChanged(T value) {
            ExecuteOnUiThread(() => {
                listeners?.Invoke(value);
            });
        }
    }
}
