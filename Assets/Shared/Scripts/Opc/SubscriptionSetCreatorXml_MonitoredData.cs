﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using OpcUa_Siemens_Client;
using System;

namespace Shared
{
    public class SubscriptionSetCreatorXml_MonitoredData : SubscriptionSetCreatorXmlBase<XmlParser_OpcMonitoredData>
    {

        public Action<string, object> OnMessageArrivedAction;
        public string address;

        protected override string GetServerEndpoint()
        {
            if (address != null) return address;
            return GetParser().settings.serverEndpoint;
        }

        //TODO: check call back functionality (this workds)
        protected override SubscriptionSet InitSubset()
        {
            var parser = GetParser();
            if (this.address != null)
            {
                parser.settings.serverEndpoint = address;
            }
            return opcManager.CreateSubscriptionSet(parser.settings, parser.opcValues, (MonitoredDataBase data) =>
            {
                OnMessageArrivedAction?.Invoke(data.m_name, data.m_value);
            }, true);
        }
    }
}
