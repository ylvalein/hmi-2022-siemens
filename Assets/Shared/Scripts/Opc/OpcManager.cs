﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;
using System.IO;
//using System;

using OpcUa_Siemens_Client;
using System;

namespace Shared
{
    public class OpcSettings
    {
        public string serverEndpoint;
        public int subscriptionTime;

        public OpcSettings(string serverEndpoint, int subscriptionTime)
        {
            this.serverEndpoint = serverEndpoint;
            this.subscriptionTime = subscriptionTime;
        }
    }

    [LookupReference]
    public class OpcManager : Singleton<OpcManager>, IInitRequired
    {
        public StatusText statusText;
        public string statusStr;
        public Color statusColor;
        public bool connected;

        private List<OPCServerInfo> myListOpcServers = new List<OPCServerInfo>();
        public HoloOpcUaConfiguration myconfig;
        public List<HoloOpcSession> myListOpcSessions = new List<HoloOpcSession>();

        public List<OPCServerInfo> OpcServerList { get => myListOpcServers; }


        public bool UseOpcReferenceServer { get; set; }
        public bool UseOpcUa { get; set; }

        public bool writingEnabled = false;

        public class OPCServerInfo
        {
            public string endpoint;
            public string username;
            public string password;
        }

        public bool IsInitialised { get; private set; }

        public void Init()
        {
            if (IsInitialised)
            {
                return;
            }

            if (!UseOpcUa)
            {
                //Debug.Log("OpcUa is disabled in the settings, aborting OpcUaManager.Init()");
                return;
            }

            OpcUa_Siemens_Client.Logger.logCallback += (string message, int level) => { if (level == 0) Debug.Log(message); else Debug.LogError(message); };

            myconfig = new HoloOpcUaConfiguration(writingEnabled);

            foreach (OPCServerInfo opcserver in myListOpcServers)
            {
                StartSession(opcserver);
            }

            IsInitialised = true;
        }

        private void StartSession(OPCServerInfo opcserver)
        {
            if (!UseOpcUa) return;

            HoloOpcSession opcsession = new HoloOpcSession(myconfig);
            opcsession.OnDisconnectionEventHandler += OnOpcSessionDisconnected;
            myListOpcSessions.Add(opcsession);

            //Debug.Log("Starting Opc Session: " + opcserver.endpoint);

            if (opcsession.isConnected) opcsession.disconnect();

            //Debug.Log("OPC Session set endpoint: " + opcserver.endpoint);
            if (!opcsession.setEndpoint(opcserver.endpoint))
            {
                statusText?.SetColor(Color.red);
                statusText?.SetText("Endpoint problem");
                statusStr = "Endpoint problem.";
                statusColor = Color.red;

                Debug.LogError("Set Endpoint failed: " + opcserver.endpoint);
                return;
            }

            Debug.LogWarning("Set Endpoint success: " + opcserver.endpoint);
            connected = opcsession.connect();
            Debug.LogWarning("Connected: " + connected);

            statusText?.SetColor(connected ? Color.green : Color.red);
            statusText?.SetText(connected ? "Connected" : "Failed");
            if (connected)
            {
                statusStr = "Connected";
                statusColor = Color.green;
            }
            else
            {
                statusStr = "Connection failed";
                statusColor = Color.red;
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            foreach (var session in myListOpcSessions)
            {
                session.disconnect();
            }
        }

        public SubscriptionSet CreateSubscriptionSet(OpcSettings settings, IEnumerable<MonitoredDataBase> subsetData, Action<MonitoredDataBase> OnValueUpdatedCallback, bool activate = false)
        {
            var sess = GetOpcSession(settings.serverEndpoint, true);
            if (sess == null) return null;

            

            var subset = new SubscriptionSet(sess, settings.subscriptionTime);

            foreach (var item in subsetData)
            {
                subset.addItem((MonitoredDataBase)item);
                item.m_session = sess;
            }

            if (OnValueUpdatedCallback != null) subset.NotificationRaised += (object sender, NotificationRaisedEventArgs e) => { OnOpcValueUpdateArrived(e, OnValueUpdatedCallback); };

            if (activate) subset.activate();



            //var items = subset.m_monitoredItems;

            return subset;
        }

        public HoloOpcSession GetOpcSession(string serverUrl, bool validate)
        {
            var sess = myListOpcSessions.Find(x => x.m_endpointURL == serverUrl);
            if (sess == null)
            {
                Debug.LogError("Could not find session with url: " + serverUrl);
            }
            else if (validate && !IsValidSession(sess))
            {
                sess = null;
                Debug.LogError("Invalid opc session, url: " + serverUrl);
            }

            return sess;
        }

        public bool IsValidOpcAdress(string adress)
        {
            return myListOpcServers.Exists(x => x.endpoint == adress);
        }

        public static bool IsValidSession(HoloOpcSession session)
        {
            if (session == null) return false;
            //#if !UNITY_EDITOR
            //if (session.m_session == null) return false;
            //#endif

            return true;
        }

        public static void AssignSession<T>(HoloOpcSession session, IEnumerable<T> data) where T : GenericDataBase
        {
            foreach (var item in data)
            {
                item.m_session = session;
            }
        }

        public static void AssignSession<T>(OpcSettings settings, IEnumerable<T> data) where T : GenericDataBase
        {
            var sess = Instance.GetOpcSession(settings.serverEndpoint, true);
            if (sess == null) return;
            foreach (var item in data)
            {
                item.m_session = sess;
            }
        }

        public void AddOpcServerInfo(OPCServerInfo myOPCServerInfo, bool startSession)
        {
            if (!myListOpcServers.Exists(x => x == myOPCServerInfo))
            {
                //Debug.Log("Adding new opc server to server list: " + myOPCServerInfo.endpoint);

                myListOpcServers.Add(myOPCServerInfo);

                if (startSession) StartSession(myOPCServerInfo);
            }
        }

        public void ActivateSubscriptionSet(SubscriptionSet subset)
        {
            if (subset != null)
            {
                subset.activate();
            }
            else
            {
                Debug.LogError("Couldn't activate subset, subset is null");
            }
        }

        public void OnOpcValueUpdateArrived(NotificationRaisedEventArgs e, Action<MonitoredDataBase> callbackFunction)
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() => { callbackFunction?.Invoke(e.dataRaised); });
        }

        public void OnOpcSessionDisconnected(object sender, OnDisconnectEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("++++++++++HOLO SIEMENS: Session disconnected" + e.m_message);
        }

    }
}