﻿using System.Collections;
using System.Collections.Generic;
using OpcUa_Siemens_Client;
using UnityEngine;

namespace Shared
{
    public class MonitoredDataWrapper<T> : MonitoredDataWrapperBase<T>
    {

        public MonitoredDataWrapper() { }


        public MonitoredDataWrapper(MonitoredData<T> data, System.Action<T> callback)
        {
            MonitoredData = data;
            AddOnValueChangedListener(callback);
        }
    }
}
