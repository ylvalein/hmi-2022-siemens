﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shared;

public class ConnectedDataAlarm : ConnectedDataArray<bool>
{
   
    public List<GameObject> AlarmMessage = new List<GameObject>();

    protected override void OnDataChanged(int index, bool value, bool multipleValuesChangedFinished = true)
    {
        AlarmMessage[index].SetActive(value);
    }
}
