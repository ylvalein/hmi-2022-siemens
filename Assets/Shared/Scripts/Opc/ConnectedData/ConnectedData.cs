﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
public abstract class ConnectedData<T> : ConnectedDataArray<T> {

    public new int ArraySize {
        get {
            return 1;
        }
    }

    private new T GetCurrentValue(int i) {
        return iDataBackend.GetCurrentValue(i);
    }

    private new void SetCurrentValue(int i, T value) {
        iDataBackend.Write(i, value);
    }

    public T GetCurrentValue() {
        return GetCurrentValue(0);
    }

    public void SetCurrentValue(T value) {
        SetCurrentValue(0, value);
    }

    protected override void OnDataChanged(int index, T value, bool multipleValuesChangedFinished = true) {
        OnDataChanged(value);
    }

    protected abstract void OnDataChanged(T value);
}
}
