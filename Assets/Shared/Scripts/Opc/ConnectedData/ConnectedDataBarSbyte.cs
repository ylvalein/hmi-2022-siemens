﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Shared
{

    public class ConnectedDataBarSbyte : ConnectedDataArray<sbyte>
    {
        public BarManager barManager;

        [Header("Alternative ArraySize")]
        [Tooltip("Alternative way if ArraySize does not work. Enable the hardcoded ArraySize.")]
        public bool useAlternativeArraySize = false;
        [Tooltip("Hardcoded ArraySize.")]
        public int alternativeArraySize = 5;

        public override void Awake()
        {
            base.Awake();

            //TODO: Check why ArraySize does not always work 
            if (useAlternativeArraySize)
            {
                barManager.SetNumberOfBars(alternativeArraySize);
            }
            else
            {
                barManager.SetNumberOfBars(ArraySize);
            }

        }

        protected override void OnDataChanged(int index, sbyte value, bool multipleValuesChangedFinished = true)
        {
            barManager.OnValueChanged(index, (float)value, multipleValuesChangedFinished);
        }
    }

}