﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shared;
using UnityEngine.UI;

public class ConnectedDataOperatingModeSMA : ConnectedData<System.Int16>
{
    public Text OperatingMode;
    protected override void OnDataChanged(short value)
    {
        switch (value)
        {
            case 0:
                OperatingMode.text = "Inaktiv";
                break;
            case 1:
                OperatingMode.text = "Grundstellung";
                break;
            case 2:
                OperatingMode.text = "Automatik aktiv";

                break;
            case 3:
                OperatingMode.text = "Handbetrieb";
                break;
            case 9:
                OperatingMode.text = "Richten vorgewählt";
                break;
            case 10:
                OperatingMode.text = "Richten aktiv";
                break;
            case 12:
                OperatingMode.text = "Automatik vorgewählt";
                break;
            default:
                break;
        }
    }
}



