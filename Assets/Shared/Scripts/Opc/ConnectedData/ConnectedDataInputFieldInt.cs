﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

using UnityEngine.UI;

namespace Shared
{
    public class ConnectedDataInputFieldInt : ConnectedData<int>
    {
        //TODO: Check if InputField works instead of KeyboardInputField
        //public KeyboardInputField targetInput;
        public InputField targetInput;

        protected override void OnDataChanged(int value)
        {

        }


        public void setTextfieldVariable()
        {
            if (targetInput == null)
            {
                Debug.LogError("target input is nulll");
                return;
            }
            string input = targetInput.text;
            if (string.IsNullOrEmpty(input))
            {
                Debug.LogError("string is null or empty");
                return;
            }
            int output = Convert.ToInt32(input);
            SetCurrentValue(output);

        }
    }
}