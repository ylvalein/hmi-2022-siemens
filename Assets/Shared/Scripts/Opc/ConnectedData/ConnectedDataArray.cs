﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ConnectedDataArray<T> : MonoBehaviour {

    public int ArraySize{
        get {
            return iDataBackend.ArraySize;
        }
    }

    public virtual void Awake() {
        if (dataBackend != null) {
            SetDataBackend(dataBackend);
        }
    }

    [SerializeField]
    private Component dataBackend;
    public IDataBackend<T> iDataBackend { get; private set; }

    public T GetCurrentValue(int i) {
        return iDataBackend.GetCurrentValue(i); 
    }

    public int GetArraySize()
    {
        return ArraySize;  //Size of the DataBackendArray
    }

    public void SetCurrentValue(int i, T value) {
        iDataBackend.Write(i, value);
    }

    public void SetDataBackend(Component dataConnector) {
        this.dataBackend = dataConnector;
        iDataBackend = (IDataBackend<T>) dataConnector;
        iDataBackend.AddOnValueChangedListener(OnDataChanged);
    }

    protected abstract void OnDataChanged(int index, T value, bool multipleValuesChangedFinished = true);
}
