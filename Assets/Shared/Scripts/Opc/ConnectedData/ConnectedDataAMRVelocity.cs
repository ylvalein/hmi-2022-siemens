using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shared;
using TMPro;

public class ConnectedDataAMRVelocity : ConnectedDataArray<double>
{
    [SerializeField]
    public float x_vel, y_vel, a_vel;
    public TextMeshProUGUI[] opcValueText;
    public AnimationController aniCon;

    //public GameObject AMR, AnimationManager;


    private void Start()
    {

    }
    private void Update()
    {
  
    }

    protected override void OnDataChanged(int index, double value, bool multipleValuesChangedFinished = true)
    { 
        switch (index) {
            case 0:
                //change x position of amr
                x_vel = (float)value;
                aniCon.SetVelocity(index, x_vel);
                break;
            case 1:
                //change z position of amr
                y_vel =(float)value;
                aniCon.SetVelocity(index, x_vel);
                break;
            case 2:
                //change rotation amr
                a_vel = (float)value;
                aniCon.SetVelocity(index, x_vel);
                break;
        }
        opcValueText[index].text = value.ToString();


    }


}