using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shared;
using TMPro;

public class ConnectedDataAMREnergy : ConnectedDataArray<double>
{
    [SerializeField]
    public float voltage, current, power, soc;
    private DataBackendOpc<double> dataBackend;
    public TextMeshProUGUI[] opcValueText;
    public TrendManager trendManager;
    public EnergyOverview energyOverview;
    public bool kpiON;
    public GameObject kpiScreen;
    public GameObject energyScreen;


    private void Start()
    {
        kpiON = false;
        //InvokeRepeating("AmrEnergyLog", 5, 2f);
    }

    protected override void OnDataChanged(int index, double value, bool multipleValuesChangedFinished = true)
    {

        switch (index)
        {
            case 0:
                voltage = (float)value;
                energyOverview.OnUpdateValueSystemVoltage(voltage);
                break;
            case 1:
                double cur = value;
                current = (float)value;
                if (!kpiON) {
                    kpiScreen.SetActive(false);
                    energyScreen.SetActive(true);
                    kpiON = true;
                    }
                trendManager.OnValueChanged(current);
                energyOverview.OnUpdateValueSystemCurrent(cur);
                break;
            case 2:
                power = (float)value;
                energyOverview.OnUpdateValuePowerUsage(power);
                break;
            case 3:
                soc = (float)value;
                energyOverview.OnUpdateValueSOC(soc);
                break;

        }

        opcValueText[index].text = value.ToString();
    }

    public void AmrEnergyLog()
    {
        Debug.LogWarning($"voltage: {voltage}");
        Debug.LogWarning($"current: {current}");
        Debug.LogWarning($"power: {power}");
        Debug.LogWarning($"soc: {soc}");
    }
}