﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Shared
{
    public class ConnectedDataTrend : ConnectedData<float>
    {

        public TrendManager trendManager;

        protected override void OnDataChanged(float value)
        {
            Debug.Log("Value!!!!!!!!!!!!!!!!!!!!!!!!!!!" + value);
            trendManager.OnValueChanged(value);
        }
    }

}
