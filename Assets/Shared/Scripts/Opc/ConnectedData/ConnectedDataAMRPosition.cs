using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shared;
using TMPro;

public class ConnectedDataAMRPosition : ConnectedDataArray<double>
{
    [SerializeField]
    public float x, y, a;
    public TextMeshProUGUI[] opcValueText;

    public GameObject AMR, AnimationManager;
    [SerializeField]
    private bool initiated;

    private void Start()
    {
        //InvokeRepeating("AmrPositionLog", 5, 2f);
        initiated = false;
    }
    private void Update()
    {
  
    }

    protected override void OnDataChanged(int index, double value, bool multipleValuesChangedFinished = true)
    { 
        switch (index) {
            case 0:
                //change x position of amr
                x = (float)value;
                break;
            case 1:
                //change z position of amr
                y =(float)value;
                break;
            case 2:
                //change rotation amr
                a = (float)value;
                break;
        }
        opcValueText[index].text = value.ToString();

        if (!initiated && x != 0 && y != 0 && a != 0)
        {
            // set AMR to initial positons
            AMR.transform.localPosition = new Vector3(x, 0, y);
            AMR.transform.localRotation = Quaternion.Euler(0, a, 0);
            initiated = true;
            AMR.GetComponent<Position_updater>().Initiate();
            return;
        }

        // send changed Data to
        if (initiated)
        {

            if (index == 0 || index == 1)
            {
                //send position Data
                AMR.GetComponent<Position_updater>().UpdatePosition(index, (float)value);

            }
            else if(index == 2)
            {
                //send roation Data
                AMR.GetComponent<Position_updater>().UpdatePosition(index, (float)value);
            }
        }
    }

    public void AmrPositionLog()
    {
        Debug.LogWarning($"x: {x}");
        Debug.LogWarning($"y: {y}");
        Debug.LogWarning($"a: {a}");
        //Debug.LogWarning($"Line {c++}");
    }
}