﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shared;

public class ConnectedDataTrendInt64 : ConnectedData<long>
{
    public TrendManager trendManager;
    public bool useAbsoluteValue = false;

    protected override void OnDataChanged(long value)
    {
        if (useAbsoluteValue)
        {
            
            value = (long)Mathf.Abs(value);
        }

        trendManager.OnValueChanged(value);
    }
}
