﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Shared {
    public class ConnectedDataGauge : ConnectedData<float> {
        public GaugeManager gaugeManager;
        protected override void OnDataChanged(float value) {
            
            gaugeManager.OnValueChanged(value);
        }
    }
}
