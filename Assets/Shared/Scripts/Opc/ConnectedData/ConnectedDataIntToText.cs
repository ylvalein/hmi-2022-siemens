﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shared;
using UnityEngine.UI;

public class ConnectedDataIntToText : ConnectedData<int>
{
    public Text output;
    protected override void OnDataChanged(int value)
    {
        output.text = value.ToString();
    }
}
