using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shared;
using TMPro;

public class ConnectedDataAMRSafety : ConnectedDataArray<bool>
{
    [SerializeField]
    public bool warn1, stop1, warn2, stop2;
    public TextMeshProUGUI[] opcValueText;
    public SpriteRenderer save_small, save_large, save_small_2, save_large_2, save_back_small, save_back_large;
    public Material small_false, small_true, large_false, large_true;

    private void Start()
    {
        //InvokeRepeating("AmrSafetyLog", 5, 2f);
    }

    protected override void OnDataChanged(int index, bool value, bool multipleValuesChangedFinished = true)
    {
        //Debug.LogWarning($"AMR Safety variable with index {index} changed to {value}");

        switch (index)
        {
            case 0:
                warn1 = value;
                if (warn1)
                {
                    save_large.material = large_true;
                }
                else
                {
                    if (!stop1) save_large.material = large_false;
                }
                break;
            case 1:
                stop1 = value;
                if (stop1)
                {
                    save_small.material = small_true;
                    save_large.material = large_true;
                }
                else
                {
                    if (!warn1) save_large.material = large_false;
                    save_small.material = small_false;
                }
                break;
            case 2:
                warn2 = value;
                if (warn2)
                {
                    save_back_large.material = large_true;
                }
                else
                {
                    if (!stop2) save_back_large.material = large_false;
                }
                break;
            case 3:
                stop2 = value;
                if (stop2)
                {
                    save_back_small.material = small_true;
                    save_back_large.material = large_true;
                }
                else
                {
                    if (!warn2) save_back_large.material = large_false;
                    save_back_small.material = small_false;
                }
                break;
        }

        opcValueText[index].text = value.ToString();
    }

    public void AmrSafetyLog()
    {
        Debug.LogWarning($"Warn1: {warn1}");
        Debug.LogWarning($"Stop1: {stop1}");
        Debug.LogWarning($"Warn2: {warn2}");
        Debug.LogWarning($"Stop2: {stop2}");
    }

}