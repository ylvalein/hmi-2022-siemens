﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared
{

    public class ConnectedDataButton : ConnectedData<bool>
    {
        public ButtonMemory btn;

        protected override void OnDataChanged(bool value)
        {
            if(btn!=null)
                btn.ActivateButton(value);
        }


        public void resetBit()
        {
            bool value = false;
            Debug.Log("Reset" + value);
            SetCurrentValue(value);
            //write tag to opc here

        }
        public void setBit()
        {
            bool value = true;
            Debug.Log("Set" + value);
            SetCurrentValue(value);
            //write tag to opc here
        }

        public void changeBit()
        {
            SetCurrentValue(!btn.isSelected);
        }

    }
}