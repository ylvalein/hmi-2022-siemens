﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Shared {

    public class ConnectedDataBar : ConnectedDataArray<float> {
        public BarManager barManager;

        public override void Awake() {
            base.Awake();
            barManager.SetNumberOfBars(ArraySize);
        }

        protected override void OnDataChanged(int index, float value, bool multipleValuesChangedFinished = true) {
            barManager.OnValueChanged(index, value, multipleValuesChangedFinished);
        }
    }

}