﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shared;
using UnityEngine.UI;
using System;


namespace Shared
{

    public class ConnectedDataInputFieldFloat : ConnectedData<float>
    {
        //TODO: Check if InputField works instead of KeyboardInputField
        //public KeyboardInputField targetInput;
        public InputField targetInput;

        protected override void OnDataChanged(float value)
        {

        }


        public void setTextfieldVariable()
        {
            if (targetInput == null) {
                Debug.LogError("target input is nulll");
                return;
            }
            string input = targetInput.text;
            if(string.IsNullOrEmpty(input))
            {
                Debug.LogError("string is null or empty");
                return;
            }
            float output = Convert.ToSingle(input);
            SetCurrentValue(output);

        }
    }
}