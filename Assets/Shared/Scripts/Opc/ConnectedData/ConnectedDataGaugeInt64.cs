﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shared;

namespace shared
{
    public class ConnectedDataGaugeInt64 : ConnectedData<System.Int64>
    {
        public GaugeManager gaugeManager;

        protected override void OnDataChanged(long value)
        {
           
            gaugeManager.OnValueChanged(value);
        }
    }
}
