﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shared;
using UnityEngine.UI;

public class ConnectedDataMagazin : ConnectedDataArray<bool>
{
    public Text output;
    private bool[] states = new bool[2];
    protected override void OnDataChanged(int index, bool value, bool multipleValuesChangedFinished = true)
    {
        states[index] = value;

        if (states[0] == true)
        {
            output.text = "Fast Leer";
            output.color = Color.yellow;
        }
        if (states[1] == true)
        {
            output.text = "Leer";
            output.color = Color.red;
        }
        else
        {
            output.text = "Gefüllt";
            output.color = Color.green;
        }

    }
}
