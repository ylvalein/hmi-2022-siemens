﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shared;

public class ConnectedDataTrendInt16 : ConnectedData<System.Int16>
{

    public TrendManager trendManager;

    protected override void OnDataChanged(short value)
    {
        trendManager.OnValueChanged(value);
    }
    
}
