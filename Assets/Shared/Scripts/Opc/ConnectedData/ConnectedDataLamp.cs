﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace Shared
{
    public class ConnectedDataLamp : ConnectedData<bool>
    {
        public Color ColorTrue=new Color();
        public Color ColorFalse=new Color();
        public RawImage TargetImage;

        protected override void OnDataChanged(bool value)
        {
            if (value == true)
               TargetImage.color = ColorTrue;

            else
                TargetImage.color = ColorFalse;
        }

    }
}