﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shared;
using UnityEngine.UI;

public class ConnectedDataStringArray : ConnectedData<string[]>
{
    
    public List<Text> Texts = new List<Text>();
    protected override void OnDataChanged(string[] value)
    {
        for(int i = 0; i < Texts.Count; i++)
        {
            Texts[i].text = value[i];
        }
    }

}
