﻿using OpcUa_Siemens_Client;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    public abstract class SubscriptionSetCreatorBase: MonoBehaviour
    {
        public bool IsInitialized { get; private set; }
        Action onInitializedCallback;

        public void AddOnInitializedListener(Action listener)
        {
            if (!IsInitialized)
            {
                onInitializedCallback += listener;
            }
            else
            {
                listener.Invoke();
            }
        }

        public bool autoInitOnAwake = true;

        public HoloOpcSession mySession;
        protected SubscriptionSet mysubset;
        [Lookup]
        public OpcManager opcManager;

        public void Awake()
        {
            if (autoInitOnAwake) Init();
        }

        private void OnDestroy()
        {
            if (mysubset != null) mysubset.remove();
        }

        public MonitoredData<T> FindByType<T>(string type, int id)
        {
            if (!IsInitialized) return null;
            if (mysubset == null) return null;
            var _type = typeof(T);

            MonitoredData<T> targetData = null;

            foreach (var item in mysubset.m_monitoredItems)
            {
                try
                {
                    var castedItem = (MonitoredDataWithTypeAndId<T>)item;
                    if (castedItem.m_type == type && castedItem.m_index == id)
                    {
                        targetData = (MonitoredData < T > ) item;
                        break;
                    }
                }
                catch
                {
                    continue;
                }
            }

            if (targetData == null)
            {
                Debug.LogError("Could not find monitored Data with type, id:" + type + ", " + id);
            }
            return targetData;
        }

        public MonitoredData<T> FindByName<T>(string name)
        {
            if (!IsInitialized) return null;
            if (mysubset == null) return null;
            var targetData = mysubset.GetMonitoredData<T>(name);
            if (targetData == null) Debug.LogError("Could not find monitored Data with name: " + name);
            return targetData;
        }

        public void WriteValue<T>(string name, T value)
        {
            var data = FindByName<T>(name);

            if (data == null) throw new Exception("Could not find data with name: " + name);

            data.Value = value;
            mySession.write(data as GenericData<T>);
        }

        public T GetValue<T>(string name)
        {
            var targetValue = mysubset.GetMonitoredData<T>(name);
            if (targetValue != null)
            {
                return targetValue.Value;
            }

            throw new Exception("Could not find data with name: " + name);
        }

        protected abstract SubscriptionSet InitSubset();
        protected abstract bool InitController();
        protected abstract string GetServerEndpoint();

        public virtual HoloOpcSession InitSession()
        {
            var session = opcManager.GetOpcSession(GetServerEndpoint(), true);
            return session;
        }

        public void Init(string address = null)
        {
            if (IsInitialized) return;

            if (InitController())
            {
                mySession = InitSession();
                if (mySession != null)
                {

                    mysubset = InitSubset();
                    if (mysubset != null)
                    {

                        IsInitialized = true;
                        onInitializedCallback?.Invoke();
                        return;
                    }
                }
            }

            Debug.LogError("Subset Init failed: " + gameObject.name);
        }
    }
}
