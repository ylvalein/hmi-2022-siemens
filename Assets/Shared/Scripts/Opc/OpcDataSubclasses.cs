﻿using OpcUa_Siemens_Client;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Shared
{
    /*
    public abstract class MonitoredDataWithTypeAndIdBase : MonitoredData<object>
    {
        public string m_type;
        public int m_index;

        public MonitoredDataWithTypeAndIdBase(string name, string nodeId, string type, int index) : base(name, nodeId)
        {
            m_type = type;
            m_index = index;
        }
    }
    */
    public class MonitoredDataWithTypeAndId<T> : MonitoredData<T> {
        public Action<T> onValueChangedListeners;

        public string m_type;
        public int m_index;

        public MonitoredDataWithTypeAndId(string name, string nodeId, string type, int index) : base(name, nodeId) {
            m_type = type;
            m_index = index;
        }

        public override void OnValueChanged() {
            onValueChangedListeners.Invoke(Value);
        }
    }

    /*
    public class GenericDataWithTypeBase : GenericDataBase {
        public string m_type;

        public GenericDataWithTypeBase(string name, string nodeId, string type) : base(name, nodeId)
        {
            m_type = type;
        }
    }
    */
    public class GenericDataWithType<T> : GenericData<T> {
        public string m_type;
        public GenericDataWithType(string name, string nodeId, string type) : base(name, nodeId) {
            m_type = type;
        }
    }
    /*

    public class GenericDataWithTypeAndIdBase : GenericDataWithTypeBase {
        public int m_index;

        public GenericDataWithTypeAndIdBase(string name, string nodeId, string type, int index) : base(name, nodeId, type) {
            m_index = index;
        }
    }
    */
    public class GenericDataWithTypeAndId<T> : GenericDataWithType<T> {
        public int m_index;

        public GenericDataWithTypeAndId(string name, string nodeId, string type, int index) : base(name, nodeId, type) {
            m_index = index;
        }
    }
    /*
    public class GenericDataWithTypeAndTwoIdsBase : GenericDataWithTypeAndIdBase {
        public string nodeIdTrue;
        public string nodeIdFalse;
        public GenericDataWithTypeAndTwoIdsBase(string name, string nodeIdTrue, string nodeIdFalse, string type, int index) : base(name, string.Empty, type, index) {
            this.nodeIdTrue = nodeIdTrue;
            this.nodeIdFalse = nodeIdFalse;
        }
    }
    */
    public class GenericDataWithTypeAndTwoIds<T> : GenericDataWithTypeAndId<T> {
        public string nodeIdTrue;
        public string nodeIdFalse;
        public GenericDataWithTypeAndTwoIds(string name, string nodeIdTrue, string nodeIdFalse, string type, int index) : base(name, string.Empty,type,index) {
            this.nodeIdTrue = nodeIdTrue;
            this.nodeIdFalse = nodeIdFalse;
        }
    }
}