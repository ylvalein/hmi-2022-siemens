﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using System.Xml.Linq;

namespace Shared
{
    public enum ServerType
    {
        RealOPCServer = 0,
        ReferenceServer,
        useSettings
    }



    public class OpcSessionsFromXml : MonoBehaviour, IInitRequired
    {
        public OpcManager opcManager;
        public TextAsset xmlOPCReal, xmlOPCReference;


        //Instead of using the settings
        public ServerType serverType = ServerType.RealOPCServer;


        private void XmlOpcUpdate()
        {
            Debug.Log("Verzeichnis" + System.IO.Directory.GetCurrentDirectory());
            string directory = System.IO.Directory.GetCurrentDirectory();

            string filename = directory + "\\Assets\\Xml\\file.xml";
            Debug.Log(filename);

            XDocument xdoc = XDocument.Load(filename);
            var element = xdoc.Root.Element("OPCParameters");
            Debug.Log("element:"+element);

            element.Value = "foo";
            xdoc.Save(filename);
        }
        public void Init()
        {
            //XmlOpcUpdate();
            var settings = (ProjectSettingsBase)AbstractSettingsManagerBase.settings;
            TextAsset xmlDocAsset;

            if (serverType == ServerType.useSettings)
            {
                if (settings.UseOpcReferenceServer) xmlDocAsset = xmlOPCReference;
                else xmlDocAsset = xmlOPCReal;
            }
            else
            {
                xmlDocAsset = serverType == ServerType.RealOPCServer ? xmlOPCReal : xmlOPCReference;
            }

            if (!xmlDocAsset)
            {
                Debug.LogError("No Opc Server xml file assigned, couldn't set sessions in OpcUaManager"); 
                return;
            }

            var xmlDoc = XMLPersistance.LoadPersistanceXml(xmlDocAsset);

            AddOpcSessions(xmlDoc);
        }

        public void AddOpcSessions(XmlDocument xmlDoc)
        {
            OpcManager.OPCServerInfo opcserverinfo1;
            foreach (XmlNode node1 in xmlDoc.DocumentElement.ChildNodes)
            {
                if (node1.Name.Equals("OPCServer"))
                {
                    opcserverinfo1 = new OpcManager.OPCServerInfo();

                    foreach (XmlNode node in node1.ChildNodes)
                    {
                        if (node.Name.Equals("EndpointURL"))
                            opcserverinfo1.endpoint = node.InnerText;

                        else if (node.Name.Equals("Username"))
                        {
                            opcserverinfo1.username = node.InnerText;
                        }
                        else if (node.Name.Equals("Password"))
                        {
                            opcserverinfo1.password = node.InnerText;
                        }
                    }

                    opcManager.AddOpcServerInfo(opcserverinfo1, opcManager.IsInitialised);
                }
            }
        }
    }
}
