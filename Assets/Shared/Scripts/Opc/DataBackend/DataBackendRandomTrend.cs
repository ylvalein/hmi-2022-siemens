﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Shared {
    public class DataBackendRandomTrend : DataBackendRandom<float>, IDataBackend<float> {
        public TrendManager trendManager;

        public override void Awake() {
            base.Awake();
        }

        public void Start() {
            trendManager.YDrift = trendManager.fixedMaxValue / 2f;
            trendManager.OnValueChanged(trendManager.fixedMaxValue / 2f);
        }

        public override void RandomUpdate() {

            float randomValue;
            if (Input.GetMouseButton(0)) {
                randomValue = (Input.mousePosition.y / 3f);
            } else {
                var yData = trendManager.YData;
                trendManager.YDrift += (UnityEngine.Random.value - 0.499f) * 10;
                float ytemp = yData.Last.Value + (trendManager.YDrift * 2 - yData.Last.Value * 0.2f);
                if (ytemp < 0 && trendManager.YDrift < 0) trendManager.YDrift = -trendManager.YDrift;
                randomValue = trendManager.YDrift;
            }

            currentValue = randomValue;
            onDataChangedCallback?.Invoke(randomValue);
        }

        protected Action<float> onDataChangedCallback;

        protected float currentValue;

        public int ArraySize {
            get {
                return 1;
            }
        }

        public float GetCurrentValue(int index) {
            return currentValue;
        }

        public void Write(int index, float newValue) { }

        public void AddOnValueChangedListener(Action<int, float, bool> callback) {
            onDataChangedCallback += (float value) => { callback.Invoke(0, value, true); };
        }
    }
}