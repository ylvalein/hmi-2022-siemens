﻿using OpcUa_Siemens_Client;
using Shared;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared {
    public class DataBackendOpcSeperateReadWrite<T> : DataBackendOpc<T> {
        List<GenericDataBase> genericDataForWrite = new List<GenericDataBase>();
        public XmlParser_SensorActuatorData xmlParserData;

        public override void SetData(int index) {
            base.SetData(index);

            if (xmlParserData == null) xmlParserData = ((SubscriptionSetCreatorXml_SensorActuatorData)opcSubset).GetParser();

            GenericDataBase element = null;
            switch (dataBackendEntries[index].dataBackendEntryType)
            {
                case DataBackendEntry.DataBackendEntryType.Name:
                     element = xmlParserData.listWrite.Find(x => x.m_name == dataBackendEntries[index].name);
                    break;
                case DataBackendEntry.DataBackendEntryType.TypeAndId:
                    foreach(var listElement in xmlParserData.listWrite)
                    {
                        try
                        {
                            var _listElement = (GenericDataWithTypeAndId<T>)listElement;
                            if (_listElement.m_index == dataBackendEntries[index].id && _listElement.m_type == dataBackendEntries[index].type) element = listElement;
                        }
                        catch
                        {
                            continue;
                        }
                    }
                  
                    break;
                default: element = null;break;
            }

            genericDataForWrite.Add(
                element
            );
        }

        public override void Write(int index, T newValue) {
            if (!opcSubset.IsInitialized) {
                Debug.LogError("Couldn't write value, subset is not initialized");
                return;
            }

            var valueItem = genericDataForWrite[index];

            if (valueItem is GenericDataWithTypeAndTwoIds<T>) {
                if(typeof(T) == typeof(bool)) {
                    var currentValue = bool.Parse(newValue.ToString());
                    if(currentValue == false) {
                        ((GenericDataWithTypeAndTwoIds<T>)valueItem).m_nodeId = ((GenericDataWithTypeAndTwoIds<T>)valueItem).nodeIdFalse;
                        valueItem.Write(false);
                        ((GenericDataWithTypeAndTwoIds<T>)valueItem).m_nodeId = ((GenericDataWithTypeAndTwoIds<T>)valueItem).nodeIdTrue;
                        valueItem.Write(true);
                    }
                    else
                    {
                        ((GenericDataWithTypeAndTwoIds<T>)valueItem).m_nodeId = ((GenericDataWithTypeAndTwoIds<T>)valueItem).nodeIdTrue;
                        valueItem.Write(false);
                        ((GenericDataWithTypeAndTwoIds<T>)valueItem).m_nodeId = ((GenericDataWithTypeAndTwoIds<T>)valueItem).nodeIdFalse;
                        valueItem.Write(true);
                    }
                } else {
                    throw new System.Exception("GenericDataWithTypeAndTwoIds can only be used with bool values");
                }
            } else {
                valueItem.Write(newValue);
            }
        }
    }
}
