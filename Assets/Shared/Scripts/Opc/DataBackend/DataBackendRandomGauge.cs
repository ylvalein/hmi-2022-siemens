﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Shared {
    public class DataBackendRandomGauge : DataBackendRandom<float>, IDataBackend<float> {

        public GaugeManager gaugeManager;
        public override void RandomUpdate() {
            gaugeManager.cDrift -= 1f;

            if (gaugeManager.cDrift == 0)
                gaugeManager.cDrift = (int)UnityEngine.Random.Range(gaugeManager.minValue, gaugeManager.maxValue);

            onDataChangedCallback?.Invoke(0, Mathf.Clamp(gaugeManager.cDrift, gaugeManager.minValue, gaugeManager.maxValue), true);
        }

        Action<int, float, bool> onDataChangedCallback;

        protected float currentValue;

        public int ArraySize {
            get {
                return 1;
            }
        }

        public float GetCurrentValue(int index) {
            return currentValue;
        }

        public void Write(int index, float newValue) { }

        public void AddOnValueChangedListener(Action<int, float, bool> callback) {
            onDataChangedCallback += callback;
        }
    }
}