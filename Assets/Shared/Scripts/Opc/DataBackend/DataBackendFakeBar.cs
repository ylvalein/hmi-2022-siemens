﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shared;
using System;

public class DataBackendFakeBar : DataBackendRandom<float>, IDataBackend<float>
{
    public float fixedValue;

    public BarManager barManager;

    Action<int, float, bool> onValueChangedCallback;

    float[] currentValue;
    
    
    public override void Awake()
    {
        base.Awake();
        currentValue = new float[ArraySize];
    }

    public int ArraySize
    {
        get
        {
            return barManager.sizeofvalues;
        }
    }

    public void AddOnValueChangedListener(Action<int, float, bool> callback)
    {
        onValueChangedCallback += callback;
    }

    public float GetCurrentValue(int index)
    {
        return currentValue[index];
    }

    public override void RandomUpdate()
    {

        for (int i = 0; i < ArraySize; i++)
        {
            var newValue = fixedValue;
            currentValue[i] = newValue;

            onValueChangedCallback.Invoke(i, newValue, i == (ArraySize - 1));
        }
    }

    public void Write(int index, float newValue) { }
}

