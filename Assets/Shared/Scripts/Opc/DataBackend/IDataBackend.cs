﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interface to connect ui elements with read (& subscription) / write function to a data backend
/// </summary>
/// <typeparam name="T"></typeparam>
public interface IDataBackendBase {

}

public interface IDataBackend<T> : IDataBackendBase {

    int ArraySize { get; }
    T GetCurrentValue(int index);

    void Write(int index, T newValue);
    void AddOnValueChangedListener(System.Action<int, T, bool> callback);
}
