﻿
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    

    [System.Serializable]
    public class DataBackendEntry {
        [System.Serializable]
        public enum DataBackendEntryType {
            Name,
            TypeAndId
        }
        public DataBackendEntryType dataBackendEntryType;
        public string name;
        public string type;
        public int id;
    }

    public abstract class DataBackendOpcBase : MonoBehaviour
    {
        public SubscriptionSetCreatorBase opcSubset;

        public List<DataBackendEntry> dataBackendEntries = new List<DataBackendEntry>();

        public void Awake()
        {
            opcSubset?.AddOnInitializedListener(Init);
        }

        public void SetOpcSubscriptionSet(SubscriptionSetCreatorBase opcSubset) {
            this.opcSubset = opcSubset;
            opcSubset.AddOnInitializedListener(Init);
        }

        public abstract void Init();

        public DataBackendEntry GetDataBackendEntry(int i) {
            return dataBackendEntries[i];
        }
    }
}
