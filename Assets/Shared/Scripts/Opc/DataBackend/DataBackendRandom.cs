﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Shared
{


    public abstract class DataBackendRandom<T> : AbstractRandomMode
    {
        public override void Awake()
        {
            base.Awake();
            if (GetRandomModeEnabled())
            {
                GetComponent<ConnectedDataArray<T>>().SetDataBackend(this);

                var comps = GetComponents<Component>();
                foreach (var comp in comps)
                {
                    if (comp == this) continue;

                    // TODO: Search for an alternative for DestroyImmediate
                    IDataBackendBase interfaceOnComponent = comp as IDataBackendBase;
                    if (interfaceOnComponent != null )
                    {
                        Debug.Log(this.GetInstanceID().ToString() + " wants to destroy " + comp.GetInstanceID().ToString());
                        Debug.LogWarning("Destroying script of type " + comp.GetType() + " on " + this.gameObject.name);                     
                        DestroyImmediate(comp);
                    }

                }
            }
        }
    }
}
