﻿using OpcUa_Siemens_Client;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared {
    public abstract class DataBackendOpc<T> : DataBackendOpcBase, IDataBackend<T> {
        List<MonitoredDataWrapper<T>> monitoredDataWrappers = new List<MonitoredDataWrapper<T>>();

        public Action<int, T, bool> onValueChangedCallback;

        public int ArraySize { get { return dataBackendEntries.Count; } }

        public override void Init() {
            SetData();
        }

        private void SetData() {
            for (int i = 0; i < ArraySize; i++) {
                SetData(i);
            }
        }

        protected MonitoredData<T> GetMonitoredData(int index) {
            switch (dataBackendEntries[index].dataBackendEntryType) {
                case DataBackendEntry.DataBackendEntryType.Name:
                return opcSubset.FindByName<T>(dataBackendEntries[index].name);
                case DataBackendEntry.DataBackendEntryType.TypeAndId:
                return (MonitoredData<T>) opcSubset.FindByType<T>(dataBackendEntries[index].type, dataBackendEntries[index].id);
                default: return null;
            }
        }

        public virtual void SetData(int index){
            var wrapper = new MonitoredDataWrapper<T>();
            monitoredDataWrappers.Add(wrapper);
            wrapper.MonitoredData = GetMonitoredData(index);
            wrapper.AddOnValueChangedListener((T value) => {
                onValueChangedCallback?.Invoke(index, value, true);
            });
        }

        public T GetCurrentValue(int index) {
            if (!opcSubset.IsInitialized) {
                Debug.LogError("Couldn't read value, subset is not initialized");
            }

            return monitoredDataWrappers[index].GetCurrentValue();
        }

        
        // added:
        public string GetName(int index)
        {
            return dataBackendEntries[index].name;
        }

        public virtual void Write(int index, T newValue) {
            if (!opcSubset.IsInitialized) {
                Debug.LogError("Couldn't write value, subset is not initialized");
                return;
            }

            monitoredDataWrappers[index].Write(newValue);
        }

        public void AddOnValueChangedListener(Action<int, T, bool> callback) {
            onValueChangedCallback += callback;
        }
    }
}
