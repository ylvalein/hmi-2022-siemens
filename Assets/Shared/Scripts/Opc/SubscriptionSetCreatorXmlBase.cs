﻿using OpcUa_Siemens_Client;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared
{
    public abstract class SubscriptionSetCreatorXmlBase<T> : SubscriptionSetCreatorBase where T : AbstractXmlParser
    {
        public TextAsset xmlFile;
        private AbstractXmlParser parser;
        public TextAsset xmlFileReferenceServer;


        protected override bool InitController()
        {
            GameObject opcManager = GameObject.Find("OpcUaManager");
            string serverType = opcManager.GetComponent<OpcSessionsFromXml>().serverType.ToString();

            parser = (AbstractXmlParser)Activator.CreateInstance(typeof(T));

            if (opcManager is null)
            {
                Debug.LogWarning("No OpcManager found!");
                return false;
            }

            if (serverType == "ReferenceServer")
            {
                Debug.Log("ReferenceServer: " + (serverType == "ReferenceServer"));
                //Debug.Log("!parser.Parse(xmlFileReferenceServer)" + (!parser.Parse(xmlFileReferenceServer)));
                //+ !parser.Parse(xmlFileReferenceServer)) ;
                if (!parser.Parse(xmlFileReferenceServer)) return false;
            }
            else
            {
                if (!parser.Parse(xmlFile)) return false;
            }

            Debug.Log("Sucess reading OpcData");
            return true;
        }

        public  T GetParser()
        {
            return (T)parser;
        }
    }
}
