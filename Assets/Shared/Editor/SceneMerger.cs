﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneMerger : MonoBehaviour {
    // Add a menu item named "Do Something" to MyMenu in the menu bar.
    [MenuItem("Scene Management/Generate Project Scene")]
    static void GenerateScene() {
        var sceneGenerated = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene);

        var sceneRawCopy = CopyAndOpenScene("Assets/Shared/Scenes/RawScene.unity");
        var sceneProjectCopy = CopyAndOpenScene("Assets/Scenes/ProjectScene.unity");

        var pathSceneRawCopy = sceneRawCopy.path;
        var pathSceneProjectCopy = sceneProjectCopy.path;

        CopyObjects(sceneRawCopy, sceneGenerated);
        CopyObjects(sceneProjectCopy, sceneGenerated);

        FileUtil.DeleteFileOrDirectory(pathSceneRawCopy);
        FileUtil.DeleteFileOrDirectory(pathSceneProjectCopy);

        EditorSceneManager.SaveScene(sceneGenerated, "Assets/Scenes/GeneratedScene.unity");
    }

    [MenuItem("Scene Management/Open Scenes to edit")]
    static void OpenSceneToEdit() {
        EditorSceneManager.SaveOpenScenes();

        EditorSceneManager.OpenScene("Assets/Shared/Scenes/RawScene.unity", OpenSceneMode.Single);
        EditorSceneManager.OpenScene("Assets/Scenes/ProjectScene.unity", OpenSceneMode.Additive);
    }

    static void CopyObjects(Scene source, Scene target) {
        EditorSceneManager.MergeScenes(source, target);
    }

    public static Scene CopyAndOpenScene(string path) {
        var copyPath = path.Insert(path.Length - (".unity").Length, "-copy");
        FileUtil.DeleteFileOrDirectory(copyPath);
        FileUtil.CopyFileOrDirectory(path, copyPath);
        return EditorSceneManager.OpenScene(copyPath, OpenSceneMode.Additive);
    }
}