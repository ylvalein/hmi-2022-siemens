﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.IO;


public class PostProcessVisualStudioDependecies : MonoBehaviour {
	static private int findLineIndex(List<string> lines, string text, int startingIndex = 0) {
		for (int i = startingIndex; i < lines.Count; i++) {
			var line = lines[i];
			var index = line.IndexOf(text);
			if (index != -1) {
				return i;
			}
		}

		return -1;
	}


	static private List<string> readFileContentsToList(StreamReader reader) {
		List<string> lines = new List<string>();
		string line;
		while ((line = reader.ReadLine()) != null) {
			lines.Add(line);
		}

		return lines;
	}

	// Add a menu item named "Do Something" to MyMenu in the menu bar.
	[MenuItem("TestMenu/TestPostprocessing")]
	static void DoSomething() {
		OnPostprocessBuild(BuildTarget.WSAPlayer, @"C:\Unity Projects\Bozhon_HM18_Repo_Sebastian\App");
	}


	[PostProcessBuildAttribute(2)]
	public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject) {
		if (target == BuildTarget.WSAPlayer) {
			var filePathTargetFile = pathToBuiltProject + @"\" + Application.productName + @"\project.json";

			bool isRunningFirstTime = true;
			//Make a copy of the file when running the first time
			try {
				File.Copy(filePathTargetFile, filePathTargetFile + ".orig");
			}
			catch (IOException e) {
				isRunningFirstTime = false;
			}


			Debug.Log("Build done, checking VisualStudio dependencies");

			var filePathDependencies = Application.dataPath + @"\Shared\Other\VisualStudioDependencies.txt";

			List<string> linesToAppend;
			List<string> linesInTargetFile;

			try {
				//Read all the lines that should be written
				using (var fileStream = new FileStream(filePathDependencies, FileMode.Open)) {
					using (var streamReader = new StreamReader(fileStream)) {
						linesToAppend = readFileContentsToList(streamReader);

						for (int i = 0; i < linesToAppend.Count; i++) {
							linesToAppend[i] = "    " + linesToAppend[i];

							if (i < linesToAppend.Count - 1) {
								linesToAppend[i] = linesToAppend[i] + ",";
							}
						}
					}
				}
			}
			catch (FileNotFoundException e) {
				Debug.LogException(e);
				return;
			}

			try {
				using (var fileStream = new FileStream(filePathTargetFile, FileMode.Open, FileAccess.ReadWrite)) {
					using (var streamReader = new StreamReader(fileStream)) {
						linesInTargetFile = readFileContentsToList(streamReader);
					}
				}
			}
			catch (FileNotFoundException e) {
				Debug.LogException(e);
				return;
			}

			//Check if each line that should be added is already present
			bool needEditing = false;
			foreach (var line in linesToAppend) {
				if (findLineIndex(linesInTargetFile, line) == -1) {
					needEditing = true;
					break;
				}
			}

			//Was file already edited?
			if (!needEditing) {
				Debug.Log("No changes to VisualStudio dependencies required");

				//Don't return when the script is running for the first time, see the region "MissingOrigFix" below
				if (!isRunningFirstTime) return;
			}
			else {
				try {
					//Load the unedited copy of the file
					using (var fileStream = new FileStream(filePathTargetFile + ".orig", FileMode.Open, FileAccess.ReadWrite)) {
						using (var streamReader = new StreamReader(fileStream)) {
							linesInTargetFile = readFileContentsToList(streamReader);
						}
					}
				}
				catch (FileNotFoundException e) {
					Debug.LogException(e);
					return;
				}

			}

			var indexDependencies = findLineIndex(linesInTargetFile, "dependencies");
			if (indexDependencies == -1) {
				Debug.LogError("\"dependencies\" not found in json");
				return;
			}

			var indexEndBlock = findLineIndex(linesInTargetFile, "}", indexDependencies);
			if (indexEndBlock == -1) {
				Debug.LogError("\"}\" not found in json");
				return;
			}

			#region MissingOrigFix
			//Fix the missing .orig file for existing projects
			if (!needEditing && isRunningFirstTime) {
				Debug.Log("Missing .orig file, applying the fix");

				foreach (var line in linesToAppend) {
					var index = findLineIndex(linesInTargetFile, line);
					if (index != -1) {
						linesInTargetFile.RemoveAt(index);
					}
				}

				indexEndBlock = findLineIndex(linesInTargetFile, "}", indexDependencies);
				linesInTargetFile[indexEndBlock - 1] = linesInTargetFile[indexEndBlock - 1].Remove(linesInTargetFile[indexEndBlock - 1].Length - 1);

				File.WriteAllText(filePathTargetFile + ".orig", System.String.Empty);

				using (var fileStream = new FileStream(filePathTargetFile + ".orig", FileMode.Open, FileAccess.ReadWrite)) {
					using (var streamWriter = new StreamWriter(fileStream)) {
						foreach (string line in linesInTargetFile) {
							streamWriter.WriteLine(line);
						}
						streamWriter.Close();
					}
				}

				//End of fix
				return;
			}
			#endregion

			//Add a comma so that the syntax is correct for appending a new line
			linesInTargetFile[indexEndBlock - 1] += ",";

			//Append all the new Lines to lines of the target file
			linesInTargetFile.InsertRange(indexEndBlock, linesToAppend);

			Debug.Log("Writing Changes to File");
			using (var fileStream = new FileStream(filePathTargetFile, FileMode.Open, FileAccess.ReadWrite)) {
				using (var streamWriter = new StreamWriter(fileStream)) {
					foreach (string line in linesInTargetFile) {
						streamWriter.WriteLine(line);
					}
					streamWriter.Close();
				}
			}
		}
	}
}
