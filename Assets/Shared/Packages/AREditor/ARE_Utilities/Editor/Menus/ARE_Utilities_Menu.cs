﻿using UnityEditor;

public class ARE_Utilities_Menu
{
    [MenuItem("AREditor/Utilities/CreateScene")]
    public static void CreateRawScene()
    {
        ARE_Scene_Utilities.GenerateRawScene();
    }

    [MenuItem("AREditor/Utilities/SeeThroughVisionHelper")]
    public static void InitXrayVisionHelper()
    {
        XRayHelperWindow.Init();
    }
}
