﻿using Rotorz.ReorderableList;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class XRayHelperWindow : EditorWindow
{
    private static XRayHelperWindow Instance;

    private List<GameObject> OccluderGameObjects=new List<GameObject>();
    private List<GameObject> OccludedGameObjects=new List<GameObject>();

    private Color OccluderShaderColor=new Color(150.0f, 150.0f, 150.0f, 255.0f);
    private Color OccludedShaderColor= new Color(150.0f, 150.0f, 150.0f, 255.0f);
    private Color XrayColor=new Color(0.0f,130.0f,130.0f, 255.0f);

    private static string path = "Assets/Shared/Packages/AREditor/ARE_Utilities/XrayVision/Materials/";
    private static string Imagepath = "Textures/OcclusionDiagram";

    private static Texture2D OcclusionImage = null;

    Vector2 scrollPosLeft;
    Vector2 scrollPosRigh;

    bool applyChildLeft = false;
    bool applyChildRight = false;

    public static void Init()
    {
        Instance = GetWindow<XRayHelperWindow>();
        Instance.titleContent = new GUIContent("See-ThroughVision");
        Instance.maxSize = new Vector2(600, 560);
        Instance.minSize = Instance.maxSize;
        OcclusionImage = (Texture2D)Resources.Load(Imagepath, typeof(Texture2D));
    }

    private void OnEnable()
    {
        Init();
    }

    public void OnGUI()
    {
        //GUILayout.Space(20);

        EditorGUILayout.BeginVertical();
        {
            GUILayout.Space(10);
            EditorGUILayout.LabelField("See-through Vision Helper", EditorStyles.boldLabel);
            EditorGUILayout.LabelField("The Xray function needs an Occluded object (witch we wanted to see even if there is another object "
                , EditorStyles.whiteLabel);
            EditorGUILayout.LabelField("obstructing our vision ) and an Occluder (the object obstructing the vision)", EditorStyles.whiteLabel);
            //GUILayout.Label(OcclusionImage);

            GUI.DrawTexture(new Rect(10, 65, 580, 250), OcclusionImage, ScaleMode.StretchToFill , true, 10.0F);
            //describe the x ray, occluder and ocludding
            //also include a picture
            //GUILayout.Space(20);
            GUILayout.Space(260);
            EditorGUILayout.LabelField("Please select:",EditorStyles.boldLabel);
            //GUILayout.Space(10);
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.BeginVertical();
                {
                    EditorGUILayout.LabelField("Occluded:", EditorStyles.boldLabel);
                    OccludedShaderColor =EditorGUILayout.ColorField("Color: ",OccludedShaderColor);
                    XrayColor = EditorGUILayout.ColorField("X ray Color: ",XrayColor);
                    applyChildLeft = EditorGUILayout.Toggle("Apply on Children",applyChildLeft);
                    EditorGUILayout.LabelField("Objects:", EditorStyles.boldLabel);
                    scrollPosLeft =EditorGUILayout.BeginScrollView(scrollPosLeft, GUILayout.Height(100));
                    ReorderableListGUI.ListField(OccludedGameObjects, drawListItem );
                    EditorGUILayout.EndScrollView();
                }
                EditorGUILayout.EndVertical();
                GUILayout.Space(20);
                EditorGUILayout.BeginVertical();
                {
                    EditorGUILayout.LabelField("Occluder:", EditorStyles.boldLabel);
                    OccluderShaderColor = EditorGUILayout.ColorField("Color: ",OccluderShaderColor);
                    GUILayout.Space(20);
                    applyChildRight = EditorGUILayout.Toggle("Apply on Children", applyChildRight);
                    EditorGUILayout.LabelField("Objects:", EditorStyles.boldLabel);
                    scrollPosRigh= EditorGUILayout.BeginScrollView(scrollPosRigh, GUILayout.Height(100));
                    ReorderableListGUI.ListField(OccluderGameObjects, drawListItem);
                    EditorGUILayout.EndScrollView();

                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Apply Materials"))
            {
                //apply the materials
                assingOccludedMaterial();
                assingOccluderMaterial();

            }
        }
        EditorGUILayout.EndVertical();

        GUILayout.Space(20);
        Repaint();
    }

    private void assingOccluderMaterial()
    {
        var newMat = getAndCloneMaterial(path,"MaterialOccluderVisible");
        newMat.color = OccluderShaderColor;
        foreach (var obj in OccluderGameObjects)
        {
            if (obj == null) continue;
            if (!applyChildRight)
            {
                var myRender = obj.GetComponents<Renderer>();
                foreach(Renderer ren in myRender)
                {
                    applyMaterialToRenderer(ren, newMat);
                }
                
            }  
            else
            {
                var myRender = obj.GetComponentsInChildren<Renderer>();
                foreach (Renderer ren in myRender)
                {
                    applyMaterialToRenderer(ren, newMat);
                }
            }
        }
    }

    private void assingOccludedMaterial()
    {
        var newMat = getAndCloneMaterial(path, "MaterialOccludedVisible");
        newMat.color = OccludedShaderColor;
        newMat.SetColor("_RimColor", XrayColor);
        foreach (var obj in OccludedGameObjects)
        {
            if (obj == null) continue;
            if (!applyChildLeft)
            {
                var myRender = obj.GetComponents<Renderer>();
                foreach (Renderer ren in myRender)
                {
                    applyMaterialToRenderer(ren, newMat);
                }

            }
            else
            {
                var myRender = obj.GetComponentsInChildren<Renderer>();
                foreach (Renderer ren in myRender)
                {
                    applyMaterialToRenderer(ren, newMat);
                }
            }
        }
    }


    private GameObject drawListItem(Rect mypos,GameObject go)
    {
        return (GameObject)EditorGUI.ObjectField(mypos,"",go, typeof(GameObject));
    }

    private Material getAndCloneMaterial(string path, string name)
    {
        string[] search_results = System.IO.Directory.GetFiles(path, name+".mat", System.IO.SearchOption.AllDirectories);

        Material mat = null;
        var folderPath = "Assets/Materials";
        if (!AssetDatabase.IsValidFolder(folderPath)) AssetDatabase.CreateFolder("Assets", "Materials");
        for (int i = 0; i < search_results.Length; i++)
        {
            var pathToCopy = folderPath+"/" + name+".mat";
            int j=0;
            while(System.IO.File.Exists(pathToCopy))
            {
                pathToCopy = folderPath + "/" + name+j + ".mat";
                j++;
            }
            bool result =AssetDatabase.CopyAsset(search_results[i], pathToCopy);
            mat = (Material)AssetDatabase.LoadAssetAtPath(pathToCopy, typeof(Material));
            return mat;
                
        }
        return mat;
    }

    private void applyMaterialToRenderer(Renderer renderers,Material mat)
    {
        if (renderers == null) return;
        Material[] m = renderers.sharedMaterials;

        for (int i = 0; i < m.Length; i++)
        {
            m[i] = mat;
        }
        renderers.materials = m;
    }
}
