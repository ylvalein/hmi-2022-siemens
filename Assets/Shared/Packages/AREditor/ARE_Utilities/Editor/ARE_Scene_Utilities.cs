﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

public class ARE_Scene_Utilities  {

    private static string rawScenePath = "Assets/Shared/Scenes/ARE_RawScene.unity";

    public static void GenerateRawScene()
    {
        //test nhg
        //var rawScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(rawScenePath);
        var newScene = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene);
        var rawScene = SceneMerger.CopyAndOpenScene(rawScenePath);
        if (rawScene == null)
        {
            EditorUtility.DisplayDialog("Error", "Scene not founded", "ok");
            return;
        }
        EditorSceneManager.MergeScenes(rawScene, newScene);

        var folderPath = "Assets/Scenes";
        if (!AssetDatabase.IsValidFolder(folderPath)) AssetDatabase.CreateFolder("Assets", "Scenes");

        EditorSceneManager.SaveScene(newScene, "Assets/Scenes/AREGeneratedScene.unity");
    }

}
