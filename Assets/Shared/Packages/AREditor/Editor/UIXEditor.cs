﻿using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System;
using System.Reflection;
using UnityEngine.Rendering;

public class UIXEditor : Editor {


    public UIXEditor()
    {
        //var customAttributes = (CustomUIXEditor[])typeof(UIXEditor).GetCustomAttributes(typeof(CustomUIXEditor), true);
        //if (customAttributes.Length > 0)
        //{
        //    var myAttribute = customAttributes[0];
        //    Type myInspectedType = myAttribute.m_InspectedUIXType;

        //    // TODO: Do something with the value
        //}
    }

    public bool haveEvents = false;
    public virtual void Init()
    {

    }

    private void OnEnable()
    {
        Init();
    }
    public virtual void OnUIXInspector()
    {

    }

    public virtual void OnUIXInspectorEvents()
    {
        
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
    }
}

[AttributeUsage(AttributeTargets.Class)]
public class CustomUIXEditor:Attribute
{
    private static readonly Dictionary<Type, List<Type>> TypeOfCustomUIXEditors = new Dictionary<Type, List<Type>>();

    public CustomUIXEditor(System.Type inspectedType) 
    {
        m_InspectedUIXType = inspectedType;
    }

    public Type m_InspectedUIXType;

    public static Type FindCustomUIXEditorByType(Type inspected)
    {
        if (inspected == null)
            return null;

        List<Type> foundEditorsTypes;
        if (TypeOfCustomUIXEditors.TryGetValue(inspected, out foundEditorsTypes))
        {
            return foundEditorsTypes.First();
        }
        else
        {
            return null;
        }
    }

    public static void Rebuild()
    {
        var assembly = Assembly.GetExecutingAssembly();
        Type[] types = assembly.GetTypes();

        foreach (var type in types)
        {
            object[] attrs = type.GetCustomAttributes(typeof(CustomUIXEditor), false);
            if (!attrs.Any()) continue;

            CustomUIXEditor mainAttr = (CustomUIXEditor)attrs[0];

            if (mainAttr.m_InspectedUIXType == null)
                return;
            else
            {
                List<Type> editorsType;
                if (!TypeOfCustomUIXEditors.TryGetValue(mainAttr.m_InspectedUIXType, out editorsType))
                {
                    editorsType = new List<Type>();
                    editorsType.Add(type);
                    /*Use it on contructor:::
                    ConstructorInfo ctor = type.GetConstructor(Type.EmptyTypes);
                    var editortemp = (UIXEditor)ctor.Invoke( new object[] { });
                    editors.Add(editortemp);
                    */
                    TypeOfCustomUIXEditors[mainAttr.m_InspectedUIXType] = editorsType;
                }
            }
        }
    }
}
