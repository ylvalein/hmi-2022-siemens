﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.UI;
using System.Linq;
using Shared;

[CustomUIXEditor(typeof(ButtonMemory))]
public class ButtomMemoryEditor : UIXEditor
{
    IconGraphicalChangeObject myIcon;
    public FlexibleUIData myData;

    SerializedProperty IsEnable;

    SerializedProperty onPointerEntered;
    SerializedProperty onPointerExit;
    SerializedProperty onClick;

    SerializedProperty myGraphicalObjects;
    SerializedProperty GameObjectsAttached;

    int _choiceIndex = 0;

    public override void Init()
    {
        base.Init();
        haveEvents = true;

        ButtonMemory b = (ButtonMemory)target;

        IsEnable = serializedObject.FindProperty("isSelected");

        onPointerEntered = serializedObject.FindProperty("onPointerEnter");
        onPointerExit = serializedObject.FindProperty("onPointerExit");
        onClick = serializedObject.FindProperty("OnClick");

        myGraphicalObjects = serializedObject.FindProperty("myGraphicalObjects");
        GameObjectsAttached = serializedObject.FindProperty("GameObjectsAttached");

        //SerializedProperty M_myData = myData
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        //ButtonMemory b = (ButtonMemory)target;

        //SerializedProperty IsEnable = serializedObject.FindProperty("isSelected");

        //SerializedProperty onPointerEntered = serializedObject.FindProperty("onPointerEnter");
        //SerializedProperty onPointerExit = serializedObject.FindProperty("onPointerExit");

        //SerializedProperty myGraphicalObjects = serializedObject.FindProperty("myGraphicalObjects");
        //SerializedProperty GameObjectsAttached = serializedObject.FindProperty("GameObjectsAttached");

        ////SerializedProperty M_myData = myData


        //EditorGUIUtility.LookLikeControls();

        //EditorGUILayout.PropertyField(onPointerEntered);
        //EditorGUILayout.PropertyField(onPointerExit);

        //EditorGUILayout.PropertyField(myGraphicalObjects,true);
        //EditorGUILayout.PropertyField(GameObjectsAttached,true);

        //EditorGUILayout.PropertyField(IsEnable);

        //serializedObject.ApplyModifiedProperties();
        //EditorGUILayout.PropertyField(this.myData,true);

        //if(b.myData!=null)
        //_choiceIndex = EditorGUILayout.Popup(_choiceIndex, b.myData.myGraphicalIcons.Select(x => x.IconName).ToArray());

        //DrawDefaultInspector();
    }

    public override void OnUIXInspector()
    {
        base.OnUIXInspector();
        //ButtonMemory b = (ButtonMemory)target;
        //SerializedProperty IsEnable = serializedObject.FindProperty("isSelected");
        //SerializedProperty GameObjectsAttached = serializedObject.FindProperty("GameObjectsAttached");

        //EditorGUILayout.PropertyField(IsEnable, new GUIContent("is Selected at Beginnig"),true, GUILayout.MaxWidth(20));
        EditorGUILayout.PropertyField(IsEnable);
        EditorGUILayout.PropertyField(GameObjectsAttached, true);
        serializedObject.ApplyModifiedProperties();

    }

    public override void OnUIXInspectorEvents()
    {
        base.OnUIXInspectorEvents();
        //ButtonMemory b = (ButtonMemory)target;
        //SerializedProperty onPointerEntered = serializedObject.FindProperty("onPointerEnter");
        //SerializedProperty onPointerExit = serializedObject.FindProperty("onPointerExit");

        EditorGUILayout.PropertyField(onPointerEntered);
        EditorGUILayout.PropertyField(onPointerExit);
        EditorGUILayout.PropertyField(onClick);
        serializedObject.ApplyModifiedProperties();
    }
}
