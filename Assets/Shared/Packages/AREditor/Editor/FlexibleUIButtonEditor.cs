﻿using UnityEngine;
using UnityEditor;
using System.Linq;
using Shared;

[CustomEditor(typeof(FlexibleUIButton))]
public class FlexibleUIButtonEditor : UIXEditor
{


    //public override void OnInspectorGUI()
    //{
    //    DrawDefaultInspector();

    //    EditorGUI.BeginChangeCheck();
    //    FlexibleUIButton fb = (FlexibleUIButton)target;

    //    fb._choiceIndex = EditorGUILayout.Popup(fb._choiceIndex, fb.myData.myGraphicalIcons.Select(x => x.IconName).ToArray());

    //    if(EditorGUI.EndChangeCheck())
    //    {
    //        EditorUtility.SetDirty(target);
    //        fb.OnSelectionChanged();
    //    }

    //}

    public override void OnUIXInspector()
    {
        base.OnUIXInspector();
        //DrawDefaultInspector();

        SerializedProperty mydata = serializedObject.FindProperty("myData");

        EditorGUI.BeginChangeCheck();
        FlexibleUIButton fb = (FlexibleUIButton)target;

        EditorGUILayout.PropertyField(mydata, true);

        GUILayout.BeginHorizontal();
        {
            EditorGUILayout.LabelField("Type Icon:");
            fb._choiceIndex = EditorGUILayout.Popup(fb._choiceIndex, fb.myData.myGraphicalIcons.Select(x => x.IconName).ToArray());
        }
        GUILayout.EndHorizontal();


        if (EditorGUI.EndChangeCheck())
        {
            EditorUtility.SetDirty(target);
            fb.OnSelectionChanged();
        }
    }
}
