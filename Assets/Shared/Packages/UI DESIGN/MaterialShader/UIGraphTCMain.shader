// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

Shader "UIGraph/Main"
{
	Properties
	{
		_Arrays("Data", 2D) = "white"{}
		_Color5("High High", Color) = (1,1,1,1)
		_Color4("High", Color) = (1,1,1,1)
		_Color3("Normal", Color) = (1,1,1,1)
		_Color2("Low", Color) = (1,1,1,1)
		_Color1("Low Low", Color) = (1,1,1,1)
			_Alpha("Alpha", Range(0, 1)) = 1
			_CutHeight8("Cut Height Alpha", Range(0, 1)) = 0.5
			_CutHeight7("Cut Height High High-", Range(0, 1)) = 0.5
			_CutHeight6("Cut Height High High", Range(0, 1)) = 0.5
			_CutHeight5("Cut Height High-", Range(0, 1)) = 0.5
			_CutHeight4("Cut Height High", Range(0, 1)) = 0.5
			_CutHeight3("Cut Height Low", Range(0, 1)) = 0.5
			_CutHeight2("Cut Height Low-", Range(0, 1)) = 0.5
			_CutHeight1("Cut Height Low Low", Range(0, 1)) = 0.5
			_CutHeight0("Cut Height Low Low-", Range(0, 1)) = 0.5
			/*
			_CutHeight3("Cut Height High High", Range(0, 1)) = 0.5
			_CutHeight2("Cut Height High", Range(0, 1)) = 0.5
			_CutHeight1("Cut Height Low", Range(0, 1)) = 0.5
			_CutHeight0("Cut Height Low Low", Range(0, 1)) = 0.5
			*/


			[HideInInspector] _ColorMask("Color Mask", Float) = 15

	}

		SubShader
		{
			Tags
			{
				"Queue" = "Transparent"
				"IgnoreProjector" = "True"
				"RenderType" = "Transparent"
				"PreviewType" = "Plane"
				"CanUseSpriteAtlas" = "True"
			}

			Stencil
			{
				Ref[_Stencil]
				Comp[_StencilComp]
				Pass[_StencilOp]
				ReadMask[_StencilReadMask]
				WriteMask[_StencilWriteMask]
			}

			Cull Off
			Lighting Off
			ZWrite Off
			ZTest[unity_GUIZTestMode]
			Blend SrcAlpha OneMinusSrcAlpha
			ColorMask[_ColorMask]

			Pass
			{
				Name "Default"
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma target 2.0

				#include "UnityCG.cginc"
				#include "UnityUI.cginc"

				#pragma multi_compile __ UNITY_UI_ALPHACLIP

				struct appdata_t
				{
					float4 vertex   : POSITION;
					float4 color    : COLOR;
					float2 texcoord : TEXCOORD0;
					UNITY_VERTEX_INPUT_INSTANCE_ID
				};

				struct v2f
				{
					float4 vertex   : SV_POSITION;
					fixed4 color : COLOR;
					float2 texcoord  : TEXCOORD0;
					float4 worldPosition : TEXCOORD1;
					UNITY_VERTEX_OUTPUT_STEREO
				};

				fixed4 _Color;
				fixed4 _TextureSampleAdd;
				float4 _ClipRect;

				v2f vert(appdata_t v)
				{
					v2f OUT;
					UNITY_SETUP_INSTANCE_ID(v);
					UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
					OUT.worldPosition = v.vertex;
					OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

					OUT.texcoord = v.texcoord;

					OUT.color = v.color * _Color;
					return OUT;
				}

				sampler2D _Arrays;
				float4 _Arrays_TexelSize;
				float arx;
				float ary;
				half4 _Color1;
				half4 _Color2;
				half4 _Color3;
				half4 _Color4;
				half4 _Color5;
				float _Alpha;
				float _CutHeight8;
				float _CutHeight7;
				float _CutHeight6;
				float _CutHeight5;
				float _CutHeight4;
				float _CutHeight3;
				float _CutHeight2;
				float _CutHeight1;
				float _CutHeight0;

				fixed4 frag(v2f IN) : SV_Target
				{
					arx = _Arrays_TexelSize.x;
					ary = _Arrays_TexelSize.y;
					/*
					half4 color;
					
					if (IN.texcoord.y > _CutHeight3) {
						color = _Color3;
					}
					else if (IN.texcoord.y > _CutHeight2) {
						color = lerp(_Color2, _Color3, ((IN.texcoord.y - _CutHeight2) / (_CutHeight3 - _CutHeight2)).xxxx);
					}
					else if (IN.texcoord.y > _CutHeight1) {
						color = _Color2;
					}
					else if (IN.texcoord.y > _CutHeight0) {
						color = lerp(_Color1, _Color2, ((IN.texcoord.y - _CutHeight0) / (_CutHeight1 - _CutHeight0)).xxxx);
					}
					else {
						color = _Color1;
					}
					
					*/
					
					half4 color;

					if (IN.texcoord.y > _CutHeight3) {
						color = _Color5;
					}
					else if (IN.texcoord.y > _CutHeight2) {
						color = _Color4;
					}
					else if (IN.texcoord.y > _CutHeight1) {
						color = _Color3;
					}
					else if (IN.texcoord.y > _CutHeight0) {
						color = _Color2;
					}
					else {
						color = _Color1;
					}
					
					/*
					half4 color;

					if (IN.texcoord.y > _CutHeight7) {
						color = _Color5;
					}
					else if (IN.texcoord.y > _CutHeight6) {
						color = lerp(_Color4, _Color5, ((IN.texcoord.y - _CutHeight6) / (_CutHeight7 - _CutHeight6)).xxxx);
					}
					else if (IN.texcoord.y > _CutHeight5) {
						color = _Color4;
					}
					else if (IN.texcoord.y > _CutHeight4) {
						color = lerp(_Color3, _Color4, ((IN.texcoord.y - _CutHeight4) / (_CutHeight5 - _CutHeight4)).xxxx);
					}
					else if (IN.texcoord.y > _CutHeight3) {
						color = _Color3;
					}
					else if (IN.texcoord.y > _CutHeight2) {
						color = lerp(_Color2, _Color3, ((IN.texcoord.y - _CutHeight2) / (_CutHeight3 - _CutHeight2)).xxxx);
					}
					else if (IN.texcoord.y > _CutHeight1) {
						color = _Color2;
					}
					else if (IN.texcoord.y > _CutHeight0) {
						color = lerp(_Color1, _Color2, ((IN.texcoord.y - _CutHeight0) / (_CutHeight1 - _CutHeight0)).xxxx);
					}
					else {
						color = _Color1;
					}
					*/

					if (IN.texcoord.y < _CutHeight8) {

						color.a =color.a* lerp(1,  0.3, ((_CutHeight8-IN.texcoord.y) / (_CutHeight8)));


					}
					
					half4 grph = tex2D(_Arrays, IN.texcoord);
					if (grph.y < IN.texcoord.y) {
						grph.a = 0;
					}
					else {
						grph.a = 1;
					}


					
					color.a = min(_Alpha, grph.a * color.a);

					//color = tex2D(_Arrays, IN.texcoord);
					//color.a = 1;

					return color;
				}
			ENDCG
			}
		}
}
