// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

Shader "UIGraph/Current"
{
	Properties
	{
		//_Main("Main", Color) = (1,1,1,1)
		//_Shade("Shade", Color) = (1,1,1,1)
		_Value("Value", Range(0, 1)) = 0.5
		//_Length("Shade Length", Range(0, 1)) = 0.5

		_Color5("High High", Color) = (1,1,1,1)
		_Color4("High", Color) = (1,1,1,1)
		_Color3("Normal", Color) = (1,1,1,1)
		_Color2("Low", Color) = (1,1,1,1)
		_Color1("Low Low", Color) = (1,1,1,1)
		_CutHeight3("Cut Height High High", Range(0, 1)) = 0.5
		_CutHeight2("Cut Height High", Range(0, 1)) = 0.5
		_CutHeight1("Cut Height Low", Range(0, 1)) = 0.5
		_CutHeight0("Cut Height Low Low", Range(0, 1)) = .5
		_Bar("BarSize", Range(0, 1)) = 0.01


		[HideInInspector] _ColorMask("Color Mask", Float) = 15

	}

		SubShader
		{
			Tags
			{
				"Queue" = "Transparent"
				"IgnoreProjector" = "True"
				"RenderType" = "Transparent"
				"PreviewType" = "Plane"
				"CanUseSpriteAtlas" = "True"
			}

			Stencil
			{
				Ref[_Stencil]
				Comp[_StencilComp]
				Pass[_StencilOp]
				ReadMask[_StencilReadMask]
				WriteMask[_StencilWriteMask]
			}

			Cull Off
			Lighting Off
			ZWrite Off
			ZTest[unity_GUIZTestMode]
			Blend SrcAlpha OneMinusSrcAlpha
			ColorMask[_ColorMask]

			Pass
			{
				Name "Default"
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma target 2.0

				#include "UnityCG.cginc"
				#include "UnityUI.cginc"

				#pragma multi_compile __ UNITY_UI_ALPHACLIP

				struct appdata_t
				{
					float4 vertex   : POSITION;
					float4 color    : COLOR;
					float2 texcoord : TEXCOORD0;
					UNITY_VERTEX_INPUT_INSTANCE_ID
				};

				struct v2f
				{
					float4 vertex   : SV_POSITION;
					fixed4 color : COLOR;
					float2 texcoord  : TEXCOORD0;
					float4 worldPosition : TEXCOORD1;
					UNITY_VERTEX_OUTPUT_STEREO
				};

				fixed4 _Color;
				fixed4 _TextureSampleAdd;
				float4 _ClipRect;

				v2f vert(appdata_t v)
				{
					v2f OUT;
					UNITY_SETUP_INSTANCE_ID(v);
					UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
					OUT.worldPosition = v.vertex;
					OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

					OUT.texcoord = v.texcoord;

					OUT.color = v.color * _Color;
					return OUT;
				}

				//half4 _Main;
				//half4 _Shade;
				float _Value;
				//float _Length;
				half4 _Color1;
				half4 _Color2;
				half4 _Color3;
				half4 _Color4;
				half4 _Color5;
				float _CutHeight3;
				float _CutHeight2;
				float _CutHeight1;
				float _CutHeight0;
				float _Bar;
				fixed4 frag(v2f IN) : SV_Target
				{
					half4 color;

				if (IN.texcoord.y > _CutHeight3) {
					color = _Color5;
				}
				else if (IN.texcoord.y > _CutHeight2) {
					color = _Color4;
				}
				else if (IN.texcoord.y > _CutHeight1) {
					color = _Color3;
				}
				else if (IN.texcoord.y > _CutHeight0) {
					color = _Color2;
				}
				else {
					color = _Color1;
				}

					if (IN.texcoord.y > _Value || IN.texcoord.y < _Value-_Bar) {
						color.a = 0;
					}
					/*
					else if (IN.texcoord.y > _Value - _Length) {
						_Main = lerp(_Main, _Shade, (IN.texcoord.y - _Value + _Length)/_Length);
					
					
					}
					*/
					return color;
				}
			ENDCG
			}
		}
}
