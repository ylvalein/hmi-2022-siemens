﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
//using UnityEngine.Experimental.UIElements;
using UnityEngine.UI;
using Button = UnityEngine.UI.Button;

//[ExecuteInEditMode]
namespace Shared
{
    public class CustomDropdown : MonoBehaviour, IPointerClickHandler
    {
        [Header("Main Parameters")]
        public CustomDropdownItem Item;
        public RectTransform Divider;
        public Text SelectedTextObj;
        public RectTransform TemplateContainer;
        public RectTransform ItemsContainter;
        public Button Blocker;
        public Text WidgetText;

        [Header("Data block")]
        public string[] Options;
        public string CaptionText;
        public int MaxDisplayItems;

        //TODO: Check; was commented for mrtk2
        //[SerializeField]
        //[Tooltip("Put the Tren-/Gauge-/ or BarManager here")]
        //private WidgetManager manager;

        //[SerializeField]
        //private HandResize handResize;

        private void OnValidate()
        {
            SelectedTextObj.text = CaptionText;
        }

        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set
            {
                _selectedIndex = value;
                UpdateView();
                //Debug.Log(Options[_selectedIndex] + "   selected index drowdown");
                SetOpcValue(Options[_selectedIndex]); // 09.04.18 test
            }
        }

        public string SelectedText
        {
            get { return _selectedIndex >= 0 && _selectedIndex < Options.Length ? Options[_selectedIndex] : null; }
        }

        private int _selectedIndex;
        private List<CustomDropdownItem> _items;
        private List<RectTransform> _divider;
        private bool _isInited = false;

        private void UpdateView()
        {
            SelectedTextObj.text = SelectedText;
            //WidgetText.text = SelectedText;
        }

        public void Start()
        {
            _items = new List<CustomDropdownItem>();
            _divider = new List<RectTransform>();

            //create items
            var itemHeight = Item.RTransform.rect.height;
            var dividerHeight = Divider.rect.height;

            var displayItemCount = Options.Length > MaxDisplayItems ? MaxDisplayItems : Options.Length;

            var paddingBorder = TemplateContainer.anchoredPosition.y;

            var containerItemsHeight = displayItemCount * itemHeight +
                                       Math.Max(0, displayItemCount - 1) * dividerHeight;
            TemplateContainer.sizeDelta = new Vector2(TemplateContainer.sizeDelta.x, paddingBorder * 2 + containerItemsHeight);

            var innerContainerHeight = Options.Length * itemHeight +
                                       Math.Max(0, Options.Length - 1) * dividerHeight - containerItemsHeight;
            ItemsContainter.sizeDelta = new Vector2(ItemsContainter.sizeDelta.x, innerContainerHeight);

            Debug.Log(itemHeight);
            Debug.Log(dividerHeight);

            for (var index = 0; index < Options.Length; index++)
            {
                var option = Options[index];
                var item = Instantiate(Item, ItemsContainter);
                item.gameObject.SetActive(true);
                item.ItemText.text = option;
                item.ItemIndex = index;
                item.RTransform.anchoredPosition = new Vector2(0, -index * itemHeight - Math.Max(0, index) * dividerHeight);

                item.ClickHandler += OnItemClicked;

                if (index == Options.Length - 1) continue;

                var divider = Instantiate(Divider, ItemsContainter);
                divider.gameObject.SetActive(true);
                divider.anchoredPosition = new Vector2(0, -(index + 1) * itemHeight - Math.Max(0, index) * dividerHeight);
            }
        }

        private void OnItemClicked(int id)
        {
            SelectedIndex = id;
            Hide();
        }

        public void Hide()
        {

            TemplateContainer.gameObject.SetActive(false);
            Blocker.gameObject.SetActive(false);

            Debug.Log("Hide");
            //handResize.resizingEnabled = true;
        }

        public void Show()
        {
            Debug.Log("Show");
            //handResize.resizingEnabled = false;

            TemplateContainer.gameObject.SetActive(true);
            TemplateContainer.GetComponent<ScrollRect>().normalizedPosition = new Vector2(0, 1);

            Blocker.gameObject.SetActive(true);
            if (!_isInited)
            {
                _isInited = true;

                Blocker.transform.SetParent(GetComponentInParent<Canvas>().transform);
                Blocker.GetComponent<Canvas>().sortingLayerID = TemplateContainer.GetComponent<Canvas>().sortingLayerID;

                (Blocker.transform as RectTransform).anchorMin = Vector3.zero;
                (Blocker.transform as RectTransform).anchorMax = Vector3.one;
                (Blocker.transform as RectTransform).sizeDelta = Vector2.zero;
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            Show();
        }

        //TODO: Check; was commented for mrtk2
        private void SetOpcValue(string name)
        {
            //manager.setOpcXmlName(name);
        }
    }
}