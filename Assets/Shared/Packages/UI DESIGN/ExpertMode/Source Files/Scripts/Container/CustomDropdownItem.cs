﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CustomDropdownItem : MonoBehaviour, IPointerClickHandler {

	[Header("Main Parameters")]
	public RectTransform RTransform;
	public Text ItemText;
	
	public int ItemIndex;
	public event Action<int> ClickHandler; 
	public void OnPointerClick(PointerEventData eventData)
	{
		if (ClickHandler != null) {
			ClickHandler(ItemIndex);
		}
	}

}
