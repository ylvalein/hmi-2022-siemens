﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Container : MonoBehaviour
{
    [Header("Main Parameters")] 
    public Text TitleTextObj;
    public RectTransform RTransform;

    [Header("Container Data")] 
    public string TitleText;
    public Vector2 Size;

    private void OnValidate()
    {
        TitleTextObj.text = TitleText;
        RTransform.sizeDelta = Size;
    }
}
