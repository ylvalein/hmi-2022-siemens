﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[Serializable]
public class CustomButtonInfo {
	public Color BorderColor;
	public Color BackgroundColor;
	public Color IconColor;
}

[ExecuteInEditMode]
public class CustomButton : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
{
	[Header("Main Parameters")] 
	public RawImage Border;
	public RawImage Background;
	public RawImage Icon;
    public AudioSource audioSrc;
	
	[Header("Colorise button")]
	public CustomButtonInfo NormalState;
	public CustomButtonInfo ActiveState;
	[Tooltip("Externaly Icon would not change with theme change")]
	public bool IconCustomColor;

	public bool _isPressed = false;
	private bool _isEntered = false;

	private void Start() {
        if(!_isPressed)
		ChangeState(0);
	}

	// Used Only in Editor mode
	private void OnValidate() {
        if (!_isPressed)
            ChangeState(0);

    }
	
	

	public void ChangeState(int state)
	{
		switch (state)
		{
			case 0:
				ChangeButtonStateTo(NormalState);
				break;
			case 1:
                ChangeButtonStateTo(ActiveState);
                break;
			case 2:
                ChangeButtonStateTo(ActiveState);
                break;
			default:
				break;
		}
		//Debug.Log(state);
	}

	private void ChangeButtonStateTo(CustomButtonInfo info)
	{
		Border.color = info.BorderColor;
		Background.color = info.BackgroundColor;
		Icon.color = info.IconColor;
	}

	public void OnPointerClick(PointerEventData eventData) {
		Debug.Log("button click");
        audioSrc.Play();
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		_isEntered = true;
		ChangeState(1);
	}

	public void OnPointerExit(PointerEventData eventData) {
		_isEntered = false;
		if (!_isPressed) {
			ChangeState(0);
		}
		
	}

	public void OnPointerDown(PointerEventData eventData) {
		//_isPressed = true;
	}

	public void OnPointerUp(PointerEventData eventData) {
		//_isPressed = false;
		//if (!_isEntered) {
		//	ChangeState(0);
		//}
	}
}
