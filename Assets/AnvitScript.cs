﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;


public class AnvitScript : MonoBehaviour
{

    public GameObject Model;
    public GameObject ButtonShow;
    public bool auto;
    public float xOffset;
    public float yOffset;
    public float zOffset;
    public float xRotate;
    public float yRotate;
    public float zRotate;

    public void PositionTransform()
    {
        GameObject targ = GameObject.Find("WittmannTarget");
        Debug.Log("Target position before" + targ.transform.position);
        Debug.Log("Model position before" + gameObject.transform.position);
        Vector3 posModel;
        //Quaternion rot;
        Debug.Log("QR Code scanned");

        if (targ.transform.position != new Vector3(0, 0, 0))
        {
            if (!auto)
            {
                posModel = new Vector3(targ.transform.position.x+xOffset, targ.transform.position.y+yOffset, targ.transform.position.z+zOffset);
                //rot = new Quaternion(xRotate, yRotate, zRotate, 1);
            }
            else
            {
                posModel = new Vector3(transform.position.x - targ.transform.position.x,
                                      transform.position.y - targ.transform.position.y,
                                      transform.position.z - targ.transform.position.z);
            }                

            gameObject.transform.position = posModel;
            //gameObject.transform.eulerAngles = new Vector3(xRotate, yRotate, zRotate);

            Debug.Log("Target position: " + targ.transform.position);
            Debug.Log("Model position: " + gameObject.transform.position);

        }
        Model.SetActive(true);
        ButtonShow.SetActive(true);

    }

}
