﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;


public class VuforiaOff : MonoBehaviour
{
    public int waittime;
    public GameObject ScannerUI;
    bool DeployOniPad;
    public bool deactivated;
    

    public void stopvuforia()
    {
        Debug.LogWarning("Stopping Vuforia...");
        StartCoroutine(stopVuforiaCoroutine());
    }

    IEnumerator stopVuforiaCoroutine()
    {
        ScannerUI.SetActive(false);
        yield return new WaitForSeconds(waittime);
        //if (DeployOniPad == false)
        //VuforiaBehaviour.Instance.enabled = false;
        TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();
        deactivated = true;

        StopCoroutine("stopVuforiaCoroutine");
    }

}
