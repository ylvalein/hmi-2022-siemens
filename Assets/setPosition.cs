﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine;

public class setPosition : MonoBehaviour
{
    public Transform newPos;
    public VuforiaOff vufOff;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void setPos()
    {
        
        Debug.Log("Deactivated:" + vufOff.deactivated);
        if (!vufOff.deactivated)
        {
            Debug.Log(newPos);
            gameObject.SetActive(true);
            transform.position = newPos.position;
            //transform.rotation = Quaternion.Euler(90f, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
        }
        else Debug.Log("No Vuforia is deactivated");

    }


    public void SetObjectPosition(GameObject newPos)
    {
        gameObject.transform.position = newPos.transform.position;
    }

}
