﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    public GameObject AMR, laser, origin;
    public Position_updater posUp;
    public float speed;
    public bool moving;
    public bool trigger;
    public LineRenderer render;
    public float speedA, speedX, speedY;
    public GameObject ArrowPrefab;

    [SerializeField]
    private Curve curve;
    [SerializeField]
    private List<Vector3> curveList;
    [SerializeField]
    private float passedTime;
    [SerializeField]
    private Vector3 targetPoint;

    // Start is called before the first frame update
    void Start()
    {
        moving = false;
        speed = 0.1f;
        render.useWorldSpace = false;
        speedA = 0;
        speedX = 0;
        speedY = 0;
    }

    // Update is called once per frame
    void Update()
    {
        ////TODO
        laser.transform.rotation = AMR.transform.rotation;
        laser.transform.position = AMR.transform.position;

        // calculate speed
        speed = Vector3.Magnitude(new Vector3(speedX, 0, speedY));

        //Draw Line
        if (moving && curveList.Count>2)
        {
            //remove past points from list
            ////Find closest Point on list
            float closestDistance = -1;
            int count = 0;
            for (int i = 0 ; i < curveList.Count; i++)
            {
                float distance = Vector3.Distance(AMR.transform.localPosition, new Vector3(curveList[i].x, AMR.transform.localPosition.y, curveList[i].z));
                if (closestDistance == -1)
                {
                    closestDistance = distance;
                }
                else if (distance < closestDistance)
                {
                    closestDistance = distance;
                    count += 1;
                }
                else if (distance > closestDistance) break;
            }
            if (count > 0)curveList.RemoveRange(0, count); 

            DrawLine();

        }

    }

    void MoveForward()
    {
        // Move our position a step closer to the target.
        float step = speed * Time.deltaTime; // calculate distance to move
        Vector3 newTarget = new Vector3(targetPoint.x, laser.transform.localPosition.y, targetPoint.z);
        //Debug.LogWarning(newTarget.ToString());
        //newTarget = newTarget - new Vector3(origin.transform.position.x, 0, origin.transform.position.z);
        laser.transform.localPosition = Vector3.MoveTowards(laser.transform.localPosition, newTarget, step);
    }

    void DrawLine()
    {
        int i = 0;
        foreach (Vector3 pos in curveList)
        {
            if (Vector3.Distance(AMR.transform.localPosition, new Vector3(pos.x, AMR.transform.localPosition.y, pos.z)) < 0.2f){
                i += 1;
            }
            else { break; }
        }

        render.positionCount = curveList.Count - i;
        //Debug.LogWarning(i +  "   " + render.positionCount);
        int count = 0;
        for (int k = i; k < curveList.Count; ++k)
        {
            render.SetPosition(count, curveList[k]);
            count += 1;
        }
    }

    public void StartMoving()
    {
        //Debug.LogError("we start");
        // get the curve path
        curve = posUp.FindCurrentCurve();
        targetPoint = curve.list[0];
        laser.transform.position = AMR.transform.position;
        foreach(Vector3 pos in curve.list)
        {
            curveList.Add(pos);
        }
        moving = true;
        //arrow.GetComponent<ArrowAnimation>().ToggleMoving();
    }
    
    public void SetVelocity(int i, float value)
    {
        switch (i)
        {
            case 0:
                speedX = value/1000;   // mm/s -> m/s
                break;
            case 1:
                speedY = value/1000;
                break;
            case 2:
                speedA = value/1000;
                break;

        }
    }

    public void InitiateMoving(Curve crve)
    {
        //write all position point on cure into the curve list
        curve = crve;
        foreach (Vector3 pos in curve.list)
        {
            curveList.Add(pos);
        }
        targetPoint = curve.list[0];
        laser.transform.position = AMR.transform.position;
        laser.transform.rotation = AMR.transform.rotation;
        moving = true;
        StartCoroutine("StartArrowAnimation");     
    }

    public void StopMoving()
    {
        curveList.Clear();
        moving = false;
        render.positionCount = 0;
        StopCoroutine("StartArrowAnimation");
    }

    IEnumerator StartArrowAnimation()
    {
        while (moving)
        {
            GameObject obj = Instantiate(ArrowPrefab, origin.transform, false);
            obj.name = "arrow";
            obj.transform.localPosition = new Vector3(AMR.transform.localPosition.x, -0.01f, AMR.transform.localPosition.z);
            obj.GetComponent<ArrowAnimation>().InitiateArrow(curveList);
            yield return new WaitForSeconds(1f);
        }
    }
}
