using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowAnimation : MonoBehaviour
{

    private List<Vector3> curveList = new List<Vector3>();
    public float speed;
    public bool initiated = false;
    public GameObject arrowTarget, origin;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!initiated) return;

        if (Vector3.Distance(transform.localPosition, new Vector3(curveList[0].x, transform.localPosition.y, curveList[0].z)) < 0.01f)
        {
            //arrow reached a waypoint. Remove the first element of list
            curveList.RemoveAt(0);
        }

        if (curveList.Count <= 1)
        {
            Object.Destroy(gameObject);
            Object.Destroy(arrowTarget);
        }
        else
        {
            MoveForward();
        }
    }



    void MoveForward()
    {
        // Move our position a step closer to the target.
        float step = speed * Time.deltaTime; // calculate distance to move
        Vector3 newTarget = new Vector3(curveList[0].x, transform.localPosition.y, curveList[0].z);
        arrowTarget.transform.localPosition = newTarget;
        transform.LookAt(arrowTarget.transform);
        transform.localPosition = Vector3.MoveTowards(transform.localPosition, newTarget, step);
    }



    public void InitiateArrow(List<Vector3> posList)
    {
        foreach (Vector3 pos in posList)
        {
            curveList.Add(pos);
        }
        origin = GameObject.Find("origin");
        arrowTarget = Instantiate(arrowTarget, origin.transform, false);
        initiated = true;
    }

}
