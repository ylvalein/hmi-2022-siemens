using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFromCSV : MonoBehaviour
{ 
    public List<Path> PathList= new List<Path>();
}

[System.Serializable]
public class Path
{
    public Path()
    {

    }
    public Path(Path path)
    {
        this.pathID = path.pathID;
        this.segments = new List<Segment>(path.segments);
    }
    public int pathID;
    public List<Segment> segments = new List<Segment>();
}


[System.Serializable]
public class Segment
{
    public List<ControlPoint> controlPoints = new List<ControlPoint>();
    public WayPoint startPoint;
    public WayPoint endPoint;
    public Segment()
    {
    }
    public Segment(Segment segment)
    {
        this.controlPoints=new List<ControlPoint>(segment.controlPoints);
        this.startPoint=segment.startPoint;
        this.endPoint=segment.endPoint;
    }
}

// Start and End Points of each Segment
[System.Serializable]
public class WayPoint
{
    public int wayPointId;
    public Vector3 position;

    public WayPoint(int id, float posX, float posY)
    {
        wayPointId=id;
        this.position = new Vector3(posX, 0, posY);
    }
} //end class

// ControlPoints
[System.Serializable]
public class ControlPoint
{
    public Vector3 position;
    public ControlPoint(float posX, float posY)
    {
        this.position = new Vector3(posX, 0, posY);
    }
} //end class Vector3_Serializable