using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.Configuration.Attributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using UnityEngine;


public class DeserializeCSV : MonoBehaviour
{
    public PathFromCSV pathFromCSV;
    public GameObject Managers;

    // Start is called before the first frame update
    void Start()
    {
        //pathFromCSV = new PathFromCSV();
        //pathFromCSV.PathList = new List<Path>();

        //filePath = System.IO.Path.Combine(Application.persistentDataPath, filename + ".csv");
        string[] dataTypes = new string[3];
        dataTypes[0] = "public.comma-separated-values-text";
        dataTypes[1] = "kUTTypeCommaSeparatedText";
        dataTypes[2] = ".csv";
        NativeFilePicker.PickFile(delegate (string path) { CreateHierarchyObjectStructure(path); }, dataTypes);
        
        foreach (var item in pathFromCSV.PathList)
        {
            Debug.Log(item.pathID + "Anzahl Segmente:" + item.segments.Count);
            foreach (Segment segment in item.segments)
            {
                Debug.Log("StartPoint: " + segment.startPoint.wayPointId + " Endepoint: " + segment.endPoint.wayPointId + "Number ControlPoints: " + segment.controlPoints.Count);
                foreach (ControlPoint cp in segment.controlPoints)
                {
                    Debug.Log("Controlpoint Start x:" + cp.position.x.ToString() + "Controlpoint Start y: " + cp.position.y.ToString());
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void CreateHierarchyObjectStructure(string filePath)
    {
        if (File.Exists(filePath))
        {
            Debug.Log("File found " + filePath);
            //CreateHierarchyObjectStructure();


            List<CSVHelperClass> objectsFromCSV = deserializeCSV(filePath);
            Path tempPath = new Path();
            Segment tempSegment = new Segment();
            int lastPathId = -1;
            int pathCounter = 0;
            int elementPointer;
            while (pathCounter < objectsFromCSV.Count)
            {
                if (objectsFromCSV[pathCounter].PathID != lastPathId
                    && objectsFromCSV[pathCounter].Type == "Path")
                {
                    //reset path object and set new path id
                    tempPath.pathID = objectsFromCSV[pathCounter].PathID;
                    tempPath.segments.Clear();
                    lastPathId = objectsFromCSV[pathCounter].PathID;
                }
                else if (objectsFromCSV[pathCounter].Type == "Segment")
                {
                    tempSegment.controlPoints.Clear();
                    tempSegment.startPoint = new WayPoint(objectsFromCSV[pathCounter].StartID, objectsFromCSV[pathCounter].StartX, objectsFromCSV[pathCounter].StartY);
                    tempSegment.endPoint = new WayPoint(objectsFromCSV[pathCounter].EndID, objectsFromCSV[pathCounter].EndX, objectsFromCSV[pathCounter].EndY);

                    //Add all following Controlpoints to the Segment
                    elementPointer = pathCounter;
                    while (elementPointer + 1 < objectsFromCSV.Count && objectsFromCSV[elementPointer + 1].Type == "ControlPoint")
                    {
                        elementPointer++;
                        tempSegment.controlPoints.Add(new ControlPoint(objectsFromCSV[elementPointer].StartX, objectsFromCSV[elementPointer].StartY));
                    }
                    tempPath.segments.Add(new Segment(tempSegment));

                    pathCounter = elementPointer;
                    if (pathCounter + 1 >= objectsFromCSV.Count
                    || objectsFromCSV[pathCounter].PathID != objectsFromCSV[pathCounter + 1].PathID)
                    {
                        pathFromCSV.PathList.Add(new Path(tempPath));
                    }
                }
                pathCounter++;
            }
            createUIPath(pathFromCSV);
        }
        else
            Debug.LogError("File not found " + filePath);
    }
    /// <summary>
    /// 
    /// </summary>
    private void createUIPath(PathFromCSV pathFromCsv)
    {
        GameObject Pathmanager = Managers.transform.Find("PathManager").gameObject;
        PathBezier pathBezier = Pathmanager.GetComponent<PathBezier>();
        pathBezier.CreateFromPathList(pathFromCsv);
    }
   

    /// <summary>
    /// 
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public List<CSVHelperClass> deserializeCSV(string filePath)
    {
        var config = new CsvConfiguration(CultureInfo.InvariantCulture)
        {
            HasHeaderRecord = true,
            Delimiter = ",",
        };
        try
        {
            //StringReader reader = new StringReader(PathFileCsv.text);
            //if (reader == null)
            //    Debug.Log("Could not create Strinreader from TextAsset");
            using (var reader = new StreamReader(filePath))
            using (var csv = new CsvReader(reader, config))
            {
                var records = new List<CSVHelperClass>();
                csv.Read();
                csv.ReadHeader();
                while (csv.Read())
                {
                    var record = new CSVHelperClass
                    {
                        PathID = csv.GetField<int>("PathID"),
                        Type = csv.GetField<string>("Type"),
                        StartID = csv.GetField<int>("StartID"),
                        StartX = csv.GetField<float>("StartX"),
                        StartY = csv.GetField<float>("StartY"),
                        EndID = csv.GetField<int>("EndID"),
                        EndX = csv.GetField<float>("EndX"),
                        EndY = csv.GetField<float>("EndY"),
                        Handle1 = csv.GetField<float>("Handle1"),
                        Handle2 = csv.GetField<float>("Handle2"),
                    };
                    records.Add(record);
                }
                return records;
            }
        }
        catch (Exception e)
        {
            Debug.LogError("Could not deserialize File: " + e.Message);
            return new List<CSVHelperClass>();
        }
    }
}


public class CSVHelperClass
{
    [Default(0)]
    public int PathID { get; set; }


    public string Type { get; set; }

    [Default(0)]
    public int StartID { get; set; }

    [Default(0)]
    public float StartX { get; set; }

    [Default(0)]
    public float StartY { get; set; }

    [Default(0)]
    public int EndID { get; set; }

    [Default(0)]
    public float EndX { get; set; }

    [Default(0)]
    public float EndY { get; set; }

    [Default(0)]
    public float Handle1 { get; set; }

    [Default(0)]
    public float Handle2 { get; set; }
}