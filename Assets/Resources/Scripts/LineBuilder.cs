using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineBuilder : MonoBehaviour
{
    public LineRenderer lineRenderer;
    public bool rendering = false;

    public Transform movingObject;
    public List<Transform> waypoints;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (rendering)
        {
            List<Vector3> pos = new List<Vector3>();
            pos.Add(new Vector3 (movingObject.position.x, transform.position.y, movingObject.position.z));
            foreach(Transform waypoint in waypoints)
            {
                pos.Add(new Vector3(waypoint.position.x, transform.position.y, waypoint.position.z));
            }
            lineRenderer.positionCount = pos.Count;
            lineRenderer.SetPositions(pos.ToArray());
            lineRenderer.useWorldSpace = true;
        }
    }

    public void ToggelRender()
    {
        rendering = !rendering;
    }
}
