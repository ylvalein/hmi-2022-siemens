﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Position_updater : MonoBehaviour
{
    [SerializeField]
    private bool initiated;
    [SerializeField]
    private bool moving;
    public AnimationController aniCon;

    public GameObject laserObject;
    [SerializeField]
    private List<Curve> curveList = new List<Curve>();
    [SerializeField]
    private WayPoint currentWP;
    private Curve currentCurve;


    // Start is called before the first frame update
    void Start()
    {
        initiated = false;
        moving = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!initiated) return;

        //Debug.LogError(gameObject.transform.localPosition + " AMR, Target " + new Vector3(currentWP.position.x, gameObject.transform.localPosition.y, currentWP.position.z));

        //check if we reached a waypoint
        if (moving && Vector3.Distance(gameObject.transform.localPosition, new Vector3(currentWP.position.x, gameObject.transform.localPosition.y, currentWP.position.z)) < 0.1f)
        {
            //stop animations
            aniCon.StopMoving();
            moving = false;
        }
        // check if we are leaving a waypoint
        else if (!moving && Vector3.Distance(gameObject.transform.localPosition, new Vector3(currentWP.position.x, gameObject.transform.localPosition.y, currentWP.position.z)) < 0.1f)
        {
            //Get the curve to display and Set the next target WP to the current one
            GetWpAndCurve();
            //send curve and WP to animation manager and start animation
            aniCon.InitiateMoving(currentCurve);
            moving = true;
        }

    }


    public void UpdatePosition(int index, float value)
    {

        switch (index)
        {
            case 0:
                //change x position of amr
                gameObject.transform.localPosition = new Vector3(value, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
                break;
            case 1:
                //change z position of amr
                gameObject.transform.localPosition = new Vector3(gameObject.transform.localPosition.x, gameObject.transform.localPosition.y, value);
                break;
            case 2:
                //change rotation amr
                float valueDegree = -value / (Mathf.PI / 180);
                gameObject.transform.localRotation = Quaternion.Euler(0, valueDegree, 0);
                break;
        }
    }

    public void Initiate()
    {
        //Debug.LogError(Time.deltaTime + " deltatime");
        initiated = true;
        laserObject.transform.position = gameObject.transform.position;
        laserObject.transform.rotation = gameObject.transform.rotation;
    }

    private WayPoint FindClosestWaypoint()
    {
        float closestDistance = -1;
        WayPoint wp = new WayPoint(0, 0, 0);
        foreach(Curve curve in curveList)
        {
            float distance = Vector3.Distance(gameObject.transform.position, new Vector3(curve.start.position.x, gameObject.transform.position.y, curve.start.position.z));
            if (closestDistance == -1) {
                closestDistance = distance;
                wp = curve.start;
            }
            else if (closestDistance > distance)
            {
                closestDistance = distance;
                wp = curve.start;
            }
        }
        return wp;
    }

    public Curve FindCurrentCurve()
    {
        /*
         * WPID: 2200 -> Pfad 28
         * WPID: 2201 -> Pfad 29
         * WPID: 2202 -> Pfad 31
         */

        WayPoint wp = FindClosestWaypoint();
        
        
        foreach (Curve curve in curveList)
        {
            if (curve.start.wayPointId == wp.wayPointId)
            {
                return curve;
            }
        }
       
        return null;
    }

    public Curve SetCurrentCurve(WayPoint wp)
    {
        foreach (Curve curve in curveList)
        {
            if (curve.start.wayPointId == wp.wayPointId)
            {
                return curve;
            }
        }
        return null;
    }


    public void SetCurveList(List<Curve> cl)
    {
        curveList = cl;
        currentWP = FindClosestWaypoint();
        Debug.LogError(currentWP.wayPointId);
        GetWpAndCurve();
    }
    public void GetWpAndCurve()
    {
        currentCurve = SetCurrentCurve(currentWP);
        currentWP = currentCurve.end;
    }
}
