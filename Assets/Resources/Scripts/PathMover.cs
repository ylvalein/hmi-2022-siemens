using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathMover : MonoBehaviour
{
    public List<Vector3> waypoints = new List<Vector3>();
    public int wayPointIndex = 0;

    public bool moving = false;
    public float speed = 1f;


    private Vector3 targetWaypoint;
    public Transform movingObject;
    public GameObject arrow;
    public GameObject origin;


    // Start is called before the first frame update
    void Start()
    {
        if(waypoints.Count != 0)
        {
            targetWaypoint = waypoints[wayPointIndex];
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Check if the position of the cube and sphere are approximately equal.
        if (Vector3.Distance(movingObject.transform.position, new Vector3(targetWaypoint.x, movingObject.transform.position.y, targetWaypoint.z)) < 0.01f && moving)
        {
            Debug.Log("we have reached waypoint" + wayPointIndex);
            wayPointIndex += 1;
        }
        //TODO if we are close to the last waypoint top the arrow animation

        if (wayPointIndex < waypoints.Count && moving)
        {
            targetWaypoint = waypoints[wayPointIndex];
            MoveForward();
        }
        else if(moving)
        {
            
            moving = false;
            //arrow.GetComponent<ArrowAnimation>().ToggleMoving();
        }
    }

    void MoveForward()
    {
        // Move our position a step closer to the target.
        float step = speed * Time.deltaTime; // calculate distance to move
        Vector3 newTarget = new Vector3(targetWaypoint.x, movingObject.transform.position.y, targetWaypoint.z);
        Debug.Log(newTarget.ToString());
        newTarget = newTarget - origin.transform.position;
        movingObject.transform.position = Vector3.MoveTowards(movingObject.transform.position, newTarget, step);
    }

    public void StartMoving()
    {
        movingObject.transform.localPosition = new Vector3(waypoints[wayPointIndex].x, movingObject.transform.localPosition.y, waypoints[wayPointIndex].z);
        moving = true;
        //arrow.GetComponent<ArrowAnimation>().ToggleMoving();
    }
    public void SetPath(List<Vector3> newPath)
    {
        waypoints = newPath;
    }
}
