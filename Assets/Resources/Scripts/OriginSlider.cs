﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.UI;

public class OriginSlider : MonoBehaviour
{

    public Slider sliderX, sliderY, sliderz, sliderRot;
    public Transform amr;
    private float prevSliderRot = 0;
    public float rotateValue;

    // Start is called before the first frame update
    void Start()
    {
        setSliderValues();
    }
    // Update is called once} per frame
    void Update()
    {

    }

    public void ChangeOriginXPosition(Slider slider)
    {
        gameObject.transform.localPosition = new Vector3(slider.value, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
    }
    public void ChangeOriginYPosition(Slider slider)
    {
        gameObject.transform.localPosition = new Vector3(gameObject.transform.localPosition.x, slider.value, gameObject.transform.localPosition.z);
    }
    public void ChangeOriginZPosition(Slider slider)
    {
        gameObject.transform.localPosition = new Vector3(gameObject.transform.localPosition.x, gameObject.transform.localPosition.y, slider.value);
    }
    public void ChangeOriginRoation(Slider slider)
    {

        //gameObject.transform.localRotation = Quaternion.Euler(0, slider.value, 0);
        if (Mathf.Abs(slider.value) > Mathf.Abs(prevSliderRot))
        {
            gameObject.transform.RotateAround(amr.position, Vector3.up, rotateValue);// = Quaternion.Euler(0, slider.value, 0);
        }
        else
        {
            gameObject.transform.RotateAround(amr.position, Vector3.up, -rotateValue);// = Quaternion.Euler(0, slider.value, 0);
        }
        prevSliderRot = slider.value;

    }

    public void saveSliderValuesToXML()
    {
        if (GameObject.Find("Sliders") is null)
        {
            string dirPath = System.IO.Path.Combine(Application.persistentDataPath, "AMRdata");
            if (!Directory.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
                Debug.LogWarning(dirPath + " created");
            }
            ////////Debug.Log("Saving offset to xml...");
            string file = System.IO.Path.Combine(dirPath, "AMR_offset.xml");
            XmlWriterSettings xws = new XmlWriterSettings();
            xws.OmitXmlDeclaration = true;
            xws.Indent = true;

            using (XmlWriter xw = XmlWriter.Create(file, xws))
            {
                XDocument doc = new XDocument(
                    new XElement("TransformVariables",
                        new XElement("TransformVariable",
                            new XElement("Variable", "x"),
                            new XElement("Value", $"{sliderX.value}")),
                        new XElement("TransformVariable",
                            new XElement("Variable", "y"),
                            new XElement("Value", $"{sliderY.value}")),
                        new XElement("TransformVariable",
                            new XElement("Variable", "z"),
                            new XElement("Value", $"{sliderz.value}")),
                        new XElement("TransformVariable",
                            new XElement("Variable", "r"),
                            new XElement("Value", $"{gameObject.transform.localRotation.eulerAngles.y}"))
                    ));
                doc.Save(xw);
            }
        }
    }

    public void setSliderValues()
    {
        sliderX.gameObject.SetActive(true);
        sliderY.gameObject.SetActive(true);
        sliderz.gameObject.SetActive(true);
        sliderRot.gameObject.SetActive(true);

        sliderX.value = gameObject.transform.position.x;
        sliderY.value = gameObject.transform.position.y;
        sliderz.value = gameObject.transform.position.z;
        //sliderRot.value = gameObject.transform.localRotation.eulerAngles.y;
    }

}
