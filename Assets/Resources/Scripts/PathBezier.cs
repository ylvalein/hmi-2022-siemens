using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class PathBezier : MonoBehaviour
{
    public GameObject PositionManager;
    public GameObject WayPointPrefab;
    public bool showWayPoints;
    public GameObject origin;

    List<LineRenderer> lineRenderers = new List<LineRenderer>();

    public Material lineMat;
    public Color lineStartColor = new Color();
    public Color lineEndColor = new Color();
    public float lineWidth;
    private static PathFromCSV PathfromCSVFile;
    private float numberofpoints = 35;
    [SerializeField]
    private List<Curve> CurveList = new List<Curve>();

    // Start is called before the first frame update
    void Start()
    {
        origin = GameObject.Find("origin");

        //PathFromCSV pathList = gameObject.GetComponent<PathFromCSV>();
        //CreateFromPathList(pathList);
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void CreateFromPathList(PathFromCSV pathFromCSV)
    {
        PathfromCSVFile = pathFromCSV;
        foreach (Path path in PathfromCSVFile.PathList)
        {
            CreatePath(path);
        }
        GameObject.Find("PathLines").SetActive(false);
        PositionManager.GetComponent<Position_updater>().SetCurveList(CurveList);
    }

    private void CreatePath(Path path)
    {
        lineRenderers.Add(CreateLine(path.pathID));
        
        int counter = 0;

        List<Vector3> pts = new List<Vector3>();
        Curve curve = new Curve();

        foreach (Segment seg in path.segments){
            if (seg.controlPoints.Count == 0)
            {
                pts.Add(seg.startPoint.position);
                pts.Add(seg.endPoint.position);
            }
            else
            {
                List<Vector3> CurvePts = new List<Vector3>();
                //create Bezier Curve
                CurvePts = VisualizeCurve(seg);
                foreach(Vector3 pos in CurvePts)
                {
                    pts.Add(pos);
                }
                if (showWayPoints)
                {
                    for (int w = 0; w < seg.controlPoints.Count; ++w)
                    {
                        //GameObject obj = Instantiate(WayPointPrefab, seg.controlPoints[w].position, Quaternion.identity, origin.transform);
                        GameObject obj = Instantiate(WayPointPrefab, origin.transform, false);
                        obj.transform.localPosition = seg.controlPoints[w].position;
                        obj.transform.rotation = Quaternion.identity;
                    }
                }
            }
            counter += 1;
            curve.start = seg.startPoint;
            curve.end = seg.endPoint;
        }
        
        curve.list = pts;
        CurveList.Add(curve);

        VisualizePath(lineRenderers[lineRenderers.Count - 1], pts);
        

        /// SET WAYPOINTS TO DOOOOOO
        //AnimationManager.GetComponent<PathMover>().SetPath(pts);

        //curve.exportCoordCsv(path.pathID.ToString());

    }

    private LineRenderer CreateLine(int id)
    {
        GameObject obj = new GameObject();
        obj.transform.SetParent(GameObject.Find("PathLines").transform);
        obj.transform.localPosition = new Vector3(0, 0, 0);
        obj.name = "Path with ID: " + id;
        LineRenderer lr = obj.AddComponent<LineRenderer>();
        lr.useWorldSpace = false;
        lr.material = lineMat;
        lr.startColor = lineStartColor;
        lr.endColor = lineEndColor;
        lr.startWidth = lineWidth;
        AddLineRendererColors(lr);
        return lr;
    }

    private void AddLineRendererColors(LineRenderer renderer)
    {
        Gradient newGradient = new Gradient();
        newGradient.SetKeys(
            new GradientColorKey[] { new GradientColorKey(lineStartColor, 0.0f), new GradientColorKey(lineEndColor, 1.0f) },
            new GradientAlphaKey[] { new GradientAlphaKey(1.0f, 1.0f), new GradientAlphaKey(70 / 255f, 1.0f) });

        renderer.colorGradient = newGradient;
    }
      

    private void VisualizeLine(LineRenderer render, Segment seg)
    {
        //create a game object with a line renderer function
        render.SetPosition(0, seg.startPoint.position);
        render.SetPosition(1, seg.endPoint.position);
    }

    private List<Vector3> VisualizeCurve(Segment seg)
    {
        //List with all the ControlPoints Vector3s
        List<Vector3> cPts = new List<Vector3>();
        cPts.Add(seg.startPoint.position);
        for (int j = 0; j < seg.controlPoints.Count; ++j)
        {
            cPts.Add(seg.controlPoints[j].position);
        }

        List<Vector3> curve = new List<Vector3>();
        if (seg.controlPoints.Count == 3)
        {
            //calculate quadratic
            //curve = CalculateQuadratic(cPts);
            curve = BezierCurve.PointList3(cPts, 0.03f);
        }
        //else if (seg.controlPoints.Count == 4)
        //{
        //    //calculate cubic
        //}
        else
        {
            //Generate points for the curve
            curve = BezierCurve.PointList3(cPts, 0.03f);
        }

        return curve;
    }
    private void VisualizePath(LineRenderer render, List<Vector3> pts)
    {
        render.positionCount = pts.Count;
        for (int k = 0; k < pts.Count; ++k)
        {
            render.SetPosition(k, pts[k]);
        }
    }

    public List<Vector3> CalculateQuadratic(List<Vector3> controlP)
    {
        List<Vector3> curvePt = new List<Vector3>();
        numberofpoints = 35;
        float t;
        Vector3 position;
        for (int i = 0; i < numberofpoints; i++)
        {
            t = i / (numberofpoints - 1.0f);
            position = (1.0f - t) * (1.0f - t) * controlP[0]
            + 2.0f * (1.0f - t) * t * controlP[1] + t * t * controlP[2];
            curvePt.Add(position);
        }
        return curvePt;
    }
}

[System.Serializable]
public class Curve
{
    public List<Vector3> list;
    public WayPoint start;
    public WayPoint end;

    /// <summary>
    /// Export path coordinates in csv for ReferenceServer 
    /// </summary>
    /// <param name="filename"></param>
    public void exportCoordCsv(string filename)
    {
        List<int> myValues;
        //string csv = string.Join(",", list.Select(x => x.ToString()).ToArray());
        string exportPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "temp", filename + ".csv");
        Debug.Log("Export Path:" + exportPath);
        foreach (Vector3 v3 in list)
        {
            Debug.Log(v3.x + ", " + v3.y + ", " + v3.z);
        }
        File.WriteAllLines(exportPath, list.Select(v3 => string.Join(";", v3.x + "; " + v3.y + "; " + v3.z)));
    }
}

