﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class setTMProText : MonoBehaviour
{
    public TMP_InputField tmpgui;
    TMP_Dropdown dropdown;

    private void Start()
    {
        dropdown=GetComponent<TMP_Dropdown>();
    }
    public void setText()
    {
        tmpgui.text = dropdown.options[dropdown.value].text;
    }

}
