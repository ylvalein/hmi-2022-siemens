﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class chooseVisibleScanner : MonoBehaviour
{
    public Camera camera;
    public GameObject laserScanner1;
    public GameObject laserScanner2;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float distScanner1 = Vector3.Distance(camera.transform.position, laserScanner1.transform.position);
        float distScanner2 = Vector3.Distance(camera.transform.position, laserScanner2.transform.position);

        if (distScanner1 < distScanner2)
        {
            laserScanner1.SetActive(true);
            laserScanner2.SetActive(false);
        }
        else
        {
            laserScanner1.SetActive(false);
            laserScanner2.SetActive(true);
        }

    }
}
