﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneContentPosition : MonoBehaviour
{
    private GameObject sceneContent;
    // Start is called before the first frame update
    void Start()
    {
        sceneContent = GameObject.Find("SceneContent");   
    }

    // Update is called once per frame
    public void setPosition()
    {
        sceneContent.transform.position = transform.position;
    }
}
