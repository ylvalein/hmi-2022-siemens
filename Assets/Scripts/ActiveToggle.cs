﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveToggle : MonoBehaviour
{
    //public GameObject obj;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void toggle()
    {
        gameObject.SetActive(!gameObject.activeInHierarchy);
    }

    public void setActive()
    {
        gameObject.SetActive(true);
    }

    public void setInactive()
    {
        gameObject.SetActive(false);
    }
}
