﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Shared
{
    public class ToggleObject : ConnectedDataArray<System.Int16>
    {
        public Canvas myCanvas;
        public GameObject Object;

        private System.Int16[] werte = new System.Int16[4];
        private float leftPx, rightPx, bottomPx, topPx;



        protected override void OnDataChanged(int index, System.Int16 value, bool multipleValuesChangedFinished = true)
        {
            werte[index] = value;
            leftPx = werte[0];
            rightPx = werte[1];
            topPx = werte[2];
            bottomPx = werte[3];

            if (leftPx == 0 && rightPx == 0 && bottomPx == 0 && topPx == 0)
            {
                myCanvas.enabled = false;
                Object.SetActive(false);
            }


            else
            {
                myCanvas.enabled = true;
                Object.SetActive(true);
            }

        }

    }


}
