﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class DisableCanvas : MonoBehaviour
{

    public GameObject GameObjectToDisable;
    public TextMeshPro ButtonText;
    private bool toggle;

    public void OnSubmit()
    {
        if(!toggle)
        {
            GameObjectToDisable.SetActive(true);
            toggle = !toggle;
            ButtonText.text = "Hide Position Panel";
        }
        else
        {
            GameObjectToDisable.SetActive(false);
            toggle = !toggle;
            ButtonText.text = "Show Position Panel";
        }
    }
}
