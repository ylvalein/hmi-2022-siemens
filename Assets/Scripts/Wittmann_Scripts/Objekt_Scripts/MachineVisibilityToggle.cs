﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class MachineVisibilityToggle : MonoBehaviour
{

    public GameObject Machine;
    public TextMeshPro ButtonText;
    public GameObject GaugeControl;

    private int i = 0;


    public void Toggle()
    {
        if(i==0)
        {
            Machine.SetActive(false);
            GaugeControl.SetActive(false);
            i++;
            ButtonText.text = "Show Machine";
        }
        else
        {
            Machine.SetActive(true);
            GaugeControl.SetActive(true);
            i = 0;
            ButtonText.text = "Hide Machine";
        }
    }
    
}
