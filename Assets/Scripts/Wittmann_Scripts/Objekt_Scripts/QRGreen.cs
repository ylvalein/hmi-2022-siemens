﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class QRGreen : MonoBehaviour
{
    public GameObject QR;
    public void TurnGreen()
    {
        QR.SetActive(true);
    }

    public void TurnNotGreen()
    {
        QR.SetActive(false);
    }
}
