﻿
using Microsoft.MixedReality.Toolkit.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Shared
{

    public class ConnectedDataPrbability : ConnectedData<float>
    {

        public Text probability;
        private float variable;

        protected override void OnDataChanged(float value)
        {
            variable = value;
            probability.text = string.Format("{0:0.0}", (variable * 100f)) + "%";
        }

       
    }


}
