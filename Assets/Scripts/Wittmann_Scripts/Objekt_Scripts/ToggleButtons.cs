using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ToggleButtons : MonoBehaviour
{
    private Image image;

    public Button button0;
    public GameObject button1;
    public GameObject button2;
    public GameObject button3;
    public GameObject button4;
    public GameObject button5;
    public GameObject button6;

    public Sprite buttonOff;
    public Sprite buttonOn;


    private bool toggle;

    public void OnSubmit()
    {
    
        if (toggle)
        {
            button1.SetActive(false);
            button2.SetActive(false);
            button3.SetActive(false);
            button4.SetActive(false);
            button5.SetActive(false);
            button6.SetActive(false);

            //button0.image.sprite = buttonOff;

            Debug.Log("Switch off");
            TextMeshProUGUI btText = this.GetComponentInChildren<TextMeshProUGUI>();
            btText.text = "Show Buttons";
            //btText.color = new Color(0.33f, 0.33f, 0.33f);

            //Button b = this.GetComponent<Button>();
            //ColorBlock colors = b.colors;
            //colors.normalColor = new Color(1, 0, 0);
            //b.colors = colors;

            //this.transform.GetChild(0);
        }
        else
        {
            button1.SetActive(true);
            button2.SetActive(true);
            button3.SetActive(true);
            button4.SetActive(true);
            button5.SetActive(true);
            button6.SetActive(true);

            //button0.image.sprite = buttonOn;

            //button0.image.color = Color.white;

            //Button b = button1.GetComponent<Button>();
            //ColorBlock colors = b.colors;
            //colors.normalColor = Color.red;
            //b.colors = colors;
            Debug.Log("Switch on");
            TextMeshProUGUI btText = this.GetComponentInChildren<TextMeshProUGUI>();
            btText.text = "Hide Buttons";
            btText.color = new Color(0.84f, 0.84f, 0.84f);

            //btText.color = Color.green;
            //GetComponent<Image>().color = Color.green;
        }
            toggle = !toggle;
    }
}
