﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Shared
{
    public class ConnectedDataLabel : ConnectedData<string>
    {
        public Text LabelText;

        protected override void OnDataChanged(string value)
        {
            LabelText.text = value;
        }
    }

}

