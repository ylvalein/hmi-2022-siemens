﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Shared
{
    public class MoveObject : ConnectedDataArray<System.Int16>
    {
        public Transform TheCanvas;
        public Transform TheImage;
        public InputField pxtocmx_field;
        public InputField pxtocmy_field;

        private System.Int16[] werte = new System.Int16[4];
        private float PosXMittelPunktPx, PosXMittelPunktCm, PosYMittelPunktPx, PosYMittelPunktCm;
        private float leftPx, rightPx, bottomPx, topPx, posX, posY;
        public float anyfeederXMin, anyfeederXMax, anyfeederYMin, anyfeederYMax;
        public float posUIXMin, posUIXMax, posUIYMin, posUIYMax;
        


        private float pxtocmx = 0.625f;
        private float pxtocmy = 0.41f;

        private void Start()
        {
            GameObject positionUI = gameObject.transform.parent.gameObject.transform.parent.gameObject;

            anyfeederXMin = positionUI.GetComponent<posUiValues>().anyfeederXMin;
            anyfeederXMax = positionUI.GetComponent<posUiValues>().anyfeederXMax;
            anyfeederYMin = positionUI.GetComponent<posUiValues>().anyfeederYMin;
            anyfeederYMax = positionUI.GetComponent<posUiValues>().anyfeederYMax;

            posUIXMin = positionUI.GetComponent<posUiValues>().posUIXMin;
            posUIXMax = positionUI.GetComponent<posUiValues>().posUIXMax;
            posUIYMin = positionUI.GetComponent<posUiValues>().posUIYMin;
            posUIYMax = positionUI.GetComponent<posUiValues>().posUIYMax;

        }

        public void OnSubmit()
        {
            pxtocmx = float.Parse(pxtocmx_field.text);
            pxtocmy = float.Parse(pxtocmy_field.text);
        }

        protected override void OnDataChanged(int index, System.Int16 value, bool multipleValuesChangedFinished = true)
        {
            werte[index] = value;
            leftPx = werte[0];
            rightPx = werte[1];
            topPx = werte[2];
            bottomPx = werte[3];

            Debug.Log("left" + leftPx);
            Debug.Log("right" + rightPx);
            Debug.Log("top" + topPx);
            Debug.Log("bottom" + bottomPx);

            PosXMittelPunktPx = leftPx + ((rightPx - leftPx) / 2);
            PosYMittelPunktPx = topPx + ((bottomPx - topPx) / 2);
            float relPosX = PosXMittelPunktPx / (anyfeederXMax - anyfeederXMin);
            float relPosY = PosYMittelPunktPx / (anyfeederYMax - anyfeederYMin);

            posX = posUIXMin + relPosX * (posUIXMax - posUIXMin);
            posX = posUIYMin + relPosY * (posUIYMax - posUIYMin);

            //TheCanvas.localPosition = new Vector3(posX, posY);


            //PosXMittelPunktCm = PosXMittelPunktPx * pxtocmx;
            //PosYMittelPunktCm = PosYMittelPunktPx * pxtocmy;

            //changeThePosition(PosXMittelPunktCm - 2, PosYMittelPunktCm - 19);
            ////changeTheScale(PosXMittelPunktCm - 2, PosYMittelPunktCm - 19);

            //changeTheScale(breitePx * pxtocmx, hoehePx * pxtocmy); <- andere Art um das Scanner UI anzuzeigen 1/2
        }
        private void changeThePosition(float positionX,float positionY)
        {
            TheCanvas.localPosition = new Vector3((positionX / 100f) * 4, (-positionY / 100f) * 4, 0.0f);

            //TheCanvas.localPosition = new Vector3((positionX / 100f) * 4, (-positionY / 100f) * 4, 0.0f);
        }
        private void changeTheScale(float breite, float hoehe)
        {
            TheImage.localPosition = new Vector3((breite / 100f) / 20f, 1.3f, (-hoehe / 100f) / 20);
            //TheImage.localScale = new Vector3(breite, hoehe, TheImage.localScale.z);   siehe Kommentar Zeile 54  2/2
        }
    }

}

