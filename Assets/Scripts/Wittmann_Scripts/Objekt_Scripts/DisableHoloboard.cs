﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;using UnityEngine.UI;

public class DisableHoloboard : MonoBehaviour
{
    public GameObject Holoboard;
    public TextMeshPro ButtonText;

    private int i = 0;

    public void OnSubmit()
    {
        if (i == 0)
        {
            Holoboard.SetActive(true);
            i++;
            ButtonText.text = "Hide Holoboard";
        }
        else
        {
            Holoboard.SetActive(false);
            i = 0;
            ButtonText.text = "Show Holoboard";
        }
    }
}
