﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColliderScript : MonoBehaviour
{
    public BoxCollider m_collider;
    
    public void ToggleCollider()
    {
        m_collider.enabled = !m_collider.enabled;
    }
}
