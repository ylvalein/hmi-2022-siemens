﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;

public class DebugWindow : MonoBehaviour
{
    public bool showLog;
    public bool showWarning;
    public bool showError;
    StringBuilder strBuilder;

    //TMPro.TMP_Text debugText;
    public Text debugText;

    // Start is called before the first frame update
    void Start()
    {
        //debugText = gameObject.GetComponentInChildren<Text>();
        //debugText.text = "Hello <color=green>Dude</color>";
        //debugText.richText = true;
        debugText.color = Color.white;
        
    }

    void OnEnable()
    {
        Application.logMessageReceived += LogMessage;
    }

    // Update is called once per frame
    void OnDisable()
    {
        Application.logMessageReceived -= LogMessage;
    }

    public void LogMessage(string message, string stackTrace, LogType type)
    {
        if(debugText.text.Length > 50000)
        {
            // stringBuilder !!
            //debugText.text = debugText.text.Substring(9000, debugText.text.Length - 9001);
            debugText.text = "";

        }

        string newLog = "";
        if (message.Length > 1000) message = message.Substring(0, 1000);
        if (type == LogType.Log && showLog) newLog = "<color=black>Log: " + message + "</color>\n";
        else if (type == LogType.Warning && showWarning) newLog = "<color=yellow>Warning: " + message + "</color>\n";
        else if (type == LogType.Error && showError) newLog = "<color=red>Error: " + message + "</color>\n";
        else if (type == LogType.Exception) newLog = "<color=magenta>Exception: " + message + "</color>\n";
        else if (type == LogType.Assert) newLog = "<color=teal>Assert: " + message + "</color>\n";

        debugText.text += newLog;
    }
}
