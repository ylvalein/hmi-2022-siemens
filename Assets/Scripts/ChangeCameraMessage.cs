﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCameraMessage : MonoBehaviour
{

    public string message0;
    public string message1;
    public string message2;
    public string message3;
    string[] messages = new string[4];
    private int i = 0;
    private float timeElapsed;
    //Vector3 pos = new Vector3(0, 0, 20);

    // Start is called before the first frame update
    void Start()
    {
        messages[0] = message0;
        messages[1] = message1;
        messages[2] = message2;
        messages[3] = message3;

    }

    // Update is called once per frame
    void Update()
    {
        //Debug.LogWarning(Camera.main.transform.position);
        //Debug.LogWarning(transform.position);
        //transform.position = pos;
        timeElapsed += Time.deltaTime;
        if (timeElapsed > 0.5f)
        {
            //Debug.LogWarning(messages[i]);
            //Object txt= GetComponent<TMPro.TMP_Text>();
            //Debug.LogWarning(txt is null);
            GetComponent<TMPro.TMP_Text>().SetText(messages[i]);
            
            timeElapsed = 0;
            i = ++i % 4;
        }
    }
}
