﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class EnergyOverview : MonoBehaviour
{
    public GameObject ValueSOC, ValueSystemCurrent, ValueSystemVoltage, ValuePowerUsage;
    public GameObject[] charges;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnUpdateValueSOC(float value)
    {
        value = Mathf.RoundToInt(value);
        ValueSOC.GetComponent<TextMeshProUGUI>().SetText(value.ToString());

        //show charges
        float val = value / 20;
        for (int i = 0; i <= 4; i++)
        {
            if (i < val) charges[i].SetActive(true);
            else charges[i].SetActive(false);
        }
    }
    public void OnUpdateValueSystemCurrent(double value)
    {
        //value = Mathf.RoundToInt(value);
        value = Math.Round(value, 1);
        ValueSystemCurrent.GetComponent<TextMeshProUGUI>().SetText(value.ToString());
    }
    public void OnUpdateValueSystemVoltage(float value)
    {
        value = Mathf.RoundToInt(value);
        ValueSystemVoltage.GetComponent<TextMeshProUGUI>().SetText(value.ToString());
    }
    public void OnUpdateValuePowerUsage(float value)
    {
        value = Mathf.RoundToInt(value);
        ValuePowerUsage.GetComponent<TextMeshProUGUI>().SetText(value.ToString());
    }
}


