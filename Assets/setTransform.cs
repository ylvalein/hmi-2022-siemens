﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine;

public class setTransform : MonoBehaviour
{
    OriginSlider sliderScript;

    // Start is called before the first frame update
    void Start()
    {
        sliderScript = gameObject.GetComponent<OriginSlider>();
        setOriginXml();
        sliderScript.setSliderValues();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void setOriginXml()
    {
        Dictionary<string, float> dic = getOffsetFromXml("AMR_offset.xml");

        if (dic is null) return;
        Debug.Log("set origin position..");
        foreach (var element in dic)
        {
            Debug.Log($"{element.Key}: { element.Value}");
        }

        transform.localPosition = new Vector3(dic["x"], dic["y"], dic["z"]);
        transform.localRotation = Quaternion.Euler(0, dic["r"], 0);
    }

    // read offset of the origin from xml file if folder assets
    public Dictionary<string, float> getOffsetFromXml(string xmlFile)
    {
        string file = System.IO.Path.Combine(Application.persistentDataPath, "AMRdata", xmlFile);

        if (!System.IO.File.Exists(file))
        {
            Debug.LogWarning(file + " not found!");
            return null;
        }

        XDocument doc = XDocument.Load(file);

        var elements = doc.Element("TransformVariables").Elements("TransformVariable");
        Dictionary<string, float> dic = new Dictionary<string, float>();

        foreach (var element in elements)
        {
            var name = element.Element("Variable");
            var value = element.Element("Value");
            dic[name.Value.ToString()] = float.Parse(value.Value.ToString());
        }

        return dic;        
    }
}
