﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class DeviceTrackToggle : MonoBehaviour
{
    bool toggle;
    // Start is called before the first frame update
    void Start()
    {
        toggle = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void toggleDeviceTrackPose()
    {
        var pdt = TrackerManager.Instance.GetTracker<PositionalDeviceTracker>();

        if (toggle) pdt.Stop();
        else pdt.Start();

        toggle = !toggle;
    }
}
